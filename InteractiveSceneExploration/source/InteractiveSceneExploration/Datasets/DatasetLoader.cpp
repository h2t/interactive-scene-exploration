#include "DatasetLoader.h"

#include "OSD.h"
#include "KIT-SRD.h"

namespace interactive_scene_exploration
{

    static std::string toID(DatasetName name)
    {
        switch (name)
        {
            case DatasetName::OSD:
                return "OSD";
            case DatasetName::KIT_SR:
                return "KIT-SR";
        }
        throw std::runtime_error("Unknown DatasetName");
    }

    static std::string toName(DatasetName name)
    {
        switch (name)
        {
            case DatasetName::OSD:
                return "Object Segmentation Dataset";
            case DatasetName::KIT_SR:
                return "KIT Support Relation Dataset";
        }
        throw std::runtime_error("Unknown DatasetName");
    }

    static std::vector<DatasetEntry> loadDataset_OSD(std::string const& path)
    {
        osd::LoadedDataset dataset = osd::loadDataset(path);

        std::vector<DatasetEntry> result;
        result.reserve(dataset.pointClouds.size());
        for (auto& pc : dataset.pointClouds)
        {
            DatasetEntry& entry = result.emplace_back();
            entry.id = osd::prefixToString(pc.prefix) + std::to_string(pc.number);
            entry.pointCloud = pc.filteredCloud;
            entry.supportRelations = pc.groundTruthSupportRelations;
        }

        return result;
    }

    static std::vector<DatasetEntry> loadDataset_KIT_SR(std::string const& path)
    {
        kit_srd::LoadedDataset dataset = kit_srd::loadDataset(path);

        std::vector<DatasetEntry> result;
        result.reserve(dataset.entries.size());
        for (auto& e : dataset.entries)
        {
            DatasetEntry& entry = result.emplace_back();
            entry.id = std::to_string(e.number);
            entry.pointCloud = e.labeledPointCloud;
            entry.supportRelations = e.supportRelations;
        }

        return result;
    }

    Dataset DatasetLoader::load(DatasetName name, std::string const& path)
    {
        Dataset result;
        result.id = toID(name);
        result.name = toName(name);

        switch (name)
        {
            case DatasetName::OSD:
                result.entries = loadDataset_OSD(path);
                break;
            case DatasetName::KIT_SR:
                result.entries = loadDataset_KIT_SR(path);
                break;
        }

        return result;
    }

}
