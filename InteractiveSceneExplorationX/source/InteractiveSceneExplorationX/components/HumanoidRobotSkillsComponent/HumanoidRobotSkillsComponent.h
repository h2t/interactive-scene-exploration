/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    InteractiveSceneExplorationX::ArmarXObjects::HumanoidRobotSkillsComponent
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/ArmarXManager.h>
#include <ArmarXCore/libraries/ArmarXCoreComponentPlugins/DebugObserverComponentPlugin.h>

#include <HumanoidRobotSkills/Platform.h>
#include <HumanoidRobotSkills/Robot.h>

#include <RobotAPI/interface/units/PlatformUnitInterface.h>
#include <RobotAPI/interface/units/KinematicUnitInterface.h>
#include <MemoryX/interface/components/PriorKnowledgeInterface.h>

#include <InteractiveSceneExplorationX/interface/HumanoidRobotSkills.h>

#include <map>

namespace humanoid_robot_skills
{
    // TODO: Load from config file
    struct Armar6RobotConfig
    {
        std::string KinematicUnit = "Armar6KinematicUnit";
        std::string KinematicUnitTopic = "RobotState";
        std::string PlatformUnit = "Armar6PlatformUnit";
        std::string PlatformUnitTopic = "PlatformState";
        // TODO: Obstacle avoiding platform unit?
    };

    struct Armar6RobotProxies
    {
        armarx::KinematicUnitInterfacePrx KinematicUnit;
        armarx::PlatformUnitInterfacePrx PlatformUnit;
    };

    struct Armar6TimingInfo
    {
        IceUtil::Time lastJointAngleInterval;
        IceUtil::Time lastPlatformPoseInterval;
    };

    struct Armar6Robot_Component
        : virtual armarx::Component
        , virtual armarx::HumanoidRobotListenerInterface
    // Should also derive from PlatformUnitListener, KinematicUnitListener
    {
        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;

        std::string getDefaultName() const override
        {
            return "Armar6Robot_Component";
        }

        RobotState getState();

        Armar6TimingInfo getTimingInfo();

        Armar6RobotConfig config;
        Armar6RobotProxies proxies;

        std::mutex stateMutex;
        RobotState state;

        std::mutex timingMutex;
        std::vector<IceUtil::Time> timingJointAngles;
        int timingJointAngleIndex = 0;
        IceUtil::Time timingLastJointAngleTime;
        std::vector<IceUtil::Time> timingPlatformPose;
        int timingPlatformPoseIndex = 0;
        IceUtil::Time timingLastPlatformPoseTime;

        // KinematicUnitListener interface
        void reportControlModeChanged(const armarx::NameControlModeMap&, Ice::Long, bool, const Ice::Current&) override;
        void reportJointAngles(const armarx::NameValueMap& jointAngles, Ice::Long timestamp, bool changed, const Ice::Current&) override;
        void reportJointVelocities(const armarx::NameValueMap&, Ice::Long, bool, const Ice::Current&) override;
        void reportJointTorques(const armarx::NameValueMap&, Ice::Long, bool, const Ice::Current&) override;
        void reportJointAccelerations(const armarx::NameValueMap&, Ice::Long, bool, const Ice::Current&) override;
        void reportJointCurrents(const armarx::NameValueMap&, Ice::Long, bool, const Ice::Current&) override;
        void reportJointMotorTemperatures(const armarx::NameValueMap&, Ice::Long, bool, const Ice::Current&) override;
        void reportJointStatuses(const armarx::NameStatusMap&, Ice::Long, bool, const Ice::Current&) override;

        // PlatformUnitListener interface
        void reportPlatformPose(const armarx::PlatformPose& pose, const Ice::Current&) override;
        void reportNewTargetPose(Ice::Float, Ice::Float, Ice::Float, const Ice::Current&) override;
        void reportPlatformVelocity(Ice::Float, Ice::Float, Ice::Float, const Ice::Current&) override;
        void reportPlatformOdometryPose(Ice::Float, Ice::Float, Ice::Float, const Ice::Current&) override;
    };

    using Armar6Robot_ComponentPtr = IceInternal::Handle<Armar6Robot_Component>;

    struct Armar6Robot : humanoid_robot_skills::RobotInterface
    {
        Armar6Robot(armarx::ArmarXManagerPtr const& armarxManager)
            : armarxManager(armarxManager)
        {
            robotComponent = armarx::Component::create<Armar6Robot_Component>();
            armarxManager->addObject(robotComponent);
        }

        RobotState getState() override;

        armarx::ArmarXManagerPtr armarxManager;
        Armar6Robot_ComponentPtr robotComponent;

    };

    using Armar6RobotPtr = std::shared_ptr<Armar6Robot>;

    struct Armar6Platform : PlatformInterface
    {
        Armar6Platform(Armar6RobotPtr const& robot);

        Platform_MoveToLocation_Result moveToLocation(PlatformLocation targetLocation, const Platform_MoveToLocation_Params& params) override;
        Platform_LookUpNamedLocation_Result lookUpNamedLocation(const NamedPlatformLocation& namedLocation) override;
        PlatformLocation getCurrentLocation() override;

        Armar6RobotPtr robot;
    };

    using Armar6PlatformPtr = std::shared_ptr<Armar6Platform>;

}

namespace armarx
{


    struct HumanoidRobotSkillsComponentPropertyDefinitions :
        armarx::ComponentPropertyDefinitions
    {
        HumanoidRobotSkillsComponentPropertyDefinitions(std::string prefix);
    };

    using namespace armarx::RemoteGui::Client;
    struct HumanoidRobotSkillsComponentTab : Tab
    {
        Button printStuff;
    };


    struct HumanoidRobotSkillsComponent
        : virtual armarx::Component
        , virtual armarx::LightweightRemoteGuiComponentPluginUser
        , virtual armarx::DebugObserverComponentPluginUser
    {
        std::string getDefaultName() const override;

        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void createRemoteGuiTab();
        void RemoteGui_update() override;

        HumanoidRobotSkillsComponentTab tab;

        humanoid_robot_skills::Armar6RobotPtr robot;
        humanoid_robot_skills::Armar6PlatformPtr platform;
    };
}
