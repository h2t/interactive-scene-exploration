#include "BoxFitter.h"

#include <SemanticObjectRelations/ShapeExtraction/util/Histogram.h>
#include <SemanticObjectRelations/ShapeExtraction/util/SoftMinMax.h>

#include <boost/container/flat_set.hpp>

#include <iostream>
#include <chrono>

namespace interactive_scene_exploration
{

    using Clock = std::chrono::high_resolution_clock;
    using TimePoint = Clock::time_point;
    using Duration = Clock::duration;

    float toMilliSeconds(Duration dur)
    {
        long microSeconds = std::chrono::duration_cast<std::chrono::microseconds>(dur).count();
        float milliSeconds = microSeconds * 0.001f;
        return milliSeconds;
    }

    float toMilliSeconds(TimePoint start, TimePoint end)
    {
        Duration dur = end - start;
        return toMilliSeconds(dur);
    }

    using Hyperplane3f = Eigen::Hyperplane<float, 3>;
    using ParametrizedLine3f = Eigen::ParametrizedLine<float, 3>;

    static float BoxErrorFunction(Eigen::Vector3f point,
                                  std::array<Hyperplane3f, 3> const& faces)
    {
        /* Cases:
         *
         * 0) behind all six faces => inside Box => error =
         * 1) outside exactly one face => distance to this face
         * 2) outside exactly two faces (orthogonal) => distance to intersection line
         * 3) outside exactly three faces => distance to corner
         *
         * In (1-3), the distance can be computed
         * by Pythagoras over the single distances as kathetes
         */

        float sumSquaredDistances = 0;
        float minDistance = std::numeric_limits<float>::max();
        for (Hyperplane3f const& face : faces)
        {
            float signedDistance = face.signedDistance(point);

            // check whether at least one signedDistance is positive (=> point outside box)
            if (signedDistance > 0)
            {
                // sum up squared distances
                sumSquaredDistances += signedDistance * signedDistance;
            }

            minDistance = std::min(minDistance, std::abs(signedDistance));
        }

        if (sumSquaredDistances > 0)
        {
            // at least one distance indicated outside
            return sumSquaredDistances;
        }
        else
        {
            // inside => return smallest distance
            return minDistance * minDistance;
        }
    }

    Evaluation evaluate(const semrel::Box& model,
                        std::vector<Eigen::Vector3f> const& points,
                        float inlierErrorThreshold,
                        float outlierRate)
    {
        // allow some points to be ignored for error sum to cope with outliers
        std::size_t numInliers = 0;

        std::vector<float> errors;
        semrel::SoftMinMax errorSoftMax(outlierRate, points.size());

        auto facesV = model.getFaces();
        std::array<Hyperplane3f, 3> faces = {facesV[0], facesV[1], facesV[2]};

        int pointsSize = (int)points.size();
        for (int i = 0; i < pointsSize; ++i)
        {
            Eigen::Vector3f point = points[i];
            float pointError = BoxErrorFunction(point, faces);
            if (pointError <= inlierErrorThreshold)
            {
                numInliers++;
            }
            errors.push_back(pointError);
            errorSoftMax.add(pointError);
        }

        float maxError = errorSoftMax.getSoftMax();

        // only add errors <= maxError
        float errorSum = 0;
        for (float pointError : errors)
        {
            errorSum += (pointError <= maxError ? pointError : 0);
        }

        //float error = errorSum / numInliers;
        float error = (numInliers > 0 ? errorSum / points.size() : std::numeric_limits<float>::max());

        float coverage = numInliers / float(points.size());

        Evaluation result;
        result.error = error;
        result.inlierCount = numInliers;
        result.coverage = coverage;
        return result;
    }

    /// Compute the line parameter of vector when projected onto line.
    static float lineProjectionParameter(const ParametrizedLine3f& line, const Eigen::Vector3f& vector)
    {
        Eigen::Vector3f pOnLine = line.projection(vector);

        Eigen::Vector3f direction = line.direction();
        return (pOnLine - line.origin()).dot(direction) / direction.squaredNorm();
    }

    static Eigen::Matrix3f fitBoxAxesToMSS(BoxFitter::SampleMatrix const& mss)
    {
        Eigen::Vector3f p1 = mss.row(0);
        Eigen::Vector3f p2 = mss.row(1);
        Eigen::Vector3f p3 = mss.row(2);
        Eigen::Vector3f p4 = mss.row(3);
        Eigen::Vector3f p5 = mss.row(4);

        Eigen::Hyperplane<float, 3> planeOne = Eigen::Hyperplane<float, 3>::Through(p1, p2, p3);

        // bad MSS
        if (std::abs(planeOne.signedDistance(p4)) < 0.01f
            || std::abs(planeOne.signedDistance(p5)) < 0.01f)
        {
            return Eigen::Matrix3f::Zero();
        }

        Eigen::Vector3f p4Proj = planeOne.projection(p4);
        Eigen::Hyperplane<float, 3> planeTwo = Eigen::Hyperplane<float, 3>::Through(p4, p4Proj, p5);


        Eigen::Matrix3f axes;
        axes.col(0) = planeOne.normal();
        axes.col(1) = planeTwo.normal();
        axes.col(2) = axes.col(0).cross(axes.col(1));

        return axes;
    }

    static std::vector<Hyperplane3f> computeVisiblePlanes(const Eigen::Matrix3f& axes,
            const std::vector<Eigen::Vector3f>& points)
    {
        // compute displacements for each normal
        std::vector<float> displacements[3];
        for (int i = 0; i < 3; ++i)
        {
            displacements[i].resize(points.size());
        }

        int pointsSize = (int)points.size();
        for (int pi = 0; pi < pointsSize; ++pi)
        {
            Eigen::Vector3f p = points[pi];
            Eigen::Vector3f displacement = - p.transpose() * axes;
            displacements[0][pi] = displacement[0];
            displacements[1][pi] = displacement[1];
            displacements[2][pi] = displacement[2];
        }

        std::vector<Hyperplane3f> result;
        result.reserve(3);
        semrel::Histogram histo;
        for (int i = 0; i < 3; ++i)
        {
            // find most frequent displacements
            histo.initialize(displacements[i]);
            // back side not visible => popPeak not necessary
            result.emplace_back(axes.col(i), histo.getMaxBinValue());
        }

        return result;
    }

    static void sortByNumberOfInliers(std::vector<Hyperplane3f>& visiblePlanes,
                                      std::vector<Eigen::Vector3f> (& planeInliers)[3])

    {
        std::size_t sizes[] =
        {
            planeInliers[0].size(),
            planeInliers[1].size(),
            planeInliers[2].size()
        };

        std::size_t iMax = std::distance(sizes, std::max_element(sizes, sizes + 3));

        auto swap = [&](std::size_t i, std::size_t j)
        {
            std::swap(visiblePlanes[i], visiblePlanes[j]);
            std::swap(planeInliers[i], planeInliers[j]);
        };

        // swap iMax to 0
        if (iMax != 0)
        {
            swap(0, iMax);
        }

        // sort 1 and 2
        if (planeInliers[1].size() < planeInliers[2].size())
        {
            swap(1, 2);
        }
    }

    std::pair<float, Eigen::Vector3f> computeDimAndNormal(float minT, float maxT,
            const ParametrizedLine3f& line,
            const ParametrizedLine3f& line12,
            const Hyperplane3f& plane)
    {
        // we compute the distance in steps and keep track of the results
        // to avoid unnecessary computations
        Eigen::Vector3f lineLimits[] = { line.pointAt(minT), line.pointAt(maxT) };

        Eigen::Vector3f lineProjs[] = { line12.projection(lineLimits[0]), line12.projection(lineLimits[1]) };

        float lineDists[] = { (lineLimits[0] - lineProjs[0]).squaredNorm(),
                              (lineLimits[1] - lineProjs[1]).squaredNorm()
                            };

        int max = lineDists[0] >= lineDists[1] ? 0 : 1;

        float dim = std::sqrt(lineDists[max]);

        Eigen::Vector3f normalDir = lineProjs[max] - lineLimits[max];
        // align with normalDir (assure normal * normalDir >= 0)
        Eigen::Vector3f normal = plane.normal().dot(normalDir) <= 0
                                 ? Eigen::Vector3f(- plane.normal())    // invert (needs cast)
                                 : plane.normal();               // correct

        return std::make_pair(dim, normal);
    }

    semrel::Box computeBox(
        float outlierRate,
        std::vector<Hyperplane3f>& visiblePlanes,
        std::vector<Eigen::Vector3f> (& planeInliers)[3])
    {
        /* Ideally, we now have the visible three planes.
         * We have to find their parallel (invisible) counterparts
         * aka, we have to find the dimensions of the box
         *
         * However, assume we only see two sides.
         * Knowing the dimension of two sides, the third can be extended
         * => take the two planes with most inliers
         * (assume the third one is garbage, besides its normal)
         */

        sortByNumberOfInliers(visiblePlanes, planeInliers);

        Hyperplane3f planeOne = visiblePlanes[0];
        std::vector<Eigen::Vector3f> const& inliersOne = planeInliers[0];

        Hyperplane3f planeTwo = visiblePlanes[1];
        std::vector<Eigen::Vector3f> const& inliersTwo = planeInliers[1];

        Eigen::Vector3f lineIntersection = planeOne.projection(planeTwo.projection(inliersOne[0]));

        // 1st: directed along normal of plane 1
        // 2nd: directed along normal plane 2
        // 3rd: along intersection line of planeOne and planeTwo

        ParametrizedLine3f line1(lineIntersection, planeOne.normal());
        ParametrizedLine3f line2(lineIntersection, planeTwo.normal());
        ParametrizedLine3f line12(lineIntersection, planeOne.normal().cross(planeTwo.normal()));


        // project inliers onto lines and get min/max line parameters

        // on line12 from planeOne
        semrel::SoftMinMax minMaxT12One(outlierRate, inliersOne.size());
        // on line12 frmo planeTwo
        semrel::SoftMinMax minMaxT12Two(outlierRate, inliersTwo.size());
        // on line1 (in planeTwo, orthotogonal to planeOne)
        semrel::SoftMinMax minMaxT1(outlierRate, inliersTwo.size());
        // on line2 (in planeOne, orthotogonal to planeTwo)
        semrel::SoftMinMax minMaxT2(outlierRate, inliersOne.size());

        /* Visualization: for planeOne (P1)
         *
         *       _____________________/ line parallel to line2
         *      /             maxT2  /
         *     /        P1          /
         *    /                    /
         *___/______________minT2 /_____ line12
         *   |                   /|
         *   |        P2        / |
         *   |                    |
         *   |____________________|
         *  minT12              maxT12
         *
         */

        // planeOne => line 2
        for (Eigen::Vector3f const& inlier : inliersOne)
        {
            float t12 = lineProjectionParameter(line12, inlier);
            float t2 = lineProjectionParameter(line2, inlier);
            minMaxT12One.add(t12);
            minMaxT2.add(t2);
        }

        // planeTwo => line 1
        for (Eigen::Vector3f const& inlier : inliersTwo)
        {
            float t12 = lineProjectionParameter(line12, inlier);
            float t1 = lineProjectionParameter(line1, inlier);
            minMaxT12Two.add(t12);
            minMaxT1.add(t1);
        }

        float minT1 = minMaxT1.getSoftMin();
        float maxT1 = minMaxT1.getSoftMax();

        float minT2 = minMaxT2.getSoftMin();
        float maxT2 = minMaxT2.getSoftMax();

        // dimensions on 12 are shared between planeOne and planeTwo
        // => take min/max (same line => same parameter space)
        float minT12 = std::min(minMaxT12One.getSoftMin(), minMaxT12Two.getSoftMin());
        float maxT12 = std::max(minMaxT12One.getSoftMax(), minMaxT12Two.getSoftMax());

        // compute dimensions and directed normals
        float dim12 = maxT12 - minT12;

        auto [dim1, normalOne] = computeDimAndNormal(minT1, maxT1, line1, line12, planeOne);
        auto [dim2, normalTwo] = computeDimAndNormal(minT2, maxT2, line2, line12, planeTwo);

        // normals shall point towards line12 (towards projections)


        // now the position remains to be computed

        // start from center point on line12, move along normals to center
        // (dim1/2 along normalOne, dim2/2 along normalTwo)

        Eigen::Vector3f line12Mid = 0.5 * (line12.pointAt(minT12) + line12.pointAt(maxT12));

        Eigen::Vector3f position = line12Mid - (dim1 / 2) * normalOne - (dim2 / 2) * normalTwo;


        /* Visualization:
         *
         *                   ^ n1
         *        M__________|___________ P1   <---> line2
         *        |                         ^      ^
         *    n2  |                         |      |
         *  <-----|          P             dim1  line1
         *        |                         |      |
         *        |                         v      v
         *       P2
         *         <------- dim2 ------->
         */


        Eigen::Vector3f dimensions; // along n1, n2, n3
        dimensions << dim1, dim2, dim12;

        Eigen::Matrix3f axes;
        axes.col(0) = normalOne;
        axes.col(1) = normalTwo;
        axes.col(2) = normalOne.cross(normalTwo);

        return semrel::Box(position, axes, dimensions);
    }

    static Duration dur_fitAxes = Duration::zero();
    static Duration dur_computeVisiblePlanes = Duration::zero();
    static Duration dur_inlier = Duration::zero();
    static Duration dur_computeBox = Duration::zero();

    std::optional<semrel::Box> runIteration(
        BoxFitterParams const& params,
        BoxFitter::SampleMatrix const& mss,
        int iteration,
        std::vector<Eigen::Vector3f> const& points)
    {
        TimePoint time_before_fitAxes = Clock::now();

        std::optional<semrel::Box> result;

        Eigen::Matrix3f boxAxes = fitBoxAxesToMSS(mss);

        TimePoint time_before_computeVisiblePlanes = Clock::now();
        dur_fitAxes += time_before_computeVisiblePlanes - time_before_fitAxes;

        if (boxAxes.isZero(1e-6f))
        {
            // bad MSS
            return result;
        }


        std::vector<semrel::Hyperplane3f> visiblePlanes = computeVisiblePlanes(boxAxes, points);

        TimePoint time_before_inlier = Clock::now();
        dur_computeVisiblePlanes += time_before_inlier - time_before_computeVisiblePlanes;

        // compute inliers for each plane
        std::vector<Eigen::Vector3f> planeInliers[3];
        for (int i = 0; i < 3; ++i)
        {
            semrel::Hyperplane3f& plane = visiblePlanes[i];
            std::vector<Eigen::Vector3f>& inliers = planeInliers[i];

            inliers.reserve(points.size());
            for (Eigen::Vector3f const& p : points)
            {
                if (plane.absDistance(p) < params.distanceThreshold)
                {
                    inliers.push_back(p);
                }
            }
        }

        int numInliers = 0;
        int numZeroInliers = 0; // how many inliers contain zero points?
        for (auto& inliers : planeInliers)
        {
            numInliers += inliers.size();
            numZeroInliers += inliers.size() == 0; // 1 if 0, else 0
        }

        // discard if too few inliers or two planes have 0 inliers
        if (numInliers < params.minInlierRate * points.size() || numZeroInliers >= 2)
        {
            return result;
        }

        TimePoint time_before_computeBox = Clock::now();
        dur_inlier += time_before_computeBox - time_before_inlier;

        // good planes found
        result = computeBox(params.outlierRate, visiblePlanes, planeInliers);

        TimePoint time_after_computeBox = Clock::now();
        dur_computeBox += time_after_computeBox - time_before_computeBox;

        return result;
    }

    // TODO: Replace


    BoxFitter::SampleMatrix BoxFitter::selectMSS(const std::vector<Eigen::Vector3f>& points, RandomEngine& randomEngine)
    {
        // FIXME: Should we remove this allocation by moving indices into an object?
        boost::container::flat_set<int> indices;
        indices.reserve(MSS_SIZE);

        std::uniform_int_distribution<int> dist(0, points.size() - 1);
        while ((int)indices.size() < MSS_SIZE)
        {
            // add a random index (is ignored if already contained)
            indices.insert(dist(randomEngine));
        }

        SampleMatrix mss;

        int row = 0;
        for (int index : indices)
        {
            mss.row(row) = points[index];
            ++row;
        }

        return mss;
    }

    std::vector<EvaluatedBox> BoxFitter::sampleModels(
        BoxFitterParams const& params,
        std::vector<Eigen::Vector3f> const& points,
        RandomEngine& randomEngine)
    {
        std::vector<EvaluatedBox> result;
        if (points.size() < MSS_SIZE)
        {
            return result;
        }

        std::optional<semrel::Box> bestModel;
        Evaluation bestEval = {std::numeric_limits<float>::max(), 0};

        int numSuccessiveTrivialFails = 0;
        const int maxSuccessiveTrivialFails = 3;

        TimePoint time_beforeFitting = Clock::now();

        std::vector<semrel::Box> candidates;
        int realIterations = 0;
        for (int i = 0; i < params.maxIterations; ++i)
        {
            SampleMatrix mss = selectMSS(points, randomEngine);

            std::optional<semrel::Box> candidate = runIteration(params, mss, i, points);
            ++realIterations;

            // only count trivial fails if they happened n times in a row
            if (!candidate)
            {
                ++numSuccessiveTrivialFails;
                if (numSuccessiveTrivialFails < maxSuccessiveTrivialFails)
                {
                    // don't count this iteration
                    --i;
                }
                else
                {
                    // count this iteration, reset counter
                    numSuccessiveTrivialFails = 0;
                }
                continue;
            }
            candidates.push_back(*candidate);
        }

        TimePoint time_beforeEvaluate = Clock::now();

        std::vector<EvaluatedBox> boxes;
        for (auto& candidate : candidates)
        {
            // compute error
            // TODO: Evaluate and use the coverage as weight?
            EvaluatedBox box;
            box.shape = candidate;
            box.evaluation = evaluate(candidate, points, params.distanceThreshold * params.distanceThreshold, params.outlierRate);

            if (box.evaluation.error < bestEval.error)
            {
                bestModel = candidate;
                bestEval = box.evaluation;
            }
            boxes.push_back(box);
        }

        TimePoint time_afterEvaluate = Clock::now();

        float dur_fitting = toMilliSeconds(time_beforeFitting, time_beforeEvaluate);
        float dur_evaluate = toMilliSeconds(time_beforeEvaluate, time_afterEvaluate);
        //        std::cout << "TIME  Fitting: " << dur_fitting << "ms, Evaluate: " << dur_evaluate << "ms"
        //                  << ", real iterations: " << realIterations
        //                  << std::endl;

        float ms_fitAxes = toMilliSeconds(dur_fitAxes);
        float ms_computeVisiblePlanes = toMilliSeconds(dur_computeVisiblePlanes);
        float ms_inlier = toMilliSeconds(dur_inlier);
        float ms_computeBox = toMilliSeconds(dur_computeBox);

        //        std::cout << "TOTAL TIME \n"
        //                  << "FitAxes: " << ms_fitAxes << "ms\n"
        //                  << "ComputeVisPlanes: " << ms_computeVisiblePlanes << "ms\n"
        //                  << "Inlier: " << ms_inlier << "ms\n"
        //                  << "ComputeBox: " << ms_computeBox << "ms\n"
        //                  << std::endl;

        return boxes;
    }



}
