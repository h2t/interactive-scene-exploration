#include "PointCloudToScene.h"

#include <SemanticObjectRelations/ShapeExtraction/PointCloudSegments.h>

#include <pcl/features/normal_3d.h>

namespace interactive_scene_exploration
{

    static void fillPointsFromCloud(PointCloud::Ptr const& segment,
                                    std::vector<Eigen::Vector3f>* points)
    {
        int segmentSize = (int)segment->size();
        auto& segmentPoints = segment->points;

        points->resize(segmentSize);
        for (int i = 0; i < segmentSize; ++i)
        {
            (*points)[i] = segmentPoints[i].getVector3fMap();
        }
    }

    static void fillNormalsFromCloud(PointCloud::Ptr const& segment,
                                     pcl::NormalEstimation<pcl::PointXYZRGBL, pcl::Normal>& normalEstimation,
                                     pcl::PointCloud<pcl::Normal>& normalPointCloud,
                                     std::vector<Eigen::Vector3f>* normals)
    {

        normalEstimation.setInputCloud(segment);
        normalEstimation.compute(normalPointCloud);

        int segmentSize = (int)segment->size();
        normals->resize(segmentSize);
        auto& normalPoints = normalPointCloud.points;
        for (int i = 0; i < (int)segment->size(); ++i)
        {
            (*normals)[i] = normalPoints[i].getNormalVector3fMap();
        }
    }

    PointCloudToSceneResult PointCloudToScene::extractScene(
        PointCloudToSceneParams const& params,
        PointCloud const& pointCloud,
        RandomEngine& randomEngine)
    {
        PointCloudToSceneResult result;

        semrel::PointCloudSegments segments(pointCloud);
        result.segmentMap = segments.getSegmentMap();

        pcl::NormalEstimation<pcl::PointXYZRGBL, pcl::Normal> normalEstimation;
        normalEstimation.setKSearch(params.normalEstimation_KSearch);
        pcl::search::KdTree<pcl::PointXYZRGBL>::Ptr searchTree(new pcl::search::KdTree<pcl::PointXYZRGBL>());
        normalEstimation.setSearchMethod(searchTree);
        pcl::PointCloud<pcl::Normal> normalPointCloud;

        std::vector<Eigen::Vector3f> points;
        std::vector<Eigen::Vector3f> normals;

        for (auto& pair : result.segmentMap)
        {
            std::uint32_t label = pair.first;
            PointCloud::Ptr const& segment = pair.second;

            fillPointsFromCloud(segment, &points);
            fillNormalsFromCloud(segment, normalEstimation, normalPointCloud, &normals);

            auto& extraction = result.extractionResults.emplace_back(
                                   ObjectFitter::extractObject(params.object, label, points, normals, randomEngine));


            result.distribution.objects.push_back(extraction.object);
        }

        return result;
    }

}
