#pragma once

#include <InteractiveSceneExploration/ObjectHypothesis/PointCloudToScene.h>
#include <InteractiveSceneExploration/SupportRelation/ProbabilisticSupportAnalysis.h>
#include <InteractiveSceneExploration/Datasets/Dataset.h>
#include <InteractiveSceneExploration/Math/Relation.h>
#include <InteractiveSceneExploration/Math/RandomEngine.h>
#include <InteractiveSceneExploration/API.h>

namespace interactive_scene_exploration
{

    struct ProbabilisticSupportEvaluationParams
    {
        PointCloudToSceneParams scene;
        ProbabilisticSupportAnalysisParams support;
    };

    struct ProbabilisticSupportEvaluationSingleResult
    {
        double brierScore = 0.0;
        double precision = 0.0;
        double recall = 0.0;
        double accuracy = 0.0;
        double F1 = 0.0;
    };

    struct ProbabilisticSupportEvaluationResult
    {
        ProbabilisticSupportEvaluationSingleResult deterministic;
        ProbabilisticSupportEvaluationSingleResult probabilistic;
    };

    // TODO: Move this somewhere else?
    struct ExtractedRelations
    {
        Relation groundTruth;
        Relation deterministic;
        Relation probabilistic;
    };

    ISE_API Relation probabilisticGraphToRelation(ProbabilisticSupportGraph const& graph);

    ISE_API Relation supportGraphToRelation(semrel::SupportGraph const& graph);

    ISE_API Relation groundTruthToRelation(std::vector<SupportRelation> const& gt);

    struct ISE_API ProbabilisticSupportEvaluation
    {
        static std::vector<ExtractedRelations> extractRelations(ProbabilisticSupportEvaluationParams const& params,
                Dataset const& dataset,
                RandomEngine& randomEngine);

        static ProbabilisticSupportEvaluationResult evaluate(std::vector<ExtractedRelations> const& relations);

        static ProbabilisticSupportEvaluationResult evaluateOnDataset(ProbabilisticSupportEvaluationParams const& params,
                Dataset const& dataset,
                RandomEngine& randomEngine);

        static void evaluateDistribution(PointCloudToSceneParams const& params,
                Dataset const& dataset,
                RandomEngine& randomEngine);

    };

}
