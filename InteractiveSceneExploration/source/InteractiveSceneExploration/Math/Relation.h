#pragma once

#include <InteractiveSceneExploration/API.h>

#include <vector>

namespace interactive_scene_exploration
{
    struct RelationEntry
    {
        int source = 0;
        int target = 0;

        double probability = 0.0;
    };

    struct ISE_API Relation
    {
        void setEntry(int source, int target, double probability);

        double getEntry(int source, int target) const;

        Relation binarize(double limit) const;

        Relation unionWith(Relation const& other) const;

        static double brierScore(Relation const& prediction, Relation const& groundTruth);

        std::vector<RelationEntry> entries;
    };

}
