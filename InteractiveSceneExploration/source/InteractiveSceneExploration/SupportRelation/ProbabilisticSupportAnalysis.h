#pragma once

#include <InteractiveSceneExploration/API.h>
#include <InteractiveSceneExploration/SupportRelation/ProbabilisticSupportGraph.h>

#include <SemanticObjectRelations/SupportAnalysis/SupportGraph.h>

#include <set>

namespace interactive_scene_exploration
{
    struct SceneDistribution;

    struct SupportAnalysisParams
    {
        Eigen::Vector3f gravityVector = -Eigen::Vector3f::UnitZ();
        float contactMargin = 10.0f;

        float vertSepPlaneAngleMax = 10.0f;
        bool vertSepPlaneAssumeSupport = false;

        bool uncertaintyDetectionEnabled = true;
        float supportAreaRatioMin = 0.7f;

        std::set<int> groundLabels;
    };

    struct ProbabilisticSupportAnalysisParams
    {
        SupportAnalysisParams support;

        int numberOfSamples = 100;
    };

    struct ProbabilisticSupportAnalysisResult
    {
        ProbabilisticSupportGraph graph;

        std::vector<semrel::ShapeList> sceneSamples;
        std::vector<semrel::SupportGraph> supportGraphs;

        semrel::ShapeList deterministicShapes;
        semrel::SupportGraph deterministicSupportGraph;
    };

    struct ISE_API ProbabilisticSupportAnalysis
    {
        static ProbabilisticSupportAnalysisResult analyze(
            SceneDistribution const& scene,
            ProbabilisticSupportAnalysisParams const& params,
            RandomEngine& randomEngine);
    };
}
