#include "Relation.h"

namespace interactive_scene_exploration
{

    void Relation::setEntry(int source, int target, double probability)
    {
        for (auto& entry : entries)
        {
            if (entry.source == source && entry.target == target)
            {
                entry.probability = probability;
                return;
            }
        }
        entries.push_back(RelationEntry{source, target, probability});
    }

    double Relation::getEntry(int source, int target) const
    {
        for (auto& entry : entries)
        {
            if (entry.source == source && entry.target == target)
            {
                return entry.probability;
            }
        }
        return 0.0;
    }

    Relation Relation::binarize(double limit) const
    {
        Relation result;
        for (auto& entry : entries)
        {
            if (entry.probability >= limit)
            {
                result.setEntry(entry.source, entry.target, 1.0);
            }
            else
            {
                result.setEntry(entry.source, entry.target, 0.0);
            }
        }
        return result;
    }

    Relation Relation::unionWith(const Relation& other) const
    {
        Relation result = other;
        for (auto& entry : entries)
        {
            result.setEntry(entry.source, entry.target, entry.probability);
        }
        return result;
    }

    double Relation::brierScore(Relation const& prediction, Relation const& groundTruth)
    {
        Relation all = prediction.unionWith(groundTruth);

        double sum = 0.0;
        for (auto& entry : all.entries)
        {
            double predP = prediction.getEntry(entry.source, entry.target);
            double gtP = groundTruth.getEntry(entry.source, entry.target);

            double error = (predP - gtP);
            sum += error * error;
        }

        double N = all.entries.size();
        return sum / N;
    }

}
