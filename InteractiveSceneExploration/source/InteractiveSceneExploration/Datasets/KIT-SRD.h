#pragma once

#include "Dataset.h"

#include <InteractiveSceneExploration/API.h>

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>

#include <Eigen/Core>

#include <string>
#include <vector>
#include <map>

namespace interactive_scene_exploration::kit_srd
{
    struct RobotState
    {
        Eigen::Matrix4f globalPose;
        std::map<std::string, float> jointConfiguration;
    };

    struct CameraState
    {
        std::string frameName;
        Eigen::Matrix4f globalPose;
    };

    struct Entry
    {
        int number = 0;
        std::string imageRgb;
        std::string imageDepth;
        std::string pcOriginal;
        std::string pcGlobal;
        std::string pcLabeled;
        std::string supportRelations;
    };

    struct Dataset
    {
        std::string directory;
        std::vector<Entry> entries;
    };

    using Point = pcl::PointXYZRGBL;
    using PointCloud = pcl::PointCloud<Point>;

    struct LoadedEntry
    {
        Entry entry;

        int number = 0;

        pcl::PointCloud<pcl::PointXYZRGBA> globalPointCloud;
        PointCloud labeledPointCloud;
        std::vector<SupportRelation> supportRelations;

        PointCloud relabeledPointCloud;
    };

    struct LoadedDataset
    {
        std::string directory;
        std::vector<LoadedEntry> entries;
    };

    ISE_API Dataset findDataset(std::string const& directory);

    ISE_API LoadedDataset loadDataset(std::string const& directory);

}
