#include "SphereFitter.h"

#include <Eigen/Dense>

#include <boost/container/flat_set.hpp>

#include <cfloat>

#include <SimoxUtility/math/SoftMinMax.h>

namespace interactive_scene_exploration
{

    bool SphereFitter::computeModelCoefficients(
        SphereFitter::SampleMatrix const& samples_,
        SphereFitter::ModelVector* modelCoefficients)
    {
        // In temp, each row contains a sample point as homogenous coordinate (w = 1)

        Eigen::Vector3f offset = samples_.row(0);
        SphereFitter::SampleMatrix samples = samples_;
        samples.row(0) -= offset;
        samples.row(1) -= offset;
        samples.row(2) -= offset;
        samples.row(3) -= offset;

        Eigen::Matrix4f temp;
        temp.block<MSS_SIZE, 3>(0, 0) = samples;
        temp.col(3).setOnes();

        // Check whether the points define a sphere
        float m11 = temp.determinant();
        if (std::abs(m11) < 1.0e-3f)
        {
            return false;
        }

        // Points on sphere with center (a, b, c) and radius r fullfil the following equation:
        // | x_1 y_1 z_1 1 |   |-2*a           |   | -x_1^2 - y_1^2 - z_1^2 |
        // | x_2 y_2 z_2 1 |   |-2*b           |   | -x_2^2 - y_2^2 - z_2^2 |
        // |     ...       | * |-2*c           | = |          ...           |
        // | x_n y_n z_n 1 |   |a^2+b^2+c^2-r^2|   | -x_n^2 - y_n^2 - z_n^2 |
        //                            ^^
        //                            ||
        // We want to solve for the middle vector

        Eigen::Vector4f squaredNorms = samples.rowwise().squaredNorm();

        temp.col(0) = squaredNorms;
        float m12 = temp.determinant();

        temp.col(0) = samples.col(0);
        temp.col(1) = squaredNorms;
        float m13 = temp.determinant();

        temp.col(1) = samples.col(1);
        temp.col(2) = squaredNorms;
        float m14 = temp.determinant();

        temp.col(0) = squaredNorms;
        temp.col(1) = samples.col(0);
        temp.col(2) = samples.col(1);
        temp.col(3) = samples.col(2);
        float m15 = temp.determinant();

        float a = 0.5f * m12 / m11;
        float b = 0.5f * m13 / m11;
        float c = 0.5f * m14 / m11;
        float r = std::sqrt(a * a + b * b + c * c - m15 / m11);

        (*modelCoefficients) = ModelVector(a + offset(0), b + offset(1), c + offset(2), r);

        return true;
    }

    SphereFitter::SampleMatrix SphereFitter::selectMSS(const std::vector<Eigen::Vector3f>& points,
            RandomEngine& randomEngine)
    {
        // FIXME: Should we remove this allocation by moving indices into an object?
        boost::container::flat_set<int> indices;
        indices.reserve(MSS_SIZE);

        std::uniform_int_distribution<int> dist(0, points.size() - 1);
        while ((int)indices.size() < MSS_SIZE)
        {
            // add a random index (is ignored if already contained)
            indices.insert(dist(randomEngine));
        }

        SampleMatrix mss;

        int row = 0;
        for (int index : indices)
        {
            mss.row(row) = points[index];
            ++row;
        }

        return mss;
    }

    std::vector<EvaluatedSphere> SphereFitter::sampleModels(const SphereFitterParams& params,
            std::vector<Eigen::Vector3f> const& points,
            RandomEngine& randomEngine)
    {
        std::vector<EvaluatedSphere> result;

        if (points.size() < MSS_SIZE)
        {
            return result;
        }

        unsigned skipped_count = 0;
        // supress infinite loops by just allowing 10 x maximum allowed iterations for invalid model parameters!
        const unsigned max_skip = params.maxIterations * 10;

        result.reserve(points.size());


        for (int iterations_ = 0; iterations_ < params.maxIterations && skipped_count < max_skip; ++iterations_)
        {
            // Get X samples which satisfy the model criteria
            SampleMatrix selection = selectMSS(points, randomEngine);

            // Search for inliers in the point cloud for the current plane model M
            ModelVector model_coefficients = ModelVector::Zero();
            bool hasModelCoefficients = computeModelCoefficients(selection, &model_coefficients);
            if (!hasModelCoefficients)
            {
                --iterations_;
                ++skipped_count;
                continue;
            }

            Eigen::Vector3f position = model_coefficients.head<3>();
            float radius = model_coefficients(3);
            if (radius > params.maxRadius)
            {
                continue;
            }

            semrel::Sphere sphere = semrel::Sphere(position, radius);

            // Now, we have the model and need to calculate the inliers as well as the error
            Evaluation evaluation = evaluate(params, sphere, points);

            EvaluatedSphere& evaluated = result.emplace_back();
            evaluated.shape = sphere;
            evaluated.evaluation = evaluation;
        }

        return result;
    }

    static float SphereErrorFunction(Eigen::Vector3f point,
                                     Eigen::Vector3f center,
                                     float radius)
    {
        // distance center <-> point minus radius
        float distance = (point - center).norm() - radius;
        float error = distance * distance;

        return error;
    }

    Evaluation SphereFitter::evaluate(SphereFitterParams const& params,
                                      semrel::Sphere const& model,
                                      std::vector<Eigen::Vector3f> const& points)
    {
        // allow some points to be ignored for error sum to cope with outliers
        std::size_t numInliers = 0;

        std::vector<float> errors;
        errors.reserve(points.size());
        simox::math::SoftMinMax errorSoftMax(params.outlierRate, points.size());

        float inlierErrorThreshold = params.distanceThreshold * params.distanceThreshold;
        Eigen::Vector3f center = model.getPosition();
        float radius = model.getRadius();
        for (std::size_t i = 0; i < points.size(); ++i)
        {
            Eigen::Vector3f point = points[i];
            float pointError = SphereErrorFunction(point, center, radius);
            if (pointError <= inlierErrorThreshold)
            {
                numInliers++;
            }
            errors.push_back(pointError);
            errorSoftMax.add(pointError);
        }

        float maxError = errorSoftMax.getSoftMax();

        // only add errors <= maxError
        float errorSum = 0;
        for (float pointError : errors)
        {
            errorSum += (pointError <= maxError ? pointError : 0);
        }

        float error = (numInliers > 0 ? errorSum / points.size() : std::numeric_limits<float>::max());

        float coverage = numInliers / float(points.size());

        Evaluation result;
        result.error = error;
        result.inlierCount = numInliers;
        result.coverage = coverage;
        return result;
    }

}
