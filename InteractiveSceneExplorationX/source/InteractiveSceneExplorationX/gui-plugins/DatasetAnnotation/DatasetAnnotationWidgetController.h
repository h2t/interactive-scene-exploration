/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    InteractiveSceneExplorationX::gui-plugins::DatasetAnnotationWidgetController
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <InteractiveSceneExplorationX/gui-plugins/DatasetAnnotation/ui_DatasetAnnotationWidget.h>


#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <InteractiveSceneExploration/Datasets/OSD.h>

namespace armarx
{
    /**
    \page InteractiveSceneExplorationX-GuiPlugins-DatasetAnnotation DatasetAnnotation
    \brief The DatasetAnnotation allows visualizing ...

    \image html DatasetAnnotation.png
    The user can

    API Documentation \ref DatasetAnnotationWidgetController

    \see DatasetAnnotationGuiPlugin
    */

    /**
     * \class DatasetAnnotationWidgetController
     * \brief DatasetAnnotationWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        DatasetAnnotationWidgetController
        : public armarx::ArmarXComponentWidgetControllerTemplate < DatasetAnnotationWidgetController >
        , public armarx::ArVizComponentPluginUser
    {
        Q_OBJECT

    public:
        explicit DatasetAnnotationWidgetController();
        virtual ~DatasetAnnotationWidgetController();

        void loadSettings(QSettings* settings) override;
        void saveSettings(QSettings* settings) override;

        static QString GetWidgetName()
        {
            return "DatasetAnnotation";
        }

        void onInitComponent() override;
        void onConnectComponent() override;

        void onInitGui();
        void onConnectGui();
        void onPointCloudChange(int index);
        void onTransCropChange(double);

    signals:
        /* QT signal declarations */
        void initGui();
        void connectGui();

    private:
        /**
         * Widget Form
         */
        Ui::DatasetAnnotationWidget widget;

        interactive_scene_exploration::osd::LoadedDataset dataset;
    };
}


