/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    InteractiveSceneExplorationX::ArmarXObjects::DatasetViewer
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DatasetViewer.h"

#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>

#include <InteractiveSceneExploration/PointCloud/IO.h>

#include <pcl/surface/convex_hull.h>
#include <pcl/filters/crop_hull.h>

namespace armarx
{
    using namespace interactive_scene_exploration;

    ARMARX_DECOUPLED_REGISTER_COMPONENT(DatasetViewer);

    DatasetViewerPropertyDefinitions::DatasetViewerPropertyDefinitions(std::string prefix) :
        armarx::ComponentPropertyDefinitions(prefix)
    {
        defineOptionalProperty<std::string>("DatasetPath", "InteractiveSceneExplorationX/dataset",
                                            "Path to the dataset");
    }


    std::string DatasetViewer::getDefaultName() const
    {
        return "DatasetViewer";
    }


    void DatasetViewer::onInitComponent()
    {
        std::string relativeDatasetPath = getProperty<std::string>("DatasetPath").getValue();
        std::string absoluteDatasetPath = relativeDatasetPath;
        if (!ArmarXDataPath::getAbsolutePath(relativeDatasetPath, absoluteDatasetPath))
        {
            ARMARX_WARNING << "Could not get absolute path from relative path: " << relativeDatasetPath;
            absoluteDatasetPath = "/home/paus/h2t/home/repos/interactive-scene-exploration/InteractiveSceneExplorationX/data/InteractiveSceneExplorationX/dataset";
        }

        ARMARX_INFO << "Loading dataset from: " << absoluteDatasetPath;
        dataset = kit_srd::loadDataset(absoluteDatasetPath);
        ARMARX_INFO << "Loaded " << dataset.entries.size() << " entries";
    }


    void DatasetViewer::onConnectComponent()
    {
        createRemoteGuiTab();
        RemoteGui_startRunningTask();
    }


    void DatasetViewer::onDisconnectComponent()
    {

    }


    void DatasetViewer::onExitComponent()
    {

    }


    armarx::PropertyDefinitionsPtr DatasetViewer::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new DatasetViewerPropertyDefinitions(
                getConfigIdentifier()));
    }

    void DatasetViewer::createRemoteGuiTab()
    {
        GridLayout grid;
        int row = 0;
        {
            if (dataset.entries.empty())
            {
                tab.entryIndex.setRange(0, 0);
                state.entryIndex = 0;
            }
            else
            {
                tab.entryIndex.setRange(0, dataset.entries.size() - 1);
            }
            tab.entryIndex.setValue(state.entryIndex);
            grid.add(Label("Entry Index"), Pos{row, 0});
            grid.add(tab.entryIndex, Pos{row, 1});
            row += 1;
        }
        {
            tab.xFilter.setValue(state.xFilter);
            grid.add(Label("X Filter:"), Pos{row, 0});
            grid.add(tab.xFilter, Pos{row, 1});
            row += 1;
        }
        {
            tab.xMin.setRange(4000.0f, 6000.0f);
            tab.xMin.setSteps(2000);
            tab.xMin.setValue(state.xMin);
            grid.add(Label("X Min:"), Pos{row, 0});
            grid.add(tab.xMin, Pos{row, 1});
            row += 1;
        }
        {
            tab.xMax.setRange(4000.0f, 6000.0f);
            tab.xMax.setSteps(2000);
            tab.xMax.setValue(state.xMax);
            grid.add(Label("X Max:"), Pos{row, 0});
            grid.add(tab.xMax, Pos{row, 1});
            row += 1;
        }
        {
            tab.yFilter.setValue(state.yFilter);
            grid.add(Label("Y Filter:"), Pos{row, 0});
            grid.add(tab.yFilter, Pos{row, 1});
            row += 1;
        }
        {
            tab.yMin.setRange(4000.0f, 6000.0f);
            tab.yMin.setSteps(2000);
            tab.yMin.setValue(state.yMin);
            grid.add(Label("Y Min:"), Pos{row, 0});
            grid.add(tab.yMin, Pos{row, 1});
            row += 1;
        }
        {
            tab.yMax.setRange(4000.0f, 6000.0f);
            tab.yMax.setSteps(2000);
            tab.yMax.setValue(state.yMax);
            grid.add(Label("Y Max:"), Pos{row, 0});
            grid.add(tab.yMax, Pos{row, 1});
            row += 1;
        }
        {
            tab.zFilter.setValue(state.zFilter);
            grid.add(Label("Z Filter:"), Pos{row, 0});
            grid.add(tab.zFilter, Pos{row, 1});
            row += 1;
        }
        {
            tab.zMin.setRange(500.0f, 2000.0f);
            tab.zMin.setSteps(1500);
            tab.zMin.setValue(state.zMin);
            grid.add(Label("Z Min:"), Pos{row, 0});
            grid.add(tab.zMin, Pos{row, 1});
            row += 1;
        }
        {
            tab.zMax.setRange(500.0f, 2000.0f);
            tab.zMax.setSteps(1500);
            tab.zMax.setValue(state.zMax);
            grid.add(Label("Z Max:"), Pos{row, 0});
            grid.add(tab.zMax, Pos{row, 1});
            row += 1;
        }
        {
            tab.labelFrom.setRange(-1, 10);
            tab.labelFrom.setValue(state.labelFrom);
            grid.add(Label("Label From:"), Pos{row, 0});
            grid.add(tab.labelFrom, Pos{row, 1});
            row += 1;
        }
        {
            tab.labelTo.setRange(-1, 10);
            tab.labelTo.setValue(state.labelFrom);
            grid.add(Label("Label To:"), Pos{row, 0});
            grid.add(tab.labelTo, Pos{row, 1});
            row += 1;
        }
        {
            tab.relabel.setLabel("Relabel");
            grid.add(tab.relabel, Pos{row, 0}, Span{1, 2});
            row += 1;
        }
        {
            tab.save.setLabel("Save");
            grid.add(tab.save, Pos{row, 0}, Span{1, 2});
            row += 1;
        }


        RemoteGui_createTab("DatasetViewer", grid, &tab);
    }

    void DatasetViewer::RemoteGui_update()
    {
        std::lock_guard lock(stateMutex);

        int newEntryIndex = tab.entryIndex.getValue();
        if (newEntryIndex != state.entryIndex)
        {
            if (newEntryIndex >= 0 &&
                newEntryIndex < (int) dataset.entries.size())
            {
                ARMARX_INFO << "New entry selected: " << newEntryIndex;
                state.entryIndex = newEntryIndex;
                selectEntry(state.entryIndex);
            }
        }

        state.xFilter = tab.xFilter.getValue();
        state.xMin = tab.xMin.getValue();
        state.xMax = tab.xMax.getValue();
        state.yFilter = tab.yFilter.getValue();
        state.yMin = tab.yMin.getValue();
        state.yMax = tab.yMax.getValue();
        state.zFilter = tab.zFilter.getValue();
        state.zMin = tab.zMin.getValue();
        state.zMax = tab.zMax.getValue();
        state.labelFrom = tab.labelFrom.getValue();
        state.labelTo = tab.labelTo.getValue();

        if (tab.relabel.wasClicked())
        {
            relabel(state.entryIndex);
        }

        if (tab.save.wasClicked())
        {
            save(state.entryIndex);
        }
    }

    void DatasetViewer::selectEntry(int index)
    {

        // Labeled point cloud
        viz::Layer layerSeg = arviz.layer("PointCloudSeg");
        viz::Layer layerRGB = arviz.layer("PointCloudRGB");

        float pointSizeInPixels = 2.0f;
        viz::PointCloud vizSeg = viz::PointCloud("Points")
                                 .pointSizeInPixels(pointSizeInPixels);
        viz::PointCloud vizRGB = viz::PointCloud("Points")
                                 .pointSizeInPixels(pointSizeInPixels);

        auto& pointCloud = dataset.entries[index].labeledPointCloud;
        for (auto& p : pointCloud)
        {
            viz::ColoredPoint cp;
            cp.x = p.x;
            cp.y = p.y;
            cp.z = p.z;
            cp.color = viz::Color(simox::color::GlasbeyLUT::at(p.label, 255));
            vizSeg.addPoint(cp);

            cp.color.r = p.r;
            cp.color.g = p.g;
            cp.color.b = p.b;
            cp.color.a = 255;
            vizRGB.addPoint(cp);
        }
        layerSeg.add(vizSeg);
        layerRGB.add(vizRGB);

        // Global point cloud
        viz::Layer layerGlobal = arviz.layer("PointCloudGlobal");

        viz::PointCloud vizGlobal = viz::PointCloud("Points")
                                    .pointSizeInPixels(pointSizeInPixels);

        for (auto& p : dataset.entries[index].globalPointCloud)
        {
            viz::ColoredPoint cp;
            cp.x = p.x;
            cp.y = p.y;
            cp.z = p.z;

            cp.color.r = p.r;
            cp.color.g = p.g;
            cp.color.b = p.b;
            cp.color.a = 255;
            vizGlobal.addPoint(cp);
        }
        layerGlobal.add(vizGlobal);

        arviz.commit({layerSeg, layerRGB, layerGlobal});
    }

    void DatasetViewer::relabel(int index)
    {
        ARMARX_INFO << "Relabeling: " << index;

        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr source(new pcl::PointCloud<pcl::PointXYZRGBA>(dataset.entries[index].globalPointCloud));
        auto& segmented = dataset.entries[index].labeledPointCloud;

        pcl::search::KdTree<pcl::PointXYZRGBL>::Ptr searchTree(new pcl::search::KdTree<pcl::PointXYZRGBL>());
        kit_srd::PointCloud::Ptr segmentedPtr(new kit_srd::PointCloud(segmented));
        searchTree->setInputCloud(segmentedPtr);

        std::map<int, kit_srd::PointCloud> segments;
        for (auto& p : segmented)
        {
            segments[p.label].push_back(p);
        }

        ARMARX_INFO << "Segments: " << segments.size();


        const int NEIGHBOR_K = 10;
        std::vector<int> foundIndices(NEIGHBOR_K);
        std::vector<float> foundDistances(NEIGHBOR_K);

        kit_srd::PointCloud output;
        const float sqDistanceMax = 70.0f * 70.0f;

        for (auto& p : *source)
        {
            pcl::PointXYZRGBL searchP;
            searchP.x = p.x;
            searchP.y = p.y;
            searchP.z = p.z;
            searchP.r = p.r;
            searchP.g = p.g;
            searchP.b = p.b;

            int foundCount = searchTree->nearestKSearch(searchP, NEIGHBOR_K, foundIndices, foundDistances);
            if (foundCount > 0)
            {
                std::map<int, int> votes;
                for (int i = 0; i < foundCount; ++i)
                {
                    int index = foundIndices[i];
                    float sqDistance = foundDistances[i];
                    if (sqDistance < sqDistanceMax)
                    {
                        votes[segmented[index].label] += 1;
                    }
                }
                if (votes.size() > 0)
                {
                    int majorityLabel = votes.rbegin()->first;

                    searchP.label = majorityLabel;
                    output.push_back(searchP);
                }
            }
        }

        // Extra checks
        if (state.labelFrom >= 0 && state.labelTo >= 0
            && (state.xFilter || state.yFilter || state.zFilter))
        {
            for (auto& p : output)
            {
                if ((int)p.label != state.labelFrom)
                {
                    continue;
                }

                bool xOk = !state.xFilter || (state.xMin <= p.x && p.x <= state.xMax);
                bool yOk = !state.yFilter || (state.yMin <= p.y && p.y <= state.yMax);
                bool zOk = !state.zFilter || (state.zMin <= p.z && p.z <= state.zMax);
                if (xOk && yOk && zOk)
                {
                    p.label = state.labelTo;
                }
            }
        }

        dataset.entries[index].relabeledPointCloud = output;

        viz::Layer layer = arviz.layer("PointCloudRelabeled");

        viz::PointCloud viz = viz::PointCloud("Points")
                              .pointSizeInPixels(2.0f);

        for (auto& p : output)
        {
            viz::ColoredPoint cp;
            cp.x = p.x;
            cp.y = p.y;
            cp.z = p.z;

            cp.color = viz::Color(simox::color::GlasbeyLUT::at(p.label, 255));
            viz.addPoint(cp);
        }

        layer.add(viz);
        arviz.commit({layer});

#if 0


        for (auto& segment : segments)
        {
            int label = segment.first;
            kit_srd::PointCloud::Ptr pc(new kit_srd::PointCloud(segment.second));

            kit_srd::PointCloud::Ptr ch_points(new kit_srd::PointCloud());
            std::vector<pcl::Vertices> ch_polygons;

            pcl::ConvexHull<pcl::PointXYZRGBL> ch;
            ch.setInputCloud(pc);
            ch.setDimension(3);
            ch.reconstruct(*ch_points, ch_polygons);

            pcl::CropHull<pcl::PointXYZRGBL> crop;
            crop.setHullIndices(ch_polygons);
            crop.setHullCloud(ch_points);
            crop.setCropOutside(true);

            kit_srd::PointCloud output;
            crop.filter(output);

            ARMARX_INFO << "Before: " << pc->size() << ", After: " << output.size();

            viz::PointCloud viz = viz::PointCloud("Points_" + std::to_string(label))
                                  .pointSizeInPixels(2.0f);
            for (auto& p : output)
            {
                viz::ColoredPoint cp;
                cp.x = p.x;
                cp.y = p.y;
                cp.z = p.z;
                cp.color = viz::Color(simox::color::GlasbeyLUT::at(p.label, 255));
                viz.addPoint(cp);
            }

            layer.add(viz);
        }

        arviz.commit({layer});
#endif
    }

    void DatasetViewer::save(int index)
    {
        auto& entry = dataset.entries[index];
        interactive_scene_exploration::PointCloudIO::saveToPCD(entry.entry.pcLabeled,
                entry.relabeledPointCloud);
        ARMARX_INFO << "Saved to " << entry.entry.pcLabeled;
    }
}
