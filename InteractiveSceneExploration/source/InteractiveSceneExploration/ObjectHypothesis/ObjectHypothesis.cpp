#include "ObjectHypothesis.h"

#include <VirtualRobot/math/Helpers.h>

#include <cassert>

namespace interactive_scene_exploration
{

    ObjectHypothesis ObjectHypothesis::fuse(const ObjectHypothesis& other) const
    {
        ObjectHypothesis result = *this;

        // TODO: Implement

        return result;
    }

    static Eigen::Vector3d boltzmann_distribution(Eigen::Vector3d errors, double temperature = 1.0)
    {
        Eigen::Vector3d exps;
        for (int i = 0; i < errors.rows(); ++i)
        {
            exps[i] = std::exp(temperature * errors[i]);
        }
        double sum = exps.sum();
        Eigen::Vector3d probabilities = exps / sum;
        return probabilities;
    }

    void ObjectHypothesis::prepareSampling(float boltzmannTemperature)
    {
        Eigen::Vector3d errors(box.error, cylinder.error, sphere.error);
        Eigen::Vector3d probs = boltzmann_distribution(-errors, boltzmannTemperature);

        box.probability = probs[0];
        cylinder.probability = probs[1];
        sphere.probability = probs[2];
    }

    semrel::ShapePtr ObjectHypothesis::sample(RandomEngine& randomEngine) const
    {
        semrel::ShapePtr result;

        ObjectHypothesisShape shape = sampleShape(randomEngine);
        switch (shape)
        {
            case ObjectHypothesisShape::Box:
            {
                semrel::Box sample = box.sampleOne(randomEngine);
                return std::make_unique<semrel::Box>(sample);
            }
            break;
            case ObjectHypothesisShape::Cylinder:
            {
                semrel::Cylinder sample = cylinder.sampleOne(randomEngine);
                return std::make_unique<semrel::Cylinder>(sample);
            }
            break;
            case ObjectHypothesisShape::Sphere:
            {
                semrel::Sphere sample = sphere.sampleOne(randomEngine);
                return std::make_unique<semrel::Sphere>(sample);
            }
            break;
            case ObjectHypothesisShape::Known:
            {
                throw std::runtime_error("Not yet implemented for known shapes");
            }
            break;
            case ObjectHypothesisShape::NotSet:
            {
                throw std::runtime_error("This should not happen!");
            }
            break;
        }
        throw std::runtime_error("Not yet implemented");
    }

    semrel::ShapePtr ObjectHypothesis::getMeanShape() const
    {
        semrel::ShapePtr result;
        if (box.probability > cylinder.probability)
        {
            if (box.probability > sphere.probability)
            {
                result.reset(new semrel::Box(BoxHypothesis::toShape(box.distribution.mean, id)));
            }
            else
            {
                result.reset(new semrel::Sphere(SphereHypothesis::toShape(sphere.distribution.mean, id)));
            }
        }
        else
        {
            if (cylinder.probability > sphere.probability)
            {
                result.reset(new semrel::Cylinder(CylinderHypothesis::toShape(cylinder.distribution.mean, id)));
            }
            else
            {
                result.reset(new semrel::Sphere(SphereHypothesis::toShape(sphere.distribution.mean, id)));
            }
        }
        return result;
    }

    ObjectHypothesisShape ObjectHypothesis::sampleShape(RandomEngine& randomEngine) const
    {
        // Decide which object should be sampled
        std::uniform_real_distribution<double> dist(0.0, 1.0);
        double sample = dist(randomEngine);

        double accProb = 0.0;
        ObjectHypothesisShape shape = ObjectHypothesisShape::Box;
        accProb += box.probability;
        if (sample < accProb)
        {
            return shape;
        }

        shape = ObjectHypothesisShape::Cylinder;
        accProb += cylinder.probability;
        if (sample < accProb)
        {
            return shape;
        }

        shape = ObjectHypothesisShape::Sphere;
        accProb += sphere.probability;
        // accProb should be 1.0 now
        return shape;
    }

    semrel::Box BoxHypothesis::toShape(State const& state, int id)
    {
        Eigen::Vector3d pos = position(state);
        Eigen::Quaterniond ori = orientationAsQuat(state);
        Eigen::Vector3d ext = extents(state);

        // Normalize quaternion!
        ori.normalize();

        semrel::Box result(pos.cast<float>(), ori.cast<float>(), ext.cast<float>(), semrel::ShapeID{id});
        return result;
    }

    BoxHypothesis::State BoxHypothesis::toState(const semrel::Box& box)
    {
        BoxHypothesis::State state;

        BoxHypothesis::position(state) = box.getPosition().cast<double>();
        Eigen::Quaterniond quat = box.getOrientation().cast<double>();
        // Internal storage format in Eigen is [x, y, z, w]!
        Eigen::Vector4d orientation(quat.x(), quat.y(), quat.z(), quat.w());
        if (quat.w() < 0.0)
        {
            // Prevent double cover from messing with the covariance estimation
            orientation *= -1.0;
        }
        BoxHypothesis::orientation(state) = orientation;
        BoxHypothesis::extents(state) = box.getExtents().cast<double>();

        return state;
    }

    semrel::Box BoxHypothesis::sampleOne(RandomEngine& randomEngine) const
    {
        State sampleState = distribution.sampleOne(randomEngine);
        return toShape(sampleState, id);
    }

    semrel::Cylinder CylinderHypothesis::toShape(State const& state, int id)
    {
        Eigen::Vector3f pos = position(state).cast<float>();
        Eigen::Vector3f dir = direction(state).cast<float>();
        float r = radius(state);
        float h = height(state);

        semrel::Cylinder result(pos, dir, r, h, semrel::ShapeID{id});
        return result;
    }

    CylinderHypothesis::State CylinderHypothesis::toState(const semrel::Cylinder& cyl)
    {
        Eigen::Vector3d pos = cyl.getPosition().cast<double>();
        Eigen::Vector3d dir = cyl.getAxisDirection().cast<double>();
        double r = cyl.getRadius();
        double h = cyl.getHeight();

        State result;
        position(result) = pos;
        direction(result) = dir;
        radius(result) = r;
        height(result) = h;
        return result;
    }

    semrel::Cylinder CylinderHypothesis::sampleOne(RandomEngine& randomEngine) const
    {
        State sampleState = distribution.sampleOne(randomEngine);
        return toShape(sampleState, id);
    }

    semrel::Sphere SphereHypothesis::toShape(State const& state, int id)
    {
        Eigen::Vector3f pos = position(state).cast<float>();
        float r = radius(state);

        return semrel::Sphere(pos, r, semrel::ShapeID{id});
    }

    SphereHypothesis::State SphereHypothesis::toState(const semrel::Sphere& sphere)
    {
        Eigen::Vector3d pos = sphere.getPosition().cast<double>();
        double r = sphere.getRadius();

        State result;
        position(result) = pos;
        radius(result) = r;
        return result;
    }

    semrel::Sphere SphereHypothesis::sampleOne(RandomEngine& randomEngine) const
    {
        State sampleState = distribution.sampleOne(randomEngine);
        return toShape(sampleState, id);
    }

}
