#pragma once

#include "ShapeModels.h"

#include <InteractiveSceneExploration/Math/Gaussian.h>

#include <SemanticObjectRelations/Shapes/Cylinder.h>

#include <Eigen/Core>

namespace interactive_scene_exploration
{



    struct CylinderFitterParams
    {
        float distanceThreshold = 10.0f;
        int maxIterations = 500;
        float outlierRate = 0.01f;
        float maxRadius = 75.0f;
        float minInlierRate = 0.75f;
    };

    struct CylinderFitter
    {
        // With normal information
        static const int MSS_SIZE = 2;
        // [pos(3), dir(3), r, h]
        static const int MODEL_SIZE = 8;

        // Each row contains a sample
        using SampleMatrix = Eigen::Matrix<float, MSS_SIZE, 3>;

        using ModelVector = Eigen::Matrix<float, MODEL_SIZE, 1>;

        static bool computeModelCoefficients(SampleMatrix const& samplePoints,
                                             SampleMatrix const& sampleNormals,
                                             ModelVector* modelCoefficients,
                                             EvaluatedCylinder* more);

        static void selectMSS(std::vector<Eigen::Vector3f> const& points,
                              std::vector<Eigen::Vector3f> const& normals,
                              RandomEngine& randomEngine,
                              SampleMatrix* samplePoints,
                              SampleMatrix* sampleNormals);

        static std::vector<EvaluatedCylinder> sampleModels(CylinderFitterParams const& params,
                std::vector<Eigen::Vector3f> const& points,
                std::vector<Eigen::Vector3f> const& normals,
                RandomEngine& randomEngine);

        static Evaluation evaluate(CylinderFitterParams const& params,
                                   semrel::Cylinder const& model,
                                   std::vector<Eigen::Vector3f> const& points,
                                   std::vector<int>* inlierIndices);
    };

}
