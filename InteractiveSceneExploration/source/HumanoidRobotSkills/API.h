#pragma once

#ifdef __GNUC__
#define HRS_API __attribute__((visibility("default")))
#else
#define HRS_API
#endif
