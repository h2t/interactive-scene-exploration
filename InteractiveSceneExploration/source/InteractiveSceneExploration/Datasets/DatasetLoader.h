#pragma once

#include "Dataset.h"

#include <InteractiveSceneExploration/API.h>

namespace interactive_scene_exploration
{

    enum class DatasetName
    {
        OSD,
        KIT_SR,
    };

    struct ISE_API DatasetLoader
    {
        static Dataset load(DatasetName name, std::string const& path);
    };

}
