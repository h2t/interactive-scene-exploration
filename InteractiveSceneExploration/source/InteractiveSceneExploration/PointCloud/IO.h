#pragma once

#include <InteractiveSceneExploration/API.h>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

namespace interactive_scene_exploration
{

    struct ISE_API PointCloudIO
    {
        static bool loadFromPCD(std::string const& filename,
                                pcl::PointCloud<pcl::PointXYZRGBL>* outPointCloud);

        static bool loadFromPCD(std::string const& filename,
                                pcl::PointCloud<pcl::PointXYZRGBA>* outPointCloud);

        static bool saveToPCD(std::string const& filename,
                              pcl::PointCloud<pcl::PointXYZRGBL> const& pointCloud);
    };

}
