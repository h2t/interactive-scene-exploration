#include "Dataset.h"

#include <cstdio>

namespace interactive_scene_exploration
{

    static std::string readEntireFile(std::string const& filename)
    {
        std::FILE* f = std::fopen(filename.c_str(), "rb");
        std::fseek(f, 0, SEEK_END);
        long fsize = std::ftell(f);
        std::fseek(f, 0, SEEK_SET);

        std::string result(fsize + 1, '\0');
        std::size_t readBytes = std::fread(&result[0], 1, fsize, f);
        std::fclose(f);

        if ((long)readBytes != fsize)
        {
            throw std::runtime_error("Could not read file completely: " + filename);
        }

        result[fsize] = '\0';

        return result;
    }

    static std::vector<SupportRelation> parseSupportRelations(std::string const& csv)
    {
        std::vector<SupportRelation> result;

        int pos = 0;
        int csvSize = (int)csv.size();
        while (pos < csvSize)
        {
            std::size_t comma = csv.find(',', pos);
            if (comma == std::string::npos)
            {
                break;
            }
            std::size_t endOfLine = csv.find('\n', pos);
            if (endOfLine == std::string::npos)
            {
                endOfLine = csv.size();
            }

            std::string source = csv.substr(pos, comma - pos);
            std::string target = csv.substr(comma + 1, endOfLine - comma - 1);

            pos = endOfLine + 1;

            if (source == "source")
            {
                continue;
            }

            try
            {
                int sourceI = std::stoi(source);
                int targetI = std::stoi(target);

                result.push_back(SupportRelation{sourceI, targetI});
            }
            catch (std::exception const& ex)
            {
                std::cerr << "Error while converting strings to integers\n"
                          << "Source: " << source << ", Target: " << target << "\n";
            }
        }

        return result;
    }

    std::vector<SupportRelation> readSupportRelationsFromCSV(std::string const& filePath)
    {
        std::string csv = readEntireFile(filePath);
        return parseSupportRelations(csv);
    }


}
