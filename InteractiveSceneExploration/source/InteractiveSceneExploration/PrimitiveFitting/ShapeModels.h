#pragma once

#include <SemanticObjectRelations/Shapes/Box.h>
#include <SemanticObjectRelations/Shapes/Sphere.h>
#include <SemanticObjectRelations/Shapes/Cylinder.h>

namespace interactive_scene_exploration
{

    struct Evaluation
    {
        float error = 1.0e5f;
        int inlierCount = 0;
        float coverage = 0.0f;
    };

    template <typename ShapeT>
    struct EvaluatedShape
    {
        ShapeT shape;
        Evaluation evaluation;
    };

    using EvaluatedBox = EvaluatedShape<semrel::Box>;
    using EvaluatedSphere = EvaluatedShape<semrel::Sphere>;
    //using EvaluatedCylinder = EvaluatedShape<semrel::Cylinder>;

    struct EvaluatedCylinder : EvaluatedShape<semrel::Cylinder>
    {
        Eigen::Vector3f p1;
        Eigen::Vector3f n1;
        Eigen::Vector3f p2;
        Eigen::Vector3f n2;

        Eigen::Vector3f pointL1;
        Eigen::Vector3f pointL2;
        Eigen::Vector3f direction;
    };

}
