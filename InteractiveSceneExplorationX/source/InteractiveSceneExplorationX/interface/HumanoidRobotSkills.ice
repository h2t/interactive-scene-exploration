#pragma once

#include <RobotAPI/interface/units/PlatformUnitInterface.ice>
#include <RobotAPI/interface/units/KinematicUnitInterface.ice>

module armarx
{

interface HumanoidRobotListenerInterface extends PlatformUnitListener, KinematicUnitListener
{

};

}
