#include "ObjectFitter.h"

#include <iostream>
#include <cfloat>

namespace interactive_scene_exploration
{

    ObjectExtractionResult ObjectFitter::extractObject(
        ObjectFitterParams const& params,
        int label,
        std::vector<Eigen::Vector3f> const& points,
        std::vector<Eigen::Vector3f> const& normals,
        RandomEngine& randomEngine)
    {
        ObjectExtractionResult result;

        // Box
        result.boxes = extractBoxes(params.box, points, randomEngine);
        std::optional<BoxHypothesis> box = estimateBox(result.boxes);
        if (box)
        {
            box->id = label;
            result.object.box = *box;
        }
        else
        {
            result.object.box.error = 1.0e5;
        }

        // Cylinder
        result.cylinders = extractCylinders(params.cylinder, points, normals, randomEngine);
        std::optional<CylinderHypothesis> cylinder = estimateCylinder(result.cylinders);
        if (cylinder)
        {
            cylinder->id = label;
            result.object.cylinder = *cylinder;
        }
        else
        {
            result.object.cylinder.error = 1.0e5;
        }

        // Sphere
        result.spheres = extractSpheres(params.sphere, points, randomEngine);
        std::optional<SphereHypothesis> sphere = estimateSphere(result.spheres);
        if (sphere)
        {
            sphere->id = label;
            result.object.sphere = *sphere;
        }
        else
        {
            result.object.sphere.error = 1.0e5;
        }

        result.object.id = label;
        result.object.prepareSampling(params.boltzmannTemperature);

        return result;
    }

    std::vector<EvaluatedBox> ObjectFitter::extractBoxes(
        BoxFitterParams const& params,
        std::vector<Eigen::Vector3f> const& points,
        RandomEngine& randomEngine)
    {
        std::vector<EvaluatedBox> samples = BoxFitter::sampleModels(params, points, randomEngine);
        return samples;
    }

    std::optional<BoxHypothesis> ObjectFitter::estimateBox(const std::vector<EvaluatedBox>& boxes)
    {
        auto result = estimateDistribution(boxes);
        if (result)
        {
            // Replace the mean with the best estimate
            float bestError = FLT_MAX;
            semrel::Box bestBox;
            for (EvaluatedBox const& box : boxes)
            {
                if (box.evaluation.error < bestError)
                {
                    bestError = box.evaluation.error;
                    bestBox = box.shape;
                }
            }
            BoxHypothesis::State& mean = result->distribution.mean;
            mean = BoxHypothesis::toState(bestBox);

            result->error = bestError;
        }
        return result;
    }

    std::optional<BoxHypothesis> ObjectFitter::estimateDistribution(const std::vector<EvaluatedBox>& evaluatedModels)
    {
        int sampleSize = evaluatedModels.size();
        if (sampleSize <= 0)
        {
            return std::nullopt;
        }

        std::vector<BoxHypothesis::State> samples(sampleSize);
        for (int i = 0; i < sampleSize; ++i)
        {
            auto& eb = evaluatedModels[i];
            samples[i] = BoxHypothesis::toState(eb.shape);
        }

        // Now we have a set of samples and need to generate mean and covariance
        // TODO: Extract into a reusable function (once we have cylinders and spheres as well)

        // Mean
        BoxHypothesis::State sum;
        sum.setZero();
        for (int i = 0; i < sampleSize; ++i)
        {
            sum += samples[i];
        }
        BoxHypothesis::State mean = sum / sampleSize;

        BoxHypothesis::State factor;
        factor.setOnes();
        factor[0] = 0.5;
        factor[1] = 0.5;
        factor[2] = 0.5;
        factor[3] = 0.1;
        factor[4] = 0.1;
        factor[5] = 0.1;
        factor[6] = 0.1;
        factor[7] = 0.4;
        factor[8] = 0.4;
        factor[9] = 0.4;

        // Covariance
        BoxHypothesis::Covariance covariance;
        if (sampleSize > 1)
        {
            for (int j = 0; j < BoxHypothesis::STATE_SIZE; j++)
            {
                for (int k = 0; k < BoxHypothesis::STATE_SIZE; k++)
                {
                    covariance(j, k) = 0.0;
                    for (BoxHypothesis::State sample :  samples)
                    {
                        covariance(j, k) += factor[j] * (sample[j] - mean[j]) * factor[k] * (sample[k] - mean[k]);
                        // covariance(j, k) += (sample[j] - mean[j]) * (sample[k] - mean[k]);
                    }
                    covariance(j, k) /= sampleSize - 1;
                }
            }
        }
        else
        {
            covariance.setIdentity();
        }

        for (int j = 0; j < BoxHypothesis::STATE_SIZE; j++)
        {
            for (int k = 0; k < j; k++)
            {
                double diff = covariance(j, k) - covariance(k, j);
                if (diff > 1.0e-4)
                {
                    std::cerr << "Error in symmetry (" << j << ", " << k << "):\n"
                              << covariance(j, k) << "\n"
                              << covariance(k, j) << "\n";
                }
                covariance(k, j) = covariance(j, k);
            }
        }

        if (!covariance.isApprox(covariance.transpose(), 1.0e-4))
        {
            std::cerr << "Covariance is not symmetric!\n";
        }
        Eigen::LLT<Eigen::MatrixXd> A_llt(covariance);
        if (A_llt.info() == Eigen::NumericalIssue)
        {
            std::cout << "Covariance is not positive semi-definite!\n";

            for (int j = 0; j < BoxHypothesis::STATE_SIZE; j++)
            {
                for (int k = 0; k < BoxHypothesis::STATE_SIZE; k++)
                {
                    std::cout << covariance(j, k) << ",";
                }
                std::cout << " ; ";
            }
            std::cout << std::endl;
        }

        // Why can a symmetric covariance matrix be not positive semi-definite?

        BoxHypothesis result;

        result.distribution.mean = mean;
        result.distribution.covariance = covariance;

        return result;
    }

    std::vector<EvaluatedCylinder> ObjectFitter::extractCylinders(
        CylinderFitterParams const& params,
        std::vector<Eigen::Vector3f> const& points,
        std::vector<Eigen::Vector3f> const& normals,
        RandomEngine& randomEngine)
    {
        // We need the normals as well
        //        pcl::NormalEstimation<pcl::PointXYZRGBL, pcl::Normal> normalEstimation;
        //        normalEstimation.setInputCloud(pointCloud);

        //        pcl::search::KdTree<pcl::PointXYZRGBL>::Ptr searchTree(new pcl::search::KdTree<pcl::PointXYZRGBL>());
        //        normalEstimation.setSearchMethod(searchTree);
        //        normalEstimation.setKSearch(40);

        //        pcl::PointCloud<pcl::Normal>::Ptr pcNormals(new pcl::PointCloud<pcl::Normal>());
        //        normalEstimation.compute(*pcNormals);

        //        std::vector<Eigen::Vector3f> normals(pointCloud->size());
        //        for (int i = 0; i < pointCloud->size(); ++i)
        //        {
        //            normals[i] = pcNormals->at(i).getNormalVector3fMap();
        //        }

        std::vector<EvaluatedCylinder> samples = CylinderFitter::sampleModels(params, points, normals, randomEngine);

        return samples;
    }

    std::optional<CylinderHypothesis> ObjectFitter::estimateDistribution(const std::vector<EvaluatedCylinder>& evaluatedModels)
    {
        using Hypothesis = CylinderHypothesis;
        using State = Hypothesis::State;
        using Covariance = Hypothesis::Covariance;
        static const int STATE_SIZE = Hypothesis::STATE_SIZE;


        // TODO: Extract this to a common method!
        int sampleSize = evaluatedModels.size();
        if (sampleSize <= 0)
        {
            return std::nullopt;
        }

        std::vector<State> samples(sampleSize);
        for (int i = 0; i < sampleSize; ++i)
        {
            auto& em = evaluatedModels[i];
            samples[i] = Hypothesis::toState(em.shape);
            if (!std::isfinite(samples[i].x()))
            {
                std::cout << "Shape: " << em.shape.getPosition().transpose()
                          << ", " << em.shape.getAxisDirection().transpose()
                          << ", " << em.shape.getRadius() << "\n";
                std::cout << samples[i].transpose() << "\n";
            }
        }

        // Now we have a set of samples and need to generate mean and covariance
        // TODO: Extract into a reusable function (once we have cylinders and spheres as well)

        // Mean
        State sum;
        sum.setZero();
        for (int i = 0; i < sampleSize; ++i)
        {
            sum += samples[i];
        }
        State mean = sum / sampleSize;

        State factor;
        factor[0] = 0.2;
        factor[1] = 0.2;
        factor[2] = 0.2;
        factor[3] = 0.2;
        factor[4] = 0.2;
        factor[5] = 0.2;
        factor[6] = 0.2;
        factor[7] = 0.2;

        // Covariance
        Covariance covariance;
        if (sampleSize > 1)
        {
            for (int j = 0; j < STATE_SIZE; j++)
            {
                for (int k = 0; k < STATE_SIZE; k++)
                {
                    covariance(j, k) = 0.0;
                    for (State sample :  samples)
                    {
                        covariance(j, k) += factor[j] * (sample[j] - mean[j]) * factor[k] * (sample[k] - mean[k]);
                    }
                    covariance(j, k) /= sampleSize - 1;
                }
            }
        }
        else
        {
            covariance.setIdentity();
        }

        for (int j = 0; j < STATE_SIZE; j++)
        {
            for (int k = 0; k < j; k++)
            {
                double diff = covariance(j, k) - covariance(k, j);
                if (diff > 1.0e-4)
                {
                    std::cerr << "Error in symmetry (" << j << ", " << k << "):\n"
                              << covariance(j, k) << "\n"
                              << covariance(k, j) << "\n";
                }
                covariance(k, j) = covariance(j, k);
            }
        }

        if (!covariance.isApprox(covariance.transpose(), 1.0e-4))
        {
            std::cerr << "Covariance is not symmetric!\n";
            std::cout << "[";
            for (int j = 0; j < STATE_SIZE; j++)
            {
                for (int k = 0; k < STATE_SIZE; k++)
                {
                    std::cout << covariance(j, k);
                    if (k < STATE_SIZE - 1)
                    {
                        std::cout << ",";
                    }
                }
                if (j < STATE_SIZE - 1)
                {
                    std::cout << " ; ";
                }
            }
            std::cout << "]" << std::endl;
        }
        Eigen::LLT<Covariance> A_llt(covariance);
        if (A_llt.info() == Eigen::NumericalIssue)
        {
            std::cout << "Covariance is not positive semi-definite!\n";

            for (int j = 0; j < STATE_SIZE; j++)
            {
                for (int k = 0; k < STATE_SIZE; k++)
                {
                    std::cout << covariance(j, k) << ",";
                }
                std::cout << " ; ";
            }
            std::cout << std::endl;
        }

        // Why can a symmetric covariance matrix be not positive semi-definite?

        Hypothesis result;

        result.distribution.mean = mean;
        result.distribution.covariance = covariance;

        return result;
    }

    std::optional<CylinderHypothesis> ObjectFitter::estimateCylinder(const std::vector<EvaluatedCylinder>& samples)
    {
        auto result = estimateDistribution(samples);
        if (result)
        {
            // Replace the mean with the best estimate
            float bestError = FLT_MAX;
            semrel::Cylinder bestSample;
            for (auto const& sample : samples)
            {
                if (sample.evaluation.error < bestError)
                {
                    bestError = sample.evaluation.error;
                    bestSample = sample.shape;
                }
            }
            auto& mean = result->distribution.mean;
            mean = CylinderHypothesis::toState(bestSample);

            result->error = bestError;
        }
        return result;
    }

    std::vector<EvaluatedSphere> ObjectFitter::extractSpheres(
        SphereFitterParams const& params,
        std::vector<Eigen::Vector3f> const& points,
        RandomEngine& randomEngine)
    {
        std::vector<EvaluatedSphere> samples = SphereFitter::sampleModels(params, points, randomEngine);
        return samples;
    }

    std::optional<SphereHypothesis> ObjectFitter::estimateSphere(std::vector<EvaluatedSphere> const& samples)
    {
        auto result = estimateDistribution(samples);
        if (result)
        {
            // Replace the mean with the best estimate
            float bestError = FLT_MAX;
            semrel::Sphere bestSample;
            for (EvaluatedSphere const& sample : samples)
            {
                if (sample.evaluation.error < bestError)
                {
                    bestError = sample.evaluation.error;
                    bestSample = sample.shape;
                }
            }
            SphereHypothesis::State& mean = result->distribution.mean;
            mean = SphereHypothesis::toState(bestSample);

            result->error = bestError;
        }
        return result;
    }

    std::optional<SphereHypothesis> ObjectFitter::estimateDistribution(std::vector<EvaluatedSphere> const& evaluatedModels)
    {
        using Hypothesis = SphereHypothesis;
        using State = Hypothesis::State;
        using Covariance = Hypothesis::Covariance;
        static const int STATE_SIZE = Hypothesis::STATE_SIZE;


        // TODO: Extract this to a common method!
        int sampleSize = evaluatedModels.size();
        if (sampleSize <= 0)
        {
            return std::nullopt;
        }

        std::vector<State> samples(sampleSize);
        for (int i = 0; i < sampleSize; ++i)
        {
            auto& em = evaluatedModels[i];
            samples[i] = Hypothesis::toState(em.shape);
        }

        // Now we have a set of samples and need to generate mean and covariance
        // TODO: Extract into a reusable function (once we have cylinders and spheres as well)

        // Mean
        State sum;
        sum.setZero();
        for (int i = 0; i < sampleSize; ++i)
        {
            sum += samples[i];
        }
        State mean = sum / sampleSize;

        State factor;
        factor.setOnes();
        factor[0] = 0.5;
        factor[1] = 0.5;
        factor[2] = 0.5;
        factor[3] = 0.5;

        // Covariance
        Covariance covariance;
        if (sampleSize > 1)
        {
            for (int j = 0; j < STATE_SIZE; j++)
            {
                for (int k = 0; k < STATE_SIZE; k++)
                {
                    covariance(j, k) = 0.0;
                    for (State sample :  samples)
                    {
                        covariance(j, k) += factor[j] * (sample[j] - mean[j]) * factor[k] * (sample[k] - mean[k]);
                    }
                    covariance(j, k) /= sampleSize - 1;
                }
            }
        }
        else
        {
            covariance.setIdentity();
        }

        for (int j = 0; j < STATE_SIZE; j++)
        {
            for (int k = 0; k < j; k++)
            {
                double diff = covariance(j, k) - covariance(k, j);
                if (diff > 1.0e-4)
                {
                    std::cerr << "Error in symmetry (" << j << ", " << k << "):\n"
                              << covariance(j, k) << "\n"
                              << covariance(k, j) << "\n";
                }
                covariance(k, j) = covariance(j, k);
            }
        }

        if (!covariance.isApprox(covariance.transpose(), 1.0e-4))
        {
            std::cerr << "Covariance is not symmetric!\n";
            std::cout << "[";
            for (int j = 0; j < STATE_SIZE; j++)
            {
                for (int k = 0; k < STATE_SIZE; k++)
                {
                    std::cout << covariance(j, k);
                    if (k < STATE_SIZE - 1)
                    {
                        std::cout << ",";
                    }
                }
                if (j < STATE_SIZE - 1)
                {
                    std::cout << " ; ";
                }
            }
            std::cout << "]" << std::endl;
        }
        Eigen::LLT<Covariance> A_llt(covariance);
        if (A_llt.info() == Eigen::NumericalIssue)
        {
            std::cout << "Covariance is not positive semi-definite!\n";

            for (int j = 0; j < STATE_SIZE; j++)
            {
                for (int k = 0; k < STATE_SIZE; k++)
                {
                    std::cout << covariance(j, k) << ",";
                }
                std::cout << " ; ";
            }
            std::cout << std::endl;
        }

        // Why can a symmetric covariance matrix be not positive semi-definite?

        Hypothesis result;

        result.distribution.mean = mean;
        result.distribution.covariance = covariance;

        return result;
    }


}
