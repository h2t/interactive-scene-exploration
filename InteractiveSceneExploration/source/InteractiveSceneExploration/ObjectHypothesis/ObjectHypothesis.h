#pragma once

#include <InteractiveSceneExploration/API.h>
#include <InteractiveSceneExploration/Math/Gaussian.h>

#include <SemanticObjectRelations/Shapes/Box.h>
#include <SemanticObjectRelations/Shapes/Cylinder.h>
#include <SemanticObjectRelations/Shapes/Sphere.h>


#include <Eigen/Core>
#include <Eigen/Dense>
#include <Eigen/Geometry>

#include <memory>

namespace interactive_scene_exploration
{

    enum class ObjectHypothesisShape
    {
        NotSet,

        Known,
        Box,
        Cylinder,
        Sphere,
    };

    struct ShapeHypothesis
    {
        int id = 0;
        double error = 0.0;
        double probability = 0.0;
    };

    struct ISE_API BoxHypothesis : ShapeHypothesis
    {
        // Position(3), Orientation(4), Extents(3)
        static const int STATE_SIZE = 3 + 4 + 3;
        using GaussianT = Gaussian<STATE_SIZE>;
        using State = GaussianT::State;
        using Covariance = GaussianT::Covariance;
        GaussianT distribution;

        static auto position(State& state)
        {
            return state.block<3, 1>(0, 0);
        }

        static auto orientation(State& state)
        {
            return state.block<4, 1>(3, 0);
        }

        static auto extents(State& state)
        {
            return state.block<3, 1>(7, 0);
        }

        static auto position(State const& state)
        {
            return state.block<3, 1>(0, 0);
        }

        static auto orientation(State const& state)
        {
            return state.block<4, 1>(3, 0);
        }

        static auto extents(State const& state)
        {
            return state.block<3, 1>(7, 0);
        }

        static Eigen::Quaterniond orientationAsQuat(State const& state)
        {
            auto ori = orientation(state);
            Eigen::Quaterniond result(ori);
            return result;
        }

        static semrel::Box toShape(State const& state, int id);

        static State toState(semrel::Box const& box);


        semrel::Box sampleOne(RandomEngine& randomEngine) const;
    };

    struct ISE_API CylinderHypothesis : ShapeHypothesis
    {
        // Position(3), Direction(3), Height(1), Radius(1)
        static const int STATE_SIZE = 3 + 3 + 1 + 1;
        using GaussianT = Gaussian<STATE_SIZE>;
        using State = GaussianT::State;
        using Covariance = GaussianT::Covariance;
        GaussianT distribution;

        static auto position(State& state)
        {
            return state.block<3, 1>(0, 0);
        }

        static auto direction(State& state)
        {
            return state.block<3, 1>(3, 0);
        }

        static double& radius(State& state)
        {
            return state(6, 0);
        }

        static double& height(State& state)
        {
            return state(7, 0);
        }

        static auto position(State const& state)
        {
            return state.block<3, 1>(0, 0);
        }

        static auto direction(State const& state)
        {
            return state.block<3, 1>(3, 0);
        }

        static double const& radius(State const& state)
        {
            return state(6, 0);
        }

        static double const& height(State const& state)
        {
            return state(7, 0);
        }

        static semrel::Cylinder toShape(State const& state, int id);

        static State toState(semrel::Cylinder const& cyl);

        semrel::Cylinder sampleOne(RandomEngine& randomEngine) const;
    };

    struct ISE_API SphereHypothesis : ShapeHypothesis
    {
        // Position(3), Radius(1)
        static const int STATE_SIZE = 3 + 1;
        using GaussianT = Gaussian<STATE_SIZE>;
        using State = GaussianT::State;
        using Covariance = GaussianT::Covariance;
        GaussianT distribution;

        static auto position(State& state)
        {
            return state.block<3, 1>(0, 0);
        }

        static double& radius(State& state)
        {
            return state(3, 0);
        }

        static auto position(State const& state)
        {
            return state.block<3, 1>(0, 0);
        }

        static double const& radius(State const& state)
        {
            return state(3, 0);
        }

        static semrel::Sphere toShape(State const& state, int id);

        static State toState(semrel::Sphere const& cyl);

        semrel::Sphere sampleOne(RandomEngine& randomEngine) const;
    };

    struct ISE_API ObjectHypothesis
    {
        int id = 0;

        BoxHypothesis box;
        CylinderHypothesis cylinder;
        SphereHypothesis sphere;

        void prepareSampling(float boltzmannTemperature);

        ObjectHypothesis fuse(ObjectHypothesis const& other) const;

        semrel::ShapePtr getMeanShape() const;

        semrel::ShapePtr sample(RandomEngine& randomEngine) const;


        ObjectHypothesisShape sampleShape(RandomEngine& randomEngine) const;
    };



}
