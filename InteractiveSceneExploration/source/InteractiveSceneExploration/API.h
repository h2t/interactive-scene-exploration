#pragma once

#ifdef __GNUC__
#define ISE_API __attribute__((visibility("default")))
#else
#define ISE_API
#endif
