#include "ProbabilisticSupportGraph.h"

#include <SemanticObjectRelations/SupportAnalysis/json.h>
#include <SemanticObjectRelations/RelationGraph/json/attributes.h>
#include <SemanticObjectRelations/RelationGraph/json/AttributedGraph.h>

namespace interactive_scene_exploration
{

    std::string ProbabilisticSupportGraph::strVertex(ProbabilisticSupportGraph::ConstVertex v) const
    {
        return "";
    }

    std::string ProbabilisticSupportGraph::strEdge(ProbabilisticSupportGraph::ConstEdge e) const
    {
        return "";
    }

    void to_json(nlohmann::json& j, const ProbabilisticSupportVertex& vertex)
    {
        semrel::json::to_json_base(j, vertex);

        j["object"] = vertex.object;
    }

    void from_json(const nlohmann::json& j, ProbabilisticSupportVertex& vertex)
    {
        semrel::json::from_json_base(j, vertex);
        j["object"].get_to(vertex.object);
    }

    void to_json(nlohmann::json& j, const ProbabilisticSupportEdge& edge)
    {
        j["geometricExistenceProbability"] = edge.geometricExistenceProbability;
        j["topDownExistenceProbability"] = edge.topDownExistenceProbability;
        j["topDownSupport"] = edge.topDownSupport;
        j["meanSupportAreaOfTopDown"] = edge.meanSupportAreaOfTopDown;
    }

    void from_json(const nlohmann::json& j, ProbabilisticSupportEdge& edge)
    {
        j["geometricExistenceProbability"].get_to(edge.geometricExistenceProbability);
        j["topDownExistenceProbability"].get_to(edge.topDownExistenceProbability);
        j["topDownSupport"].get_to(edge.topDownSupport);
        j["meanSupportAreaOfTopDown"].get_to(edge.meanSupportAreaOfTopDown);
    }

    void to_json(nlohmann::json& j, const BoxHypothesis& object)
    {
        j["error"] = object.error;
        j["probability"] = object.probability;
        j["distribution"]["mean"] = object.distribution.mean;
        j["distribution"]["covariance"] = object.distribution.covariance;
    }

    void from_json(const nlohmann::json& j, BoxHypothesis& object)
    {
        j["error"].get_to(object.error);
        j["probability"].get_to(object.probability);
        j["distribution"]["mean"].get_to(object.distribution.mean);
        j["distribution"]["covariance"].get_to(object.distribution.covariance);
    }

    void to_json(nlohmann::json& j, const CylinderHypothesis& object)
    {
        j["error"] = object.error;
        j["probability"] = object.probability;
        j["distribution"]["mean"] = object.distribution.mean;
        j["distribution"]["covariance"] = object.distribution.covariance;
    }

    void from_json(const nlohmann::json& j, CylinderHypothesis& object)
    {
        j["error"].get_to(object.error);
        j["probability"].get_to(object.probability);
        j["distribution"]["mean"].get_to(object.distribution.mean);
        j["distribution"]["covariance"].get_to(object.distribution.covariance);
    }

    void to_json(nlohmann::json& j, const SphereHypothesis& object)
    {
        j["error"] = object.error;
        j["probability"] = object.probability;
        j["distribution"]["mean"] = object.distribution.mean;
        j["distribution"]["covariance"] = object.distribution.covariance;
    }

    void from_json(const nlohmann::json& j, SphereHypothesis& object)
    {
        j["error"].get_to(object.error);
        j["probability"].get_to(object.probability);
        j["distribution"]["mean"].get_to(object.distribution.mean);
        j["distribution"]["covariance"].get_to(object.distribution.covariance);
    }

    void to_json(nlohmann::json& j, const ObjectHypothesis& object)
    {
        j["id"] = object.id;
        j["box"] = object.box;
        j["cylinder"] = object.cylinder;
        j["sphere"] = object.sphere;
    }

    void from_json(const nlohmann::json& j, ObjectHypothesis& object)
    {
        j["id"].get_to(object.id);
        j["box"].get_to(object.box);
        j["cylinder"].get_to(object.cylinder);
        j["sphere"].get_to(object.sphere);
    }

}
