/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    InteractiveSceneExplorationX::gui-plugins::DatasetAnnotationWidgetController
 * \author     Fabian Paus ( fabian dot paus at kit dot edu )
 * \date       2020
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DatasetAnnotationWidgetController.h"

#include <SimoxUtility/math/convert/rpy_to_mat3f.h>

#include <string>

namespace armarx
{
    using namespace interactive_scene_exploration;

    DatasetAnnotationWidgetController::DatasetAnnotationWidgetController()
    {
        widget.setupUi(getWidget());

        using This = DatasetAnnotationWidgetController;
        connect(this, &This::initGui, this, &This::onInitGui);
        connect(this, &This::connectGui, this, &This::onConnectGui);
        connect(widget.pointCloudComboBox, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &This::onPointCloudChange);

        connect(widget.trans_rollSpinBox, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &This::onTransCropChange);
        connect(widget.trans_pitchSpinBox, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &This::onTransCropChange);
        connect(widget.trans_yawSpinBox, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &This::onTransCropChange);
        connect(widget.crop_minX, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &This::onTransCropChange);
        connect(widget.crop_minY, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &This::onTransCropChange);
        connect(widget.crop_minZ, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &This::onTransCropChange);
        connect(widget.crop_maxX, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &This::onTransCropChange);
        connect(widget.crop_maxY, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &This::onTransCropChange);
        connect(widget.crop_maxZ, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &This::onTransCropChange);
    }


    DatasetAnnotationWidgetController::~DatasetAnnotationWidgetController()
    {

    }


    void DatasetAnnotationWidgetController::loadSettings(QSettings* settings)
    {

    }

    void DatasetAnnotationWidgetController::saveSettings(QSettings* settings)
    {

    }

    void DatasetAnnotationWidgetController::onInitComponent()
    {

    }

    void DatasetAnnotationWidgetController::onInitGui()
    {

    }

    void DatasetAnnotationWidgetController::onConnectGui()
    {
        QComboBox* cb = widget.pointCloudComboBox;

        cb->blockSignals(true);
        cb->clear();

        for (auto& pc : dataset.pointClouds)
        {
            std::string prefix = osd::prefixToString(pc.prefix);
            std::string number = std::to_string(pc.number);
            std::string pcName = prefix + number;
            QString qPcName = QString::fromStdString(pcName);

            cb->addItem(qPcName);
        }

        cb->blockSignals(false);
    }


    static viz::Color glasbeyLUT(int id, int alpha)
    {
        simox::Color sc = simox::color::GlasbeyLUT::at(id, alpha);
        viz::Color c;
        c.r = sc.r;
        c.g = sc.g;
        c.b = sc.b;
        c.a = sc.a;
        return c;
    }

    void DatasetAnnotationWidgetController::onPointCloudChange(int index)
    {
        int dsSize = (int)dataset.pointClouds.size();
        if (index >= dsSize)
        {
            ARMARX_WARNING << "Invalid index " << index << ", given size: " << dsSize;
            return;
        }

        osd::PointCloud newPC;
        // Transformation & Cropping
        {
            auto& pc = dataset.pointClouds[index];

#if 0
            float roll = widget.trans_rollSpinBox->value();
            float pitch = widget.trans_pitchSpinBox->value();
            float yaw = widget.trans_yawSpinBox->value();

            float minX = 0.001f * widget.crop_minX->value();
            float minY = 0.001f * widget.crop_minY->value();
            float minZ = 0.001f * widget.crop_minZ->value();
            float maxX = 0.001f * widget.crop_maxX->value();
            float maxY = 0.001f * widget.crop_maxY->value();
            float maxZ = 0.001f * widget.crop_maxZ->value();
#else
            float roll = pc.rotation.roll;
            float pitch = pc.rotation.pitch;
            float yaw = pc.rotation.yaw;

            float minX = pc.crop.minX;
            float minY = pc.crop.minY;
            float maxX = pc.crop.maxX;
            float maxY = pc.crop.maxY;
#endif
            Eigen::Matrix3f rotation = simox::math::rpy_to_mat3f(roll, pitch, yaw);

            auto& points = pc.pointCloud.points;
            int pointsSize = (int)points.size();
            for (int i = 0; i < pointsSize; ++i)
            {
                auto& point = points[i];
                Eigen::Vector3f local = point.getVector3fMap();
                Eigen::Vector3f global = rotation * local;
                if (minX <= global.x() && global.x() <= maxX &&
                    minY <= global.y() && global.y() <= maxY)
                {
                    auto& newPoint = newPC.points.emplace_back();
                    newPoint.getVector3fMap() = global;
                    newPoint.r = point.r;
                    newPoint.g = point.g;
                    newPoint.b = point.b;
                    newPoint.label = point.label;
                }
            }
        }


        armarx::viz::Layer rgbLayer = arviz.layer("PointCloudRGB");
        armarx::viz::Layer labelLayer = arviz.layer("PointCloudLabel");
        armarx::viz::PointCloud rgb = armarx::viz::PointCloud("RGB");
        armarx::viz::PointCloud label = armarx::viz::PointCloud("Label");
        auto& points = newPC.points;
        int pointsSize = (int)points.size();
        for (int i = 0; i < pointsSize; ++i)
        {
            auto& point = points[i];

            if (!std::isfinite(point.x) ||
                !std::isfinite(point.y) ||
                !std::isfinite(point.z))
            {
                continue;
            }

            armarx::viz::ColoredPoint cp;
            cp.x = point.x;
            cp.y = point.y;
            cp.z = point.z;

            cp.color = armarx::viz::Color(point.r, point.g, point.b);

            rgb.addPoint(cp);

            cp.color = glasbeyLUT(point.label, 255);

            label.addPoint(cp);
        }

        rgbLayer.add(rgb);
        labelLayer.add(label);

        arviz.commit({rgbLayer, labelLayer});
    }

    void DatasetAnnotationWidgetController::onTransCropChange(double)
    {
        onPointCloudChange(widget.pointCloudComboBox->currentIndex());
    }


    void DatasetAnnotationWidgetController::onConnectComponent()
    {
        arviz = armarx::viz::Client::createForGuiPlugin(*this);

        std::string datasetFilepath = "/home/paus/projects/2020/OSD-0.2/pcd";

        dataset = osd::loadDataset(datasetFilepath);

        for (auto& pc : dataset.pointClouds)
        {
            ARMARX_INFO << "prefix: " << (int)pc.prefix
                        << ", number: " << pc.number
                        << ", #points: " << pc.pointCloud.size();
        }

        emit connectGui();
    }
}
