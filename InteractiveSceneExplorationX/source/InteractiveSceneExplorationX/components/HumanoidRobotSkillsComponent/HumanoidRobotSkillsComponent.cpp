/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    InteractiveSceneExplorationX::ArmarXObjects::HumanoidRobotSkillsComponent
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "HumanoidRobotSkillsComponent.h"

#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>


namespace humanoid_robot_skills
{

    using namespace armarx;

    static constexpr int TimingBufferSize = 1000;

    void Armar6Robot_Component::onInitComponent()
    {
        usingProxy(config.PlatformUnit);
        usingProxy(config.KinematicUnit);

        usingTopic(config.PlatformUnitTopic);
        usingTopic(config.KinematicUnitTopic);

        timingJointAngles.resize(TimingBufferSize);
        timingPlatformPose.resize(TimingBufferSize);
    }

    void Armar6Robot_Component::onConnectComponent()
    {

    }

    void Armar6Robot_Component::onDisconnectComponent()
    {

    }

    void Armar6Robot_Component::onExitComponent()
    {

    }

    RobotState Armar6Robot_Component::getState()
    {
        std::lock_guard lock(stateMutex);
        return state;
    }

    Armar6TimingInfo Armar6Robot_Component::getTimingInfo()
    {
        Armar6TimingInfo result;

        std::lock_guard lock(timingMutex);
        result.lastJointAngleInterval = timingJointAngles[timingJointAngleIndex];
        result.lastPlatformPoseInterval = timingPlatformPose[timingPlatformPoseIndex];

        return result;
    }

    void Armar6Robot_Component::reportControlModeChanged(const NameControlModeMap&, Ice::Long, bool, const Ice::Current&)
    {
        // Do nothing
    }

    void Armar6Robot_Component::reportJointAngles(const NameValueMap& jointAngles, Ice::Long timestamp, bool changed, const Ice::Current&)
    {
        {
            std::lock_guard lock(stateMutex);
            state.jointAngles = jointAngles;
        }
        IceUtil::Time now = IceUtil::Time::now();
        {
            std::lock_guard lock(timingMutex);
            IceUtil::Time duration = now - timingLastJointAngleTime;
            timingJointAngleIndex = (timingJointAngleIndex + 1) % TimingBufferSize;
            timingJointAngles[timingJointAngleIndex] = duration;
            timingLastJointAngleTime = now;
        }
    }

    void Armar6Robot_Component::reportJointVelocities(const NameValueMap&, Ice::Long, bool, const Ice::Current&)
    {
        // Do nothing
    }

    void Armar6Robot_Component::reportJointTorques(const NameValueMap&, Ice::Long, bool, const Ice::Current&)
    {
        // Do nothing
    }

    void Armar6Robot_Component::reportJointAccelerations(const NameValueMap&, Ice::Long, bool, const Ice::Current&)
    {
        // Do nothing
    }

    void Armar6Robot_Component::reportJointCurrents(const NameValueMap&, Ice::Long, bool, const Ice::Current&)
    {
        // Do nothing
    }

    void Armar6Robot_Component::reportJointMotorTemperatures(const NameValueMap&, Ice::Long, bool, const Ice::Current&)
    {
        // Do nothing
    }

    void Armar6Robot_Component::reportJointStatuses(const NameStatusMap&, Ice::Long, bool, const Ice::Current&)
    {
        // Do nothing
    }

    void Armar6Robot_Component::reportPlatformPose(const PlatformPose& pose, const Ice::Current&)
    {
        {
            std::lock_guard lock(stateMutex);
            state.platformPose.x = pose.x;
            state.platformPose.y = pose.y;
            state.platformPose.theta = pose.rotationAroundZ;
        }
        IceUtil::Time now = IceUtil::Time::now();
        {
            std::lock_guard lock(timingMutex);
            IceUtil::Time duration = now - timingLastPlatformPoseTime;
            timingPlatformPoseIndex = (timingPlatformPoseIndex + 1) % TimingBufferSize;
            timingPlatformPose[timingPlatformPoseIndex] = duration;
            timingLastPlatformPoseTime = now;
        }
    }

    void Armar6Robot_Component::reportNewTargetPose(Ice::Float, Ice::Float, Ice::Float, const Ice::Current&)
    {
        // Do nothing
    }

    void Armar6Robot_Component::reportPlatformVelocity(Ice::Float, Ice::Float, Ice::Float, const Ice::Current&)
    {
        // Do nothing
    }

    void Armar6Robot_Component::reportPlatformOdometryPose(Ice::Float, Ice::Float, Ice::Float, const Ice::Current&)
    {
        // Do nothing
    }



    RobotState humanoid_robot_skills::Armar6Robot::getState()
    {
        return robotComponent->getState();
    }

    Armar6Platform::Armar6Platform(const Armar6RobotPtr& robot)
        : robot(robot)
    {

    }

    Platform_MoveToLocation_Result Armar6Platform::moveToLocation(PlatformLocation targetLocation, const Platform_MoveToLocation_Params& params)
    {
        Platform_MoveToLocation_Result result;
        return result;
    }

    Platform_LookUpNamedLocation_Result Armar6Platform::lookUpNamedLocation(const NamedPlatformLocation& namedLocation)
    {
        Platform_LookUpNamedLocation_Result result;
        return result;
    }

    PlatformLocation Armar6Platform::getCurrentLocation()
    {
        return robot->getState().platformPose;
    }

}

namespace armarx
{
    ARMARX_DECOUPLED_REGISTER_COMPONENT(HumanoidRobotSkillsComponent);

    HumanoidRobotSkillsComponentPropertyDefinitions::HumanoidRobotSkillsComponentPropertyDefinitions(std::string prefix) :
        armarx::ComponentPropertyDefinitions(prefix)
    {
        defineOptionalProperty<std::string>("ExampleProperty", "Example",
                                            "Example property");
    }


    std::string HumanoidRobotSkillsComponent::getDefaultName() const
    {
        return "HumanoidRobotSkillsComponent";
    }


    void HumanoidRobotSkillsComponent::onInitComponent()
    {
    }


    void HumanoidRobotSkillsComponent::onConnectComponent()
    {
        ARMARX_IMPORTANT << "onConnect()";
        robot = std::make_shared<humanoid_robot_skills::Armar6Robot>(getArmarXManager());
        platform = std::make_shared<humanoid_robot_skills::Armar6Platform>(robot);
        ARMARX_IMPORTANT << "Created Ice objects";

        // TODO: Get all the proxies (or only their names) from a config file
        // Then, instantiate a Platform, Robot and Perception implementation
        // Should the proxies by lazy?
        // I guess, we can expect the core stuff to be available?
        createRemoteGuiTab();
        RemoteGui_startRunningTask();
    }


    void HumanoidRobotSkillsComponent::onDisconnectComponent()
    {

    }


    void HumanoidRobotSkillsComponent::onExitComponent()
    {

    }


    armarx::PropertyDefinitionsPtr HumanoidRobotSkillsComponent::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new HumanoidRobotSkillsComponentPropertyDefinitions(
                getConfigIdentifier()));
    }

    void HumanoidRobotSkillsComponent::createRemoteGuiTab()
    {
        GridLayout grid;
        int row = 0;
        {
            tab.printStuff.setLabel("Print Stuff");
            grid.add(tab.printStuff, Pos{row, 0}, Span{1, 2});
            row += 1;
        }

        RemoteGui_createTab("HumanoidRobotSkillsComponent", grid, &tab);
    }

    void HumanoidRobotSkillsComponent::RemoteGui_update()
    {
        if (tab.printStuff.wasClicked())
        {
            ARMARX_IMPORTANT << "The button was pressed!";
        }

        {
            // Plot some values to see that something happens
            humanoid_robot_skills::RobotState state = robot->getState();
            humanoid_robot_skills::Armar6TimingInfo timing = robot->robotComponent->getTimingInfo();

            humanoid_robot_skills::PlatformLocation loc = state.platformPose;

            StringVariantBaseMap map;
            map["platform.x"] = new Variant(loc.x);
            map["platform.y"] = new Variant(loc.x);
            map["platform.theta"] = new Variant(loc.theta);
            map["robot.TorsoJoint"] = new Variant(state.jointAngles["TorsoJoint"]);
            map["timing.jointAngles"] = new Variant(timing.lastJointAngleInterval.toMilliSecondsDouble());
            map["timing.platformPose"] = new Variant(timing.lastPlatformPoseInterval.toMilliSecondsDouble());
            //            map["platform.speed.y"] = new Variant(speed.y);
            //            map["platform.speed.z"] = new Variant(speed.z);
            //            map["platform.speed.x"] = new Variant(speed.x);
            //            //map["timing.platformMove"] =
            setDebugObserverChannel("HumanoidRobotSkills", map);
        }

    }
}
