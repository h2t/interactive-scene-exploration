/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    InteractiveSceneExplorationX::ArmarXObjects::PickAndPlaceComponent
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PickAndPlaceComponent.h"

#include <Armar6Skills/libraries/Armar6Tools/Armar6HandV2Helper.h>
#include <Armar6Skills/libraries/Armar6Tools/BimanualTransformationHelper.h>

#include <RobotAPI/libraries/RobotStatechartHelpers/ForceTorqueHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/KinematicUnitHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/PositionControllerHelper.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectFinder.h>

#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>
#include <ArmarXCore/core/time/CycleUtil.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>

#include <VirtualRobot/math/Line.h>
#include <VirtualRobot/math/Helpers.h>
#include <VirtualRobot/math/LinearInterpolatedPose.h>

#include <SimoxUtility/json/io.h>
#include <SimoxUtility/json/eigen_conversion.h>
#include <SimoxUtility/math/pose/pose.h>


namespace armarx
{
    ARMARX_DECOUPLED_REGISTER_COMPONENT(PickAndPlaceComponent);

    // JSON conversions
    void from_json(nlohmann::json const& j, GeneralConfig& config)
    {
        j["IsSimulation"].get_to(config.IsSimulation);
        j["MaximalDistanceToStartInMM"].get_to(config.MaximalDistanceToStartInMM);
    }

    void from_json(nlohmann::json const& j, PreposeSingleConfig& config)
    {
        config.Waypoints.clear();
        for (auto& jw : j["Waypoints"])
        {
            float qw = jw["qw"];
            float qx = jw["qx"];
            float qy = jw["qy"];
            float qz = jw["qz"];
            float x = jw["x"];
            float y = jw["y"];
            float z = jw["z"];

            Eigen::Matrix4f pose = simox::math::pose(Eigen::Vector3f(x, y, z),
                                   Eigen::Quaternionf(qw, qx, qy, qz));

            config.Waypoints.push_back(pose);
        }
    }

    void from_json(nlohmann::json const& j, PreposeConfig& config)
    {
        j["Left"].get_to(config.Left);
        j["Right"].get_to(config.Right);
    }

    void from_json(nlohmann::json const& j, PickAndPlaceConfig& config)
    {
        j["General"].get_to(config.General);
        j["Prepose"].get_to(config.Prepose);
        j["Support"].get_to(config.Support);
    }

    void from_json(nlohmann::json const& j, PlanStep& step)
    {
        j["name"].get_to(step.objectName);
        j["placingPosition"].get_to(step.placingPosition);
    }

    void from_json(nlohmann::json const& j, Plan& plan)
    {
        plan.steps.clear();
        j["planningSteps"].get_to(plan.steps);
    }

    const char* toString(Side side)
    {
        const char* names[] =
        {
            "Left",
            "Right",
        };
        return names[side];
    }


    const char* toString(Action action)
    {
        const char* names[] =
        {
            "<None>",
            "Retreat",
            "Prepose",
            "PreposeReverse",
            "Push",
            "Pick",
            "Place",
            "SupportPrepose",
            "SupportPreposeReverse",
            "Support",
            "SupportRetreat",
        };
        return names[action];
    }

    void PickAndPlaceComponent::onInitComponent()
    {
        {
            std::string packageName = "Armar6Skills";
            armarx::CMakePackageFinder finder(packageName);
            std::string dataDir = finder.getDataDir() + "/" + packageName;
            graspTrajectories["RightTopOpen"] = Armar6GraspTrajectory::ReadFromFile(dataDir + "/motions/grasps/RightTopOpen.xml");
            graspTrajectories["RightSideOpen"] = Armar6GraspTrajectory::ReadFromFile(dataDir + "/motions/grasps/RightSideOpen.xml");
        }

        offeringTopicFromProperty("DebugObserverName");
        usingProxyFromProperty("KinematicUnitName");
        usingProxyFromProperty("RobotUnitName");
        usingProxyFromProperty("RobotUnitObserverName");
        usingProxyFromProperty("ForceTorqueObserverName");

        armarx::CMakePackageFinder finder("InteractiveSceneExplorationX");
        if (!finder.packageFound())
        {
            ARMARX_WARNING << "ArmarX Package has not been found!";
        }
        else
        {
            ARMARX_INFO << "Adding to datapaths: " << finder.getDataDir();
            armarx::ArmarXDataPath::addDataPaths(finder.getDataDir());
        }
        std::string relativeConfigPath = getProperty<std::string>("ConfigPath").getValue();
        std::string configPath = ArmarXDataPath::getAbsolutePath(relativeConfigPath);

        ARMARX_INFO << "Loading config from: " << configPath;

        nlohmann::json j = nlohmann::read_json(configPath);
        j.get_to(config);

        ARMARX_INFO << "Prepose waypoints for left: " << config.Prepose.Left.Waypoints.size()
                    << ", for right: " << config.Prepose.Right.Waypoints.size();

        std::string relativePlanPath = getProperty<std::string>("PlanPath").getValue();
        planPath = ArmarXDataPath::getAbsolutePath(relativePlanPath);

        //loadPlan();
    }


    void PickAndPlaceComponent::onConnectComponent()
    {
        getTopicFromProperty(debugObserver, "DebugObserverName");
        getProxyFromProperty(kinematicUnit, "KinematicUnitName");
        getProxyFromProperty(robotUnit, "RobotUnitName");
        getProxyFromProperty(robotUnitObserver, "RobotUnitObserverName");
        getProxyFromProperty(forceTorqueObserver, "ForceTorqueObserverName");

        robot = addRobot("robot", VirtualRobot::RobotIO::eStructure);
        robot->setPropagatingJointValuesEnabled(!config.General.IsSimulation);
        guiRobot = addRobot("gui-robot", VirtualRobot::RobotIO::eStructure);
        guiRobot->setPropagatingJointValuesEnabled(!config.General.IsSimulation);
        robotNameHelper = RobotNameHelper::Create(getRobotStateComponent()->getRobotInfo(), nullptr);

        saveObjectPoses();

        createRemoteGuiTab();
        RemoteGui_startRunningTask();

        controlTask = new RunningTask<PickAndPlaceComponent>(this, &PickAndPlaceComponent::runControlLoop);
        controlTask->start();
    }


    void PickAndPlaceComponent::onDisconnectComponent()
    {
        controlTask->stop();
        controlTask = nullptr;
    }


    void PickAndPlaceComponent::onExitComponent()
    {

    }

    void PickAndPlaceComponent::createRemoteGuiTab()
    {
        GridLayout root;
        int row = 0;
        {
            tab.selectSide.setOptions({"Left", "Right"});
            tab.selectSide.setIndex(guiSide);

            root.add(Label("Side:"), Pos{row, 0});
            root.add(tab.selectSide, Pos{row, 1});
            row += 1;
        }
        {
            std::vector<std::string> options;
            options.reserve(Action_End - Action_Begin);
            for (Action action = Action_Begin; action < Action_End; action = (Action)(action + 1))
            {
                options.push_back(toString(action));
            }
            tab.selectAction.setOptions(options);
            tab.selectAction.setIndex(guiAction);

            root.add(Label("Action:"), Pos{row, 0});
            root.add(tab.selectAction, Pos{row, 1});
            row += 1;
        }
        {
            std::vector<std::string> options;
            options.reserve(objectPosesInfo.size());
            for (auto& objectPose : objectPosesInfo)
            {
                options.push_back(objectPose.name);
            }
            if (options.empty())
            {
                options.push_back("<None>");
            }
            tab.selectObject.setOptions(options);
            if (guiObjectIndex >= 0 && guiObjectIndex < (int)options.size())
            {
                tab.selectObject.setIndex(guiObjectIndex);
            }

            root.add(Label("Object:"), Pos{row, 0});
            root.add(tab.selectObject, Pos{row, 1});
            row += 1;
        }
        {
            std::vector<std::string> options;
            options.reserve(graspCandidates.size());
            int graspIndex = 0;
            for (auto& grasp : graspCandidates)
            {
                options.push_back(std::to_string(graspIndex));
                ++graspIndex;
            }
            if (options.empty())
            {
                options.push_back("<None>");
            }
            tab.selectGrasp.setOptions(options);
            if (guiGraspIndex >= 0 && guiGraspIndex < (int)options.size())
            {
                tab.selectGrasp.setIndex(guiGraspIndex);
            }

            root.add(Label("Grasp:"), Pos{row, 0});
            root.add(tab.selectGrasp, Pos{row, 1});
            row += 1;
        }
        {
            tab.placePosition.setValue(guiPlacePosition);
            tab.placePosition.setDecimals(0);
            tab.placePosition.setRange(0.0f, 5000.0f);
            tab.placePosition.setSteps(500);
            root.add(Label("Place Position:"), Pos{row, 0});
            root.add(tab.placePosition, Pos{row, 1});
            row += 1;
        }
        {
            tab.supportPosition.setValue(guiSupportPosition);
            tab.supportPosition.setDecimals(0);
            tab.supportPosition.setRange(-1000.0f, 1000.0f);
            tab.supportPosition.setSteps(500);
            root.add(Label("Support Position:"), Pos{row, 0});
            root.add(tab.supportPosition, Pos{row, 1});
            row += 1;
        }
        {
            tab.saveObjectPoses.setLabel("Save Object Poses");
            root.add(tab.saveObjectPoses, Pos{row, 0}, Span{1, 2});
            row += 1;
        }
        {
            tab.calculateGraspCandidates.setLabel("Calculate Grasp Canidates");
            root.add(tab.calculateGraspCandidates, Pos{row, 0}, Span{1, 2});
            row += 1;
        }
        {
            tab.autoUpdateGraspCandidates.setValue(guiAutoUpdateGrasps);
            root.add(Label("Auto Update Grasp Candidates:"), Pos{row, 0});
            root.add(tab.autoUpdateGraspCandidates, Pos{row, 1});
            row += 1;
        }
        {
            tab.autoUpdatePlaceCandidate.setValue(guiAutoUpdatePlace);
            root.add(Label("Auto Update Place Candidate:"), Pos{row, 0});
            root.add(tab.autoUpdatePlaceCandidate, Pos{row, 1});
            row += 1;
        }
        {
            tab.executeAction.setLabel("Execute Action");
            root.add(tab.executeAction, Pos{row, 0}, Span{1, 2});
            row += 1;
        }
        {
            tab.loadPlan.setLabel("Load Plan");
            root.add(tab.loadPlan, Pos{row, 0}, Span{1, 2});
            row += 1;
        }
        {
            std::vector<std::string> options;
            options.reserve(plan.steps.size());
            int stepIndex = 0;
            for (auto& step : plan.steps)
            {
                ++stepIndex;
                options.push_back(std::to_string(stepIndex) + "_" + step.objectName);
            }
            if (options.empty())
            {
                options.push_back("<NONE>");
            }
            tab.planSteps.setOptions(options);
            tab.planSteps.setIndex(guiPlanStepIndex);

            root.add(Label("Plan Step:"), Pos{row, 0});
            root.add(tab.planSteps, Pos{row, 1});
            row += 1;
        }
        {
            tab.executePlanStep.setLabel("Execute Next Plan Step");
            root.add(tab.executePlanStep, Pos{row, 0}, Span{1, 2});
            row += 1;
        }
        {
            tab.stop.setLabel("STOP");
            root.add(tab.stop, Pos{row, 0}, Span{2, 2});
            row += 1;
        }

        RemoteGui_createTab(getName(), root, &tab);
    }


    void PickAndPlaceComponent::RemoteGui_update()
    {
        bool recreate = recreateTabNextLoop;

        Side newSide = (Side)tab.selectSide.getIndex();
        if (newSide != guiSide)
        {
            ARMARX_INFO << "Selected new side: " << toString(newSide);
            guiSide = newSide;
        }

        Action newAction = (Action)tab.selectAction.getIndex();
        if (newAction != guiAction)
        {
            ARMARX_INFO << "Selected new action: " << toString(newAction);
            guiAction = newAction;
            // TODO: Make the remote GUI for that specific action
        }

        int newObjectIndex = tab.selectObject.getIndex();
        if (newObjectIndex != guiObjectIndex)
        {
            ARMARX_INFO << "Selected new object index: " << newObjectIndex;
            if (newObjectIndex >= 0 && newObjectIndex < (int)objectPosesInfo.size() && !objectPosesInfo.empty())
            {
                auto& objectPose = objectPosesInfo[newObjectIndex];
                ARMARX_INFO << "Selected new object: " << objectPose.name;
                guiObjectIndex = newObjectIndex;
                Eigen::Vector3f globalPosition = math::Helpers::Position(objectPose.globalPose);
                tab.placePosition.setValue(globalPosition);
                tab.supportPosition.setValue(Eigen::Vector3f::Zero());
            }
        }
        int newGraspIndex = tab.selectGrasp.getIndex();
        if (newGraspIndex != guiGraspIndex)
        {
            if (newGraspIndex >= 0 && newGraspIndex < (int)graspCandidates.size())
            {
                ARMARX_INFO << "Selected new grasp: " << newGraspIndex;
                guiGraspIndex = newGraspIndex;
                visualizeGraspCandidates();
            }
        }

        int newPlanStepIndex = tab.planSteps.getIndex();
        if (newPlanStepIndex != guiPlanStepIndex)
        {
            if (newPlanStepIndex >= 0 && newPlanStepIndex < (int)plan.steps.size())
            {
                ARMARX_INFO << "Selected new plan step";

                auto& step = plan.steps[newPlanStepIndex];
                for (int objectIndex = 0; objectIndex < (int)objectPosesInfo.size(); ++objectIndex)
                {
                    auto& object = objectPosesInfo[objectIndex];
                    if (object.name == step.objectName)
                    {
                        tab.selectObject.setIndex(objectIndex);
                        guiObjectIndex = objectIndex;
                    }
                }

                tab.placePosition.setValue(step.placingPosition);

                guiPlanStepIndex = newPlanStepIndex;
            }
        }

        bool newAutoUpdateGrasps = tab.autoUpdateGraspCandidates.getValue();
        if (newAutoUpdateGrasps != guiAutoUpdateGrasps)
        {
            guiAutoUpdateGrasps = newAutoUpdateGrasps;
        }
        bool newAutoUpdatePlace = tab.autoUpdatePlaceCandidate.getValue();
        if (newAutoUpdatePlace != guiAutoUpdatePlace)
        {
            guiAutoUpdatePlace = newAutoUpdatePlace;
        }

        Eigen::Vector3f newPlacePosition = tab.placePosition.getValue();
        Eigen::Vector3f placeDiff = guiPlacePosition - newPlacePosition;
        if (placeDiff.norm() > 0.1f)
        {
            guiPlacePosition = newPlacePosition;
            visualizePlacePosition();
        }

        Eigen::Vector3f newSupportPosition = tab.supportPosition.getValue();
        Eigen::Vector3f supportDiff = guiSupportPosition - newSupportPosition;
        if (supportDiff.norm() > 0.1f)
        {
            guiSupportPosition = newSupportPosition;
            visualizeSupportPosition();
        }


        if (tab.saveObjectPoses.wasClicked())
        {
            saveObjectPoses();
        }

        if (tab.calculateGraspCandidates.wasClicked())
        {
            std::unique_lock lock(controlMutex);
            controlInfo.action = guiAction;
            controlInfo.side = toString(guiSide);
            if (guiObjectIndex >= 0 && guiObjectIndex < (int)objectPosesInfo.size())
            {
                controlInfo.objectPose = objectPosesInfo[guiObjectIndex];
                calculateGraspCandidates(controlInfo);
            }
            else
            {
                ARMARX_WARNING << "guiObjectIndex = " << guiObjectIndex
                               << ", objectPosesInfo size: " << objectPosesInfo.size();
            }
        }

        if (tab.executeAction.wasClicked())
        {
            std::unique_lock lock(controlMutex);
            controlInfo.action = guiAction;
            controlInfo.side = toString(guiSide);
            if (guiObjectIndex >= 0 && guiObjectIndex < (int)objectPosesInfo.size())
            {
                controlInfo.objectPose = objectPosesInfo[guiObjectIndex];
            }
            if (guiGraspIndex >= 0 && guiGraspIndex < (int)graspCandidates.size())
            {
                controlInfo.grasp = graspCandidates[guiGraspIndex];
            }
            else
            {
                controlInfo.grasp = nullptr;
            }
            controlInfo.placePositionGlobal = guiPlacePosition;
            controlInfo.supportPositionOffset = guiSupportPosition;
            ARMARX_INFO << "Executing action: " << toString(guiAction);
            runExecution = true;
        }

        if (tab.loadPlan.wasClicked())
        {
            loadPlan();
            recreate = true;
        }

        if (tab.stop.wasClicked())
        {
            ARMARX_INFO << "Stopping action";
            stop = true;
        }

        if (guiAutoUpdateGrasps)
        {
            std::unique_lock lock(controlMutex);
            controlInfo.action = guiAction;
            controlInfo.side = toString(guiSide);
            if (guiObjectIndex >= 0 && guiObjectIndex < (int)objectPosesInfo.size())
            {
                controlInfo.objectPose = objectPosesInfo[guiObjectIndex];
                calculateGraspCandidates(controlInfo);
            }
        }
        if (guiAutoUpdatePlace)
        {
            visualizePlacePosition();
        }

        // TODO: Make this optional?
        visualizeObjectPoses();

        if (recreate)
        {
            createRemoteGuiTab();
            recreateTabNextLoop = false;
        }
    }

    void PickAndPlaceComponent::saveObjectPoses()
    {
        objectPoses = objectPoseObserver->getObjectPoses();

        objectPosesInfo.clear();
        for (auto& objPose : objectPoses)
        {

            ObjectFinder finder;
            std::optional<ObjectInfo> foundObject = finder.findObject(objPose.objectID.dataset, objPose.objectID.className);
            if (!foundObject)
            {
                ARMARX_WARNING << "No object information found for object: " << objPose.objectID.className;
                continue;
            }

            ObjectPoseInfo info;
            info.dataset = objPose.objectID.dataset;
            info.name = objPose.objectID.className;
            auto location = foundObject->simoxXML();
            info.package = location.package;
            info.simoxFile = location.relativePath;

            std::optional<simox::OrientedBox<float> > oobb = foundObject->loadOOBB();
            if (oobb)
            {
                info.localCenter = oobb->center();
                info.localExtend1 = oobb->extend(0);
                info.localExtend2 = oobb->extend(1);
                info.localExtend3 = oobb->extend(2);
            }
            else
            {
                ARMARX_WARNING << "No object bounding box for: " << info.name;
                info.localCenter = Eigen::Vector3f::Zero();
                info.localExtend1 = Eigen::Vector3f::UnitX();
                info.localExtend2 = Eigen::Vector3f::UnitY();
                info.localExtend3 = Eigen::Vector3f::UnitZ();
            }

            info.globalPose = PosePtr::dynamicCast(objPose.objectPoseGlobal)->toEigen();

            objectPosesInfo.push_back(info);
        }
        recreateTabNextLoop = true;
    }

    void PickAndPlaceComponent::calculateGraspCandidates(const ControlInfo& control)
    {
        ARMARX_VERBOSE << "Calculating grasp candidates";
        grasping::GraspCandidateSeq candidates;

        synchronizeLocalClone(guiRobot);
        Eigen::Matrix4f robotPoseGlobal = guiRobot->getGlobalPose();
        Eigen::Matrix3f robotRotGlobal = robotPoseGlobal.block<3, 3>(0, 0);

        RobotNameHelper::RobotArm arm = robotNameHelper->getRobotArm(control.side, guiRobot);
        VirtualRobot::RobotNodeSetPtr rns = arm.getKinematicChain();
        VirtualRobot::RobotNodePtr tcp = arm.getTCP();

        Eigen::Vector3f localCenter = control.objectPose.localCenter;
        Eigen::Vector3f localExtend1 = control.objectPose.localExtend1;
        Eigen::Vector3f localExtend2 = control.objectPose.localExtend2;
        Eigen::Vector3f localExtend3 = control.objectPose.localExtend3;

        ARMARX_VERBOSE << "Got local OOBB, center: " << localCenter.transpose()
                       << ", extend1: " << localExtend1.transpose()
                       << ", extend2: " << localExtend2.transpose()
                       << ", extend3: " << localExtend3.transpose();

        Eigen::Matrix4f objectPoseGlobal = control.objectPose.globalPose;

        Eigen::Matrix4f objectPoseRobot = robotPoseGlobal.inverse() * objectPoseGlobal;
        Eigen::Matrix3f objectRotRobot = objectPoseRobot.block<3, 3>(0, 0);

        Eigen::Vector3f robotCenter = (objectPoseRobot * localCenter.homogeneous()).head<3>();
        // TODO: These are actually half extends
        Eigen::Vector3f robotExtend1 = 0.5f * (objectRotRobot * localExtend1);
        Eigen::Vector3f robotExtend2 = 0.5f * (objectRotRobot * localExtend2);
        Eigen::Vector3f robotExtend3 = 0.5f * (objectRotRobot * localExtend3);


        Eigen::Vector3f center = robotCenter;

        Eigen::Vector3f v1 = robotExtend1.normalized();
        Eigen::Vector3f v2 = robotExtend2.normalized();
        Eigen::Vector3f v3 = robotExtend3.normalized();
        // These have to be sorted in a specific way
        {
            // v3 should point in the direction of Z
            Eigen::Vector3f z = Eigen::Vector3f::UnitZ();
            if (std::abs(v1.dot(z)) > 0.8f)
            {
                std::swap(v1, v3);
                std::swap(robotExtend1, robotExtend3);
            }
            if (std::abs(v2.dot(z)) > 0.8f)
            {
                std::swap(v2, v3);
                std::swap(robotExtend2, robotExtend3);
            }
            if (v3.z() < 0.0f)
            {
                v3 = -v3;
                robotExtend3 = -robotExtend3;
            }
            if (robotExtend1.norm() < robotExtend2.norm())
            {
                Eigen::Vector3f temp = v1;
                v1 = v2;
                v2 = -temp;

                temp = robotExtend1;
                robotExtend1 = robotExtend2;
                robotExtend2 = -temp;
            }
        }
        using namespace math;

        struct GraspDescription
        {
            Eigen::Matrix4f pose;
            Eigen::Vector3f approach;
            grasping::ApertureType preshape;
        };

        Eigen::Matrix3f referenceOriRightTopOpen = Eigen::Quaternionf(
                    -0.3687919974327087,
                    0.6144002676010132,
                    0.6136394143104553,
                    0.331589400768280)
                .toRotationMatrix();

        Eigen::Matrix3f referenceOriRightSide = Eigen::AngleAxisf(M_PI / 2, Eigen::Vector3f::UnitY()) * referenceOriRightTopOpen;

        // TODO: Check whether these make sense!
        Eigen::Matrix3f referenceOriLeftTopOpen =
            BimanualTransformationHelper::transformOrientationToOtherHand(referenceOriRightTopOpen, "Armar6").toRootEigen(robot);
        //Eigen::AngleAxisf(-M_PI / 3, Eigen::Vector3f::UnitX()) * Eigen::AngleAxisf(M_PI, Eigen::Vector3f::UnitY()) * referenceOriRightTopOpen;
        Eigen::Matrix3f referenceOriLeftSide =
            //Eigen::AngleAxisf(M_PI, Eigen::Vector3f::UnitY()) *
            BimanualTransformationHelper::transformOrientationToOtherHand(referenceOriRightSide, "Armar6").toRootEigen(robot);
        //Eigen::AngleAxisf(M_PI, Eigen::Vector3f::UnitX()) *
        //Eigen::AngleAxisf(M_PI / 2, Eigen::Vector3f::UnitY()) *
        //referenceOriLeftTopOpen;

        // Left hand seems to need a bigger offset, otherwise the thumb misses the object
        bool isRightSide = (control.side == "Right");
        Eigen::Vector3f graspOffsetTCP = isRightSide ? Eigen::Vector3f(0.0f, 30.0f, 0.0f) : Eigen::Vector3f(0.0f, 50.0f, 0.0f);

        Eigen::Matrix3f referenceOriTop = isRightSide ? referenceOriRightTopOpen : referenceOriLeftTopOpen;
        Eigen::Matrix3f referenceOriSide = isRightSide ? referenceOriRightSide : referenceOriLeftSide;

        auto calculateGraspPoseTop = [&](const Eigen::Vector3f & preferredForward, grasping::ApproachType graspType)
        {
            Eigen::Vector3f graspForwardVector = v2;

            grasping::ApertureType preshape = grasping::OpenAperture;

            if (graspForwardVector.dot(preferredForward) < 0)
            {
                graspForwardVector = -graspForwardVector;
            }
            float angleV2 = Helpers::Angle(Eigen::Vector2f(graspForwardVector(0), graspForwardVector(1)));
            float angleGraspOri = Helpers::Angle(Eigen::Vector2f(0, 1));
            float rotationAngle = Helpers::AngleModPI(angleV2 - angleGraspOri);
            // TODO: Handle left as well
            Eigen::Matrix3f referenceOrientation = referenceOriTop;
            Eigen::AngleAxisf aa = Eigen::AngleAxisf(rotationAngle, Eigen::Vector3f::UnitZ());
            Eigen::Matrix3f graspOri = aa.toRotationMatrix() * referenceOrientation;

            Eigen::Vector3f graspPos = center + robotExtend3 + graspOri * graspOffsetTCP;



            GraspDescription grasp;
            grasp.pose = Helpers::CreatePose(graspPos, graspOri);
            grasp.approach = Eigen::Vector3f::UnitZ();
            grasp.preshape = preshape;

            return grasp;
        };

        auto calculateGraspPoseSide = [&](const Eigen::Vector3f & preferredForward)
        {
            Eigen::Vector3f graspForwardVector = v1;
            Eigen::Vector3f offset_v1 = 0.95f * robotExtend1; // Was 95 percentile

            if (graspForwardVector.dot(preferredForward) < 0)
            {
                graspForwardVector = -graspForwardVector;
                offset_v1 = -0.95f * robotExtend1;
            }
            if (!isRightSide)
            {
                //graspForwardVector *= -1.0f;
                offset_v1 *= -1.0f;
            }

            float angleV1 = Helpers::Angle(Eigen::Vector2f(graspForwardVector(0), graspForwardVector(1)));
            float angleGraspOri = Helpers::Angle(Eigen::Vector2f(1, 0));
            float rotationAngle = Helpers::AngleModPI(angleV1 - angleGraspOri);
            Eigen::Matrix3f referenceOrientation = referenceOriSide;
            Eigen::AngleAxisf aa = Eigen::AngleAxisf(rotationAngle, Eigen::Vector3f::UnitZ());
            Eigen::Matrix3f graspOri = aa.toRotationMatrix() * referenceOrientation;

            // Move the grasp a bit down, so that the thumb can actually hit the object
            Eigen::Vector3f offsetDown = 5.0f * v3;
            Eigen::Vector3f graspPos = center + offset_v1 + offsetDown;

            GraspDescription grasp;
            grasp.pose = Helpers::CreatePose(graspPos, graspOri);
            grasp.approach = graspForwardVector.normalized();
            if (!isRightSide)
            {
                grasp.approach *= -1.0f;
            }
            grasp.preshape = grasping::OpenAperture;

            return grasp;
        };

        {
            Eigen::Vector3f globalCenter = (robotPoseGlobal * robotCenter.homogeneous()).head<3>();
            Eigen::Vector3f globalExtend1 = (robotRotGlobal * robotExtend1);
            Eigen::Vector3f globalExtend2 = (robotRotGlobal * robotExtend2);
            Eigen::Vector3f globalExtend3 = (robotRotGlobal * robotExtend3);
            viz::Layer bbLayer = arviz.layer("BoundingBox");
            viz::Arrow ex1 = viz::Arrow("ex1")
                             .position(globalCenter)
                             .direction(globalExtend1)
                             .length(globalExtend1.norm())
                             .width(2.0f)
                             .color(viz::Color::red());
            viz::Arrow ex2 = viz::Arrow("ex2")
                             .position(globalCenter)
                             .direction(globalExtend2)
                             .length(globalExtend2.norm())
                             .width(2.0f)
                             .color(viz::Color::green());
            viz::Arrow ex3 = viz::Arrow("ex3")
                             .position(globalCenter)
                             .direction(globalExtend3)
                             .length(globalExtend3.norm())
                             .width(2.0f)
                             .color(viz::Color::blue());

            bbLayer.add(ex1, ex2, ex3);
            arviz.commit(bbLayer);
        }

        Eigen::Vector3f preferredForwardTop(-1, 1, 0);
        Eigen::Vector3f preferredForwardSide(1, 0, 0);

        std::vector<float> signs = {1, -1};

        // TODO: Make this selectable
        grasping::ApproachType graspType = grasping::TopApproach;
        for (float fwdSign : signs)
        {
            if (control.side == "Left")
            {
                // FIME: Top grasps for the left hand are disabled, because we do not have a trajectory for them
                // break;
            }
            std::string suffix = (fwdSign > 0 ? "_fwd" : "_rev");

            GraspDescription grasp;
            grasping::GraspCandidatePtr candidate = new grasping::GraspCandidate();
            candidate->executionHints = new grasping::GraspCandidateExecutionHints();
            candidate->sourceInfo = new grasping::GraspCandidateSourceInfo();
            candidate->reachabilityInfo = new grasping::GraspCandidateReachabilityInfo();

            Armar6GraspTrajectoryPtr graspTrajectory;
            grasp.pose = Helpers::CreatePose(center, Eigen::Matrix3f::Identity());
            if (graspType == grasping::TopApproach)
            {
                grasp = calculateGraspPoseTop(fwdSign * preferredForwardTop, graspType);
                candidate->executionHints->approach = grasping::TopApproach;
                candidate->executionHints->graspTrajectoryName = "RightTopOpen";
                graspTrajectory = graspTrajectories.at("RightTopOpen");
                if (control.side == "Left")
                {
                    graspTrajectories["LeftTopOpen"] = BimanualTransformationHelper::transformGraspTrajectoryToOtherHand(graspTrajectory);
                    candidate->executionHints->graspTrajectoryName = "LeftTopOpen";
                }
            }
            else if (graspType == grasping::SideApproach)
            {
                grasp = calculateGraspPoseSide(fwdSign * preferredForwardSide);
                candidate->executionHints->approach = grasping::SideApproach;
                candidate->executionHints->graspTrajectoryName = "RightSideOpen";
                graspTrajectory = graspTrajectories.at("RightSideOpen");
            }
            else
            {
                ARMARX_ERROR << "Unsupported grasp type";
            }


            graspTrajectory = graspTrajectory->getTransformedToGraspPose(grasp.pose);


            std::map<std::string, float> fingerJointsVisu;
            std::map<std::string, float> openVisu =
            {
                {"Pinky R 1 Joint", 0},
                {"Pinky R 2 Joint", 0},
                {"Pinky R 3 Joint", 0},
                {"Ring R 1 Joint", 0},
                {"Ring R 2 Joint", 0},
                {"Ring R 3 Joint", 0},
                {"Middle R 1 Joint", 0},
                {"Middle R 2 Joint", 0},
                {"Middle R 3 Joint", 0},
                {"Index R 1 Joint", 0},
                {"Index R 2 Joint", 0},
                {"Index R 3 Joint", 0},
                {"Thumb R 1 Joint", 0},
                {"Thumb R 2 Joint", 0}
            };

            if (grasp.preshape == grasping::OpenAperture)
            {
                fingerJointsVisu = openVisu;
                candidate->executionHints->preshape = grasping::OpenAperture;
            }
            else if (grasp.preshape == grasping::PreshapedAperture)
            {
                // TODO: Add preshape visu
                fingerJointsVisu = openVisu;
                candidate->executionHints->preshape = grasping::PreshapedAperture;
            }
            else
            {
                ARMARX_ERROR << "Unsupported preshape type";
            }

            candidate->robotPose = new Pose(guiRobot->getGlobalPose());
            candidate->graspPose = new Pose(grasp.pose);
            candidate->groupNr = 1;
            candidate->providerName = getName();
            candidate->side = control.side;
            candidate->approachVector = new Vector3(grasp.approach);
            candidate->graspSuccessProbability = 0.5f;
            candidate->objectType = grasping::KnownObject;
            candidate->sourceFrame = "root";
            candidate->targetFrame = "root";
            candidate->sourceInfo->referenceObjectName = control.objectPose.name;
            candidate->sourceInfo->segmentationLabelID = 1;
            candidate->sourceInfo->bbox = new grasping::BoundingBox();
            candidate->sourceInfo->bbox->center = new Vector3(robotCenter);
            candidate->sourceInfo->bbox->ha1 = new Vector3(robotExtend1);
            candidate->sourceInfo->bbox->ha2 = new Vector3(robotExtend2);
            candidate->sourceInfo->bbox->ha3 = new Vector3(robotExtend3);

            SimpleDiffIK::Reachability reachability = graspTrajectory->calculateReachability(rns, tcp);

            //ARMARX_IMPORTANT << labelstr << ": " << reachability.ikResults.at(0).posDiff.transpose();

            candidate->reachabilityInfo->reachable = reachability.reachable;
            candidate->reachabilityInfo->minimumJointLimitMargin = reachability.minimumJointLimitMargin;
            candidate->reachabilityInfo->jointLimitMargins = ::math::Helpers::VectorToStd(reachability.jointLimitMargins);
            candidate->reachabilityInfo->maxPosError = reachability.maxPosError;
            candidate->reachabilityInfo->maxOriError = reachability.maxOriError;
            candidates.push_back(candidate);
        }

        graspType = grasping::SideApproach;
        for (float fwdSign : signs)
        {
            std::string suffix = (fwdSign > 0 ? "_fwd" : "_rev");

            GraspDescription grasp;
            grasping::GraspCandidatePtr candidate = new grasping::GraspCandidate();
            candidate->executionHints = new grasping::GraspCandidateExecutionHints();
            candidate->sourceInfo = new grasping::GraspCandidateSourceInfo();
            candidate->reachabilityInfo = new grasping::GraspCandidateReachabilityInfo();

            Armar6GraspTrajectoryPtr graspTrajectory;
            grasp.pose = Helpers::CreatePose(center, Eigen::Matrix3f::Identity());
            if (graspType == grasping::TopApproach)
            {
                grasp = calculateGraspPoseTop(fwdSign * preferredForwardTop, graspType);
                candidate->executionHints->approach = grasping::TopApproach;
                candidate->executionHints->graspTrajectoryName = "RightTopOpen";
                graspTrajectory = graspTrajectories.at("RightTopOpen");
            }
            else if (graspType == grasping::SideApproach)
            {
                grasp = calculateGraspPoseSide(fwdSign * preferredForwardSide);
                candidate->executionHints->approach = grasping::SideApproach;
                candidate->executionHints->graspTrajectoryName = "RightSideOpen";
                graspTrajectory = graspTrajectories.at("RightSideOpen");
            }
            else
            {
                ARMARX_ERROR << "Unsupported grasp type";
            }


            graspTrajectory = graspTrajectory->getTransformedToGraspPose(grasp.pose);


            std::map<std::string, float> fingerJointsVisu;
            std::map<std::string, float> openVisu =
            {
                {"Pinky R 1 Joint", 0},
                {"Pinky R 2 Joint", 0},
                {"Pinky R 3 Joint", 0},
                {"Ring R 1 Joint", 0},
                {"Ring R 2 Joint", 0},
                {"Ring R 3 Joint", 0},
                {"Middle R 1 Joint", 0},
                {"Middle R 2 Joint", 0},
                {"Middle R 3 Joint", 0},
                {"Index R 1 Joint", 0},
                {"Index R 2 Joint", 0},
                {"Index R 3 Joint", 0},
                {"Thumb R 1 Joint", 0},
                {"Thumb R 2 Joint", 0}
            };

            if (grasp.preshape == grasping::OpenAperture)
            {
                fingerJointsVisu = openVisu;
                candidate->executionHints->preshape = grasping::OpenAperture;
            }
            else if (grasp.preshape == grasping::PreshapedAperture)
            {
                // TODO: Add preshape visu
                fingerJointsVisu = openVisu;
                candidate->executionHints->preshape = grasping::PreshapedAperture;
            }
            else
            {
                ARMARX_ERROR << "Unsupported preshape type";
            }

            candidate->robotPose = new Pose(guiRobot->getGlobalPose());
            candidate->graspPose = new Pose(grasp.pose);
            candidate->groupNr = 1;
            candidate->providerName = getName();
            candidate->side = control.side;
            candidate->approachVector = new Vector3(grasp.approach);
            candidate->graspSuccessProbability = 0.5f;
            candidate->objectType = grasping::KnownObject;
            candidate->sourceFrame = "root";
            candidate->targetFrame = "root";
            candidate->sourceInfo->referenceObjectName = control.objectPose.name;
            candidate->sourceInfo->segmentationLabelID = 1;
            candidate->sourceInfo->bbox = new grasping::BoundingBox();
            candidate->sourceInfo->bbox->center = new Vector3(robotCenter);
            candidate->sourceInfo->bbox->ha1 = new Vector3(robotExtend1);
            candidate->sourceInfo->bbox->ha2 = new Vector3(robotExtend2);
            candidate->sourceInfo->bbox->ha3 = new Vector3(robotExtend3);

            SimpleDiffIK::Reachability reachability = graspTrajectory->calculateReachability(rns, tcp);

            //ARMARX_IMPORTANT << labelstr << ": " << reachability.ikResults.at(0).posDiff.transpose();

            candidate->reachabilityInfo->reachable = reachability.reachable;
            candidate->reachabilityInfo->minimumJointLimitMargin = reachability.minimumJointLimitMargin;
            candidate->reachabilityInfo->jointLimitMargins = ::math::Helpers::VectorToStd(reachability.jointLimitMargins);
            candidate->reachabilityInfo->maxPosError = reachability.maxPosError;
            candidate->reachabilityInfo->maxOriError = reachability.maxOriError;
            candidates.push_back(candidate);
        }

        std::string candidateReachability;
        for (auto& candidate : candidates)
        {
            candidateReachability += candidate->reachabilityInfo->reachable ? "1, " : "0, ";
        }
        ARMARX_VERBOSE << "Found " << candidates.size() << " grasp candidates: " << candidateReachability;

        std::string handRootNodeName = (control.side == "Right") ? "Hand R Root" : "Hand L Root";
        graspTcpToHandRoot = tcp->getPoseInRootFrame().inverse() * guiRobot->getRobotNode(handRootNodeName)->getPoseInRootFrame();
        graspRobotPoseGlobal = robotPoseGlobal;
        graspSide = control.side;
        recreateTabNextLoop = candidates.size() != graspCandidates.size();
        graspCandidates = candidates;
        visualizeGraspCandidates();
    }

    void PickAndPlaceComponent::visualizeGraspCandidates()
    {
        viz::Layer graspsLayer = arviz.layer("Grasps");
        int candidateIndex = 0;
        for (auto& candidate : graspCandidates)
        {
            bool isReachable = candidate->reachabilityInfo->reachable;
            viz::Color color = isReachable ? viz::Color::green() : viz::Color::red();
            if (candidateIndex == guiGraspIndex)
            {
                color.a = 255;
            }
            else
            {
                color.a = 80;
            }


            Eigen::Matrix4f tcp2handRoot = graspTcpToHandRoot;

            Eigen::Matrix4f graspPose = PosePtr::dynamicCast(candidate->graspPose)->toEigen();
            std::string modelFile = "Armar6RT/robotmodel/Armar6-SH/Armar6-" + graspSide + "Hand-v3.xml";
            viz::Robot hand = viz::Robot("Grasp_" + std::to_string(candidateIndex))
                              .file("Armar6RT", modelFile)
                              .pose(graspRobotPoseGlobal * graspPose * tcp2handRoot)
                              .overrideColor(color);

            graspsLayer.add(hand);

            candidateIndex += 1;
        }
        arviz.commit(graspsLayer);
    }

    void PickAndPlaceComponent::visualizePlacePosition()
    {
        Eigen::Vector3f placePosition = guiPlacePosition;

        synchronizeLocalClone(guiRobot);
        Eigen::Matrix4f robotPoseGlobal = guiRobot->getGlobalPose();

        RobotNameHelper::RobotArm arm = robotNameHelper->getRobotArm(toString(guiSide), guiRobot);
        VirtualRobot::RobotNodeSetPtr rns = arm.getKinematicChain();
        VirtualRobot::RobotNodePtr tcp = arm.getTCP();

        Eigen::Matrix4f startPose = tcp->getPoseInRootFrame();
        // Make an intermediate pose which only moves in x,y not z
        Eigen::Vector3f placePositionRobot = (robotPoseGlobal.inverse() * placePosition.homogeneous()).head<3>();
        Eigen::Matrix4f intermediatePose = startPose;
        intermediatePose(0, 3) = placePositionRobot(0);
        intermediatePose(1, 3) = placePositionRobot(1);
        Eigen::Matrix4f placePose = intermediatePose;
        placePose(2, 3) = placePositionRobot(2);
        math::Helpers::Position(placePose) = placePositionRobot;

        std::vector<Eigen::Matrix4f> placeWaypoints;
        placeWaypoints.push_back(startPose);
        placeWaypoints.push_back(intermediatePose);
        placeWaypoints.push_back(placePose);

        SimpleDiffIK::Reachability reachability = SimpleDiffIK::CalculateReachability(placeWaypoints,
                Eigen::VectorXf::Zero(rns->getSize()),
                rns, tcp);

        // TODO: Make color red if not reachable!
        viz::Color color = reachability.reachable ? viz::Color::green() : viz::Color::red();

        viz::Layer placeLayer = arviz.layer("Place");
        viz::Sphere vizPlace = viz::Sphere("PlacePosition")
                               .position(placePosition)
                               .color(color)
                               .radius(20.0f);
        placeLayer.add(vizPlace);

        arviz.commit(placeLayer);
    }

    void PickAndPlaceComponent::visualizeSupportPosition()
    {
        Eigen::Vector3f supportPosition = guiSupportPosition;

        synchronizeLocalClone(guiRobot);

        Eigen::Affine3f globalPose(guiRobot->getGlobalPose());

        RobotNameHelper::RobotArm arm = robotNameHelper->getRobotArm("Left", guiRobot);
        //RobotNameHelper::RobotArm arm = robotNameHelper->getRobotArm(toString(guiSide), guiRobot);
        VirtualRobot::RobotNodeSetPtr rns = arm.getKinematicChain();
        VirtualRobot::RobotNodePtr tcp = arm.getTCP();

        auto waypointsPrepose = guiSide == Side_Left ? config.Support.Left.Waypoints : config.Support.Right.Waypoints;
        Eigen::Matrix4f referencePose = waypointsPrepose.back();
        Eigen::Vector3f supportPositionRobot = math::Helpers::Position(referencePose) + supportPosition;
        Eigen::Matrix4f supportPose = referencePose;
        math::Helpers::Position(supportPose) = supportPositionRobot;

        Eigen::Matrix4f startPose = tcp->getPoseInRootFrame();
        std::vector<Eigen::Matrix4f> placeWaypoints;
        placeWaypoints.push_back(startPose);
        placeWaypoints.push_back(supportPose);

        SimpleDiffIK::Reachability reachability = SimpleDiffIK::CalculateReachability(placeWaypoints,
                Eigen::VectorXf::Zero(rns->getSize()),
                rns, tcp);

        viz::Color color = reachability.reachable ? viz::Color::green() : viz::Color::red();

        viz::Layer layer = arviz.layer("Support");
        viz::Box vizSupport = viz::Box("SupportPosition")
                              .position(globalPose * supportPositionRobot)
                              .color(color)
                              .size(30.0f);
        layer.add(vizSupport);

        arviz.commit(layer);
    }

    void PickAndPlaceComponent::visualizeObjectPoses()
    {
        synchronizeLocalClone(guiRobot);

        viz::Layer objectLayer = arviz.layer("Objects");
        viz::Layer robotLayer = arviz.layer("Robot");
        {
            std::unique_lock lock(controlMutex);
            for (auto& object : objectPosesInfo)
            {
                Eigen::Matrix4f pose = object.globalPose;
                if (!object.attachedTo.empty())
                {
                    auto attachedToNode = guiRobot->getRobotNode(object.attachedTo);
                    pose = attachedToNode->getGlobalPose() * object.attachedToTransformation;
                }
                viz::Object viz = viz::Object(object.name)
                                  .file(object.package, object.simoxFile)
                                  .pose(pose);
                objectLayer.add(viz);
            }

            std::map<std::string, float> jointValues = guiRobot->getConfig()->getRobotNodeJointValueMap();
            viz::Robot robot = viz::Robot("Armar6")
                               .file("Armar6RT", "Armar6RT/robotmodel/Armar6-SH/Armar6-SH.xml")
                               .pose(guiRobot->getGlobalPose())
                               .joints(jointValues);

            robotLayer.add(robot);
        }

        arviz.commit({objectLayer, robotLayer});
    }

    void PickAndPlaceComponent::attachObjectToRobotPose(const ObjectPoseInfo& object, VirtualRobot::RobotNodePtr node)
    {
        std::unique_lock lock(controlMutex);

        ObjectPoseInfo* info = nullptr;
        for (auto& objectInfo : objectPosesInfo)
        {
            if (objectInfo.name == object.name)
            {
                info = &objectInfo;
            }
        }
        if (info == nullptr)
        {
            ARMARX_WARNING << "Object info is unknwon";
            return;
        }

        info->attachedTo = node->getName();
        info->attachedToTransformation = node->getGlobalPose().inverse() * info->globalPose;
    }

    void PickAndPlaceComponent::detachAllObjects()
    {
        std::unique_lock lock(controlMutex);
        for (auto& objectInfo : objectPosesInfo)
        {
            if (!objectInfo.attachedTo.empty())
            {
                auto attachedToNode = robot->getRobotNode(objectInfo.attachedTo);
                objectInfo.globalPose = attachedToNode->getGlobalPose() * objectInfo.attachedToTransformation;
                objectInfo.attachedTo = "";
            }
        }
    }

    void PickAndPlaceComponent::loadPlan()
    {
        ARMARX_INFO << "Loading plan from: " << planPath;

        nlohmann::json j = nlohmann::read_json(planPath);
        j.get_to(plan);
    }

    void PickAndPlaceComponent::executeNextPlanStep()
    {

    }

    void PickAndPlaceComponent::runControlLoop()
    {
        CycleUtil c(IceUtil::Time::milliSeconds(20));
        while (!controlTask->isStopped())
        {
            if (runExecution)
            {
                ControlInfo control;
                {
                    std::unique_lock lock(controlMutex);
                    control = controlInfo;
                }

                switch (control.action)
                {
                    case Action_Retreat:
                        executeRetreat(control);
                        break;
                    case Action_Prepose:
                        executePrepose(control);
                        break;
                    case Action_PreposeReverse:
                        executePreposeReverse(control);
                        break;
                    case Action_Push:
                        executePush();
                        break;
                    case Action_Pick:
                        executePick(control);
                        break;
                    case Action_Place:
                        executePlace(control);
                        break;
                    case Action_SupportPrepose:
                        executeSupportPrepose(control);
                        break;
                    case Action_SupportPreposeReverse:
                        executeSupportPreposeReverse(control);
                        break;
                    case Action_Support:
                        executeSupport(control);
                        break;
                    case Action_SupportRetreat:
                        executeSupportRetreat(control);
                        break;
                    default:
                        ARMARX_WARNING << "Unknown action value: " << (int)guiAction;
                        break;
                }

                runExecution = false;
                stop = false;
            }
            else if (runSimulation)
            {
                // TODO

                runSimulation = false;
            }
            else
            {
                c.waitForCycleDuration();
            }
        }
    }

    void PickAndPlaceComponent::executeRetreat(const ControlInfo& control)
    {
        std::string side = control.side;
        RobotNameHelper::Arm arm = robotNameHelper->getArm(side);
        RobotNameHelper::RobotArm robotArm = arm.addRobot(robot);
        VirtualRobot::RobotNodeSetPtr rns = robotArm.getKinematicChain();
        VirtualRobot::RobotNodePtr tcp = robotArm.getTCP();

        Armar6HandV2Helper handHelper(kinematicUnit, robotUnitObserver, side, config.General.IsSimulation);
        std::string controllerName = getName() + "_" + side;
        VelocityControllerHelperPtr velHelper(new VelocityControllerHelper(robotUnit, arm.getKinematicChain(), controllerName));
        //ForceTorqueHelper ftHelper(getForceTorqueObserver(), arm.getForceTorqueSensor());

        // Initial sync to get the right values
        synchronizeLocalClone(robot);

        Eigen::Matrix4f initial_tcp_pose = tcp->getPoseInRootFrame();

        PositionControllerHelper posHelper(tcp, velHelper, initial_tcp_pose);
        {
            CartesianPositionControllerConfigPtr controllerConfig = new CartesianPositionControllerConfig();
            // TODO: Move into
            controllerConfig->KpOri = 1.0f;
            controllerConfig->KpPos = 1.0f;
            controllerConfig->maxOriVel = 1.0f;
            controllerConfig->maxPosVel = 100.0f;
            controllerConfig->thresholdOrientationNear = 0.1f;
            controllerConfig->thresholdOrientationReached = 0.05f;
            controllerConfig->thresholdPositionNear = 40.0f;
            controllerConfig->thresholdPositionReached = 5.0f;
            posHelper.readConfig(controllerConfig);
        }

        auto waypoints = control.side == "Left" ? config.Prepose.Left.Waypoints : config.Prepose.Right.Waypoints;
        // For retreat motion only move TCP to the final prepose
        // This is used if we have executed an action and want to retreat back to the prepose
        posHelper.addWaypoint(waypoints.back());

        Eigen::Vector3f initialTcpPosition = simox::math::position(initial_tcp_pose);
        if (posHelper.waypoints.size() > 1)
        {
            Eigen::Vector3f start_position = simox::math::position(posHelper.waypoints[1]);
            Eigen::Vector3f tcp_to_start = start_position - initialTcpPosition;
            float distance_tcp_to_start_in_mm = tcp_to_start.norm();
            ARMARX_INFO << "Distance TCP <-> start: " << distance_tcp_to_start_in_mm << " mm";
            float maximalDistanceToRetreatInMM = 1400.0f;
            if (distance_tcp_to_start_in_mm > maximalDistanceToRetreatInMM)
            {
                ARMARX_WARNING << "Maximal distance between start and TCP exceeded: " << distance_tcp_to_start_in_mm
                               << " mm > " << maximalDistanceToRetreatInMM << " mm";
                ARMARX_WARNING << "Motion is not started. Are you in the right configuration to start this motion?";
                return;
            }
        }


        CycleUtil c(IceUtil::Time::milliSeconds(10));
        while (!stop && !controlTask->isStopped())
        {
            synchronizeLocalClone(robot);

            posHelper.updateRead();

            bool targetReached = posHelper.isFinalTargetReached();

            if (targetReached)
            {
                posHelper.immediateHardStop();
                // TODO: Indicate success
                ARMARX_IMPORTANT << "Goal reached!";
                break;
            }

            posHelper.updateWrite();

            c.waitForCycleDuration();
        }

        handHelper.shapeHand(0.0f, 0.0f);

        posHelper.immediateHardStop();
        posHelper.updateWrite();
    }

    void PickAndPlaceComponent::executePrepose(ControlInfo const& control)
    {
        std::string side = control.side;
        RobotNameHelper::Arm arm = robotNameHelper->getArm(side);
        RobotNameHelper::RobotArm robotArm = arm.addRobot(robot);
        VirtualRobot::RobotNodeSetPtr rns = robotArm.getKinematicChain();
        VirtualRobot::RobotNodePtr tcp = robotArm.getTCP();

        Armar6HandV2Helper handHelper(kinematicUnit, robotUnitObserver, side, config.General.IsSimulation);
        std::string controllerName = getName() + "_" + side;
        VelocityControllerHelperPtr velHelper(new VelocityControllerHelper(robotUnit, arm.getKinematicChain(), controllerName));
        //ForceTorqueHelper ftHelper(getForceTorqueObserver(), arm.getForceTorqueSensor());

        // Initial sync to get the right values
        synchronizeLocalClone(robot);

        Eigen::Matrix4f initial_tcp_pose = tcp->getPoseInRootFrame();

        PositionControllerHelper posHelper(tcp, velHelper, initial_tcp_pose);
        {
            CartesianPositionControllerConfigPtr controllerConfig = new CartesianPositionControllerConfig();
            // TODO: Move into
            controllerConfig->KpOri = 1.0f;
            controllerConfig->KpPos = 1.0f;
            controllerConfig->maxOriVel = 1.0f;
            controllerConfig->maxPosVel = 160.0f;
            controllerConfig->thresholdOrientationNear = 0.1f;
            controllerConfig->thresholdOrientationReached = 0.05f;
            controllerConfig->thresholdPositionNear = 40.0f;
            controllerConfig->thresholdPositionReached = 5.0f;
            posHelper.readConfig(controllerConfig);
        }

        auto waypoints = control.side == "Left" ? config.Prepose.Left.Waypoints : config.Prepose.Right.Waypoints;
        for (auto& waypoint : waypoints)
        {
            posHelper.addWaypoint(waypoint);
        }

        Eigen::Vector3f initialTcpPosition = simox::math::position(initial_tcp_pose);
        if (posHelper.waypoints.size() > 1)
        {
            Eigen::Vector3f start_position = simox::math::position(posHelper.waypoints[1]);
            Eigen::Vector3f tcp_to_start = start_position - initialTcpPosition;
            float distance_tcp_to_start_in_mm = tcp_to_start.norm();
            ARMARX_INFO << "Distance TCP <-> start: " << distance_tcp_to_start_in_mm << " mm";
            if (distance_tcp_to_start_in_mm > config.General.MaximalDistanceToStartInMM)
            {
                ARMARX_WARNING << "Maximal distance between start and TCP exceeded: " << distance_tcp_to_start_in_mm
                               << " mm > " << config.General.MaximalDistanceToStartInMM << " mm";
                ARMARX_WARNING << "Motion is not started. Are you in the right configuration to start this motion?";
                return;
            }
        }


        CycleUtil c(IceUtil::Time::milliSeconds(10));
        while (!stop && !controlTask->isStopped())
        {
            synchronizeLocalClone(robot);

            posHelper.updateRead();

            bool targetReached = posHelper.isFinalTargetReached();

            if (targetReached)
            {
                posHelper.immediateHardStop();
                // TODO: Indicate success
                ARMARX_IMPORTANT << "Goal reached!";
                break;
            }

            posHelper.updateWrite();

            c.waitForCycleDuration();
        }

        posHelper.immediateHardStop();
        posHelper.updateWrite();
    }

    void PickAndPlaceComponent::executePreposeReverse(const ControlInfo& control)
    {
        std::string side = control.side;
        RobotNameHelper::Arm arm = robotNameHelper->getArm(side);
        RobotNameHelper::RobotArm robotArm = arm.addRobot(robot);
        VirtualRobot::RobotNodeSetPtr rns = robotArm.getKinematicChain();
        VirtualRobot::RobotNodePtr tcp = robotArm.getTCP();

        Armar6HandV2Helper handHelper(kinematicUnit, robotUnitObserver, side, config.General.IsSimulation);
        std::string controllerName = getName() + "_" + side;
        VelocityControllerHelperPtr velHelper(new VelocityControllerHelper(robotUnit, arm.getKinematicChain(), controllerName));
        //ForceTorqueHelper ftHelper(getForceTorqueObserver(), arm.getForceTorqueSensor());

        Eigen::Matrix4f initialTcpPose = tcp->getPoseInRootFrame();

        PositionControllerHelper posHelper(tcp, velHelper, initialTcpPose);
        {
            CartesianPositionControllerConfigPtr controllerConfig = new CartesianPositionControllerConfig();
            // TODO: Move into
            controllerConfig->KpOri = 1.0f;
            controllerConfig->KpPos = 1.0f;
            controllerConfig->maxOriVel = 1.0f;
            controllerConfig->maxPosVel = 160.0f;
            controllerConfig->thresholdOrientationNear = 0.1f;
            controllerConfig->thresholdOrientationReached = 0.05f;
            controllerConfig->thresholdPositionNear = 40.0f;
            controllerConfig->thresholdPositionReached = 5.0f;
            posHelper.readConfig(controllerConfig);
        }

        auto waypoints = control.side == "Left" ? config.Prepose.Left.Waypoints : config.Prepose.Right.Waypoints;
        std::reverse(waypoints.begin(), waypoints.end());
        for (auto& waypoint : waypoints)
        {
            posHelper.addWaypoint(waypoint);
        }

        // Initial sync to get the right values
        synchronizeLocalClone(robot);

        Eigen::Vector3f initialTcpPosition = simox::math::position(initialTcpPose);
        if (posHelper.waypoints.size() > 1)
        {
            Eigen::Vector3f start_position = simox::math::position(posHelper.waypoints[1]);
            Eigen::Vector3f tcp_to_start = start_position - initialTcpPosition;
            float distance_tcp_to_start_in_mm = tcp_to_start.norm();
            ARMARX_INFO << "Distance TCP <-> start: " << distance_tcp_to_start_in_mm << " mm";
            if (distance_tcp_to_start_in_mm > config.General.MaximalDistanceToStartInMM)
            {
                ARMARX_WARNING << "Maximal distance between start and TCP exceeded: " << distance_tcp_to_start_in_mm
                               << " mm > " << config.General.MaximalDistanceToStartInMM << " mm";
                ARMARX_WARNING << "Motion is not started. Are you in the right configuration to start this motion?";
                return;
            }
        }


        CycleUtil c(IceUtil::Time::milliSeconds(10));
        while (!stop && !controlTask->isStopped())
        {
            synchronizeLocalClone(robot);

            posHelper.updateRead();

            bool targetReached = posHelper.isFinalTargetReached();

            if (targetReached)
            {
                posHelper.immediateHardStop();
                // TODO: Indicate success
                ARMARX_IMPORTANT << "Goal reached!";
                break;
            }

            posHelper.updateWrite();

            c.waitForCycleDuration();
        }

        posHelper.immediateHardStop();
        posHelper.updateWrite();
    }

    void PickAndPlaceComponent::executePush()
    {
        ARMARX_WARNING << "Not yet implemented";
    }

    enum class PhaseType
    {
        ApproachPrePose,
        ApproachObject,
        SimpleGrasp,
        TrajectoryGrasp,
        Lift,
        Replace,
        Place,
        RetractAfterPlace
    };
    enum class GraspProcedureType
    {
        Simple,
        Trajectory
    };

    void PickAndPlaceComponent::executePick(const ControlInfo& control)
    {
        if (!control.grasp)
        {
            ARMARX_WARNING << "No grasp selected!";
            return;
        }
        if (!control.grasp->reachabilityInfo->reachable)
        {
            ARMARX_WARNING << "Grasp is not reachable!";
            return;
        }
        Eigen::Vector3f liftVector(0.0f, 0.0f, 200.0f);

        std::string side = control.side;
        if (side == "Left")
        {
            //            ARMARX_WARNING << "Picking with the left hand is not yet implemented.";
            //            return;
        }
        RobotNameHelper::Arm arm = robotNameHelper->getArm(side);
        RobotNameHelper::RobotArm robotArm = arm.addRobot(robot);
        VirtualRobot::RobotNodeSetPtr rns = robotArm.getKinematicChain();
        VirtualRobot::RobotNodePtr tcp = robotArm.getTCP();

        Armar6HandV2Helper handHelper(kinematicUnit, robotUnitObserver, side, config.General.IsSimulation);
        std::string controllerName = getName() + "_" + side;
        VelocityControllerHelperPtr velHelper(new VelocityControllerHelper(robotUnit, arm.getKinematicChain(), controllerName));
        ForceTorqueHelper ftHelper(forceTorqueObserver, arm.getForceTorqueSensor());

        synchronizeLocalClone(robot);

        // BEGIN
        GraspProcedureType graspProcedure = GraspProcedureType::Trajectory;

        // TODO: This needs to be configurable
        Armar6GraspTrajectoryPtr graspTrajectory = graspTrajectories[control.grasp->executionHints->graspTrajectoryName];
        ARMARX_INFO << "Loaded trajectory: " << control.grasp->executionHints->graspTrajectoryName;

        ARMARX_IMPORTANT << "Read grasp trajectory, length: " << graspTrajectory->calculateLength().pos << ", duration: " << graspTrajectory->getDuration();
        handHelper.shapeHand(graspTrajectory->GetHandValues(0));

        // TODO: Make configurable
        std::string graspType = "Top";
        Eigen::Vector3f graspPositionOffset(0.0f, 0.0f, 0.0f);
        float minimumFingerShape = 0.0f;
        if (control.grasp->executionHints->graspTrajectoryName == "RightSideOpen")
        {
            graspType = "Side";

            // Do some preshaping
            minimumFingerShape = 0.30f;
            graspPositionOffset = Eigen::Vector3f(0.0f, 10.0f, 0.0f);
            if (control.side == "Left")
            {
                graspPositionOffset(0) = -10.0f;
            }
            else
            {
                handHelper.shapeHand(minimumFingerShape, 0.0f);
            }
        }

        Eigen::Vector3f approachVector = Vector3Ptr::dynamicCast(control.grasp->approachVector)->toEigen();
        float approachDistance = 120.0f;
        Eigen::Matrix4f graspPose = PosePtr::dynamicCast(control.grasp->graspPose)->toEigen();
        math::Helpers::Position(graspPose) = math::Helpers::Position(graspPose) + graspPositionOffset;
        Eigen::Matrix4f prePose1 = math::Helpers::TranslatePose(graspPose, approachVector * approachDistance);
        Eigen::Matrix4f prePose2 = math::Helpers::TranslatePose(graspPose, approachVector * approachDistance / 2);
        // TODO: Let's do placing later
        //Eigen::Matrix4f placePose = in.getPlacePose()->toEigen();



        std::vector<Eigen::Matrix4f> waypoints;

        math::LinearInterpolatedPose linPose(tcp->getPoseInRootFrame(), prePose1, 0, 1, true);
        waypoints.push_back(linPose.Get(0.5f));
        waypoints.push_back(prePose1);
        waypoints.push_back(prePose2);

        ARMARX_INFO << VAROUT(waypoints);
        ARMARX_INFO << VAROUT(graspPose);

        // END

        PositionControllerHelper posHelper(tcp, velHelper, waypoints);
        {
            CartesianPositionControllerConfigPtr controllerConfig = new CartesianPositionControllerConfig();
            // TODO: Move into
            controllerConfig->KpOri = 1.0f;
            controllerConfig->KpPos = 1.0f;
            controllerConfig->maxOriVel = 1.0f;
            controllerConfig->maxPosVel = 80.0f;
            controllerConfig->thresholdOrientationNear = 0.1f;
            controllerConfig->thresholdOrientationReached = 0.05f;
            controllerConfig->thresholdPositionNear = 40.0f;
            controllerConfig->thresholdPositionReached = 5.0f;
            posHelper.readConfig(controllerConfig);
        }

        //        // Generate a grasp pose for the object
        //        Eigen::Matrix4f objectPoseRobot = PosePtr::dynamicCast(control.objectPose.objectPoseRobot)->toEigen();
        //        ARMARX_INFO << "Trying to pick up object: " << control.objectPose.objectID.name;
        //        ARMARX_INFO << "At pose: " << objectPoseRobot;

        //        Eigen::Matrix4f graspPoseRobot = PosePtr::dynamicCast(control.grasp->graspPose)->toEigen();
        //        posHelper.addWaypoint(graspPoseRobot);

        float defaultMaxPosVel = posHelper.posController.maxPosVel;

        PhaseType currentPhase = PhaseType::ApproachPrePose;

        IceUtil::Time graspStartTime;

        CycleUtil c(IceUtil::Time::milliSeconds(10));
        while (!stop && !controlTask->isStopped())
        {
            synchronizeLocalClone(robot);

            posHelper.updateRead();

            // BEGIN

            float positionError = posHelper.getPositionError();
            float orientationError = posHelper.getOrientationError();
            //ARMARX_INFO << deactivateSpam(1) << VAROUT(positionError) << " " << VAROUT(orientationError) << " " << VAROUT(cv.transpose());

            debugObserver->setDebugDatafield("CartesianPositionControl", "positionError", new Variant(positionError));
            debugObserver->setDebugDatafield("CartesianPositionControl", "orientationError", new Variant(orientationError));


            PhaseType nextPhase = currentPhase;
            bool targetReached = posHelper.isFinalTargetReached();
            bool targetNear = posHelper.isFinalTargetNear();

            const Eigen::Vector3f currentForce = ftHelper.getForce();
            const Eigen::Vector3f currentTorque = ftHelper.getTorque();

            debugObserver->setDebugDatafield("ExecuteGrasp", "force_x", new Variant(currentForce(0)));
            debugObserver->setDebugDatafield("ExecuteGrasp", "force_y", new Variant(currentForce(1)));
            debugObserver->setDebugDatafield("ExecuteGrasp", "force_z", new Variant(currentForce(2)));
            debugObserver->setDebugDatafield("ExecuteGrasp", "torque_x", new Variant(currentTorque(0)));
            debugObserver->setDebugDatafield("ExecuteGrasp", "torque_y", new Variant(currentTorque(1)));
            debugObserver->setDebugDatafield("ExecuteGrasp", "torque_z", new Variant(currentTorque(2)));


            bool forceExceeded = currentForce.norm() > config.General.ForceThresholdInPotatoes;
            Eigen::Vector3f tcpPos = tcp->getPositionInRootFrame();
            Eigen::Matrix4f tcpPose = tcp->getPoseInRootFrame();
            if (config.General.IsSimulation && tcpPos(2) < config.General.SimulationForceHeightThresholdInMM)
            {
                ARMARX_IMPORTANT << "Simulating force exceeded";
                forceExceeded = true;
            }

            if (currentPhase == PhaseType::ApproachPrePose)
            {
                //getMessageDisplay()->setMessage("Approaching Pre Pose", posHelper.getStatusText());
                if (targetReached)
                {
                    ARMARX_IMPORTANT << "PrePose reached";
                    if (graspType == "Top")
                    {
                        Eigen::Vector3f TopGraspingVector = Eigen::Vector3f(0.0f, 0.0f, -100.0f);
                        ARMARX_IMPORTANT << "graspType = Top: Adding top grasping vector";
                        posHelper.setTarget(math::Helpers::TranslatePose(graspPose, TopGraspingVector));
                        posHelper.posController.maxPosVel = defaultMaxPosVel / 2;
                        ARMARX_IMPORTANT << "setting posController.maxPosVel = " << posHelper.posController.maxPosVel;
                    }
                    else
                    {
                        posHelper.setTarget(graspPose);
                    }
                    velHelper->controller->setKpJointLimitAvoidance(0);
                    velHelper->controller->setJointLimitAvoidanceScale(0);
                    ftHelper.setZero();
                    nextPhase = PhaseType::ApproachObject;
                }
            }
            if (currentPhase == PhaseType::ApproachObject)
            {
                //getMessageDisplay()->setMessage("Approaching Object", posHelper.getStatusText());

                if (forceExceeded || targetReached)
                {
                    posHelper.immediateHardStop();
                    posHelper.posController.maxPosVel = defaultMaxPosVel;
                    graspStartTime = TimeUtil::GetTime();

                    if (graspProcedure == GraspProcedureType::Simple)
                    {
                        nextPhase = PhaseType::SimpleGrasp;
                        Eigen::Matrix4f target = tcp->getPoseInRootFrame();
                        //target(2, 3) = fmax(target(2, 3), in.getGraspMinZHeight());
                        Eigen::Vector3f offsetDuringGrasp(0.0f, 0.0f, 0.0f);
                        target = ::math::Helpers::TranslatePose(target, offsetDuringGrasp);
                        posHelper.setTarget(target);

                    }
                    else if (graspProcedure == GraspProcedureType::Trajectory)
                    {
                        nextPhase = PhaseType::TrajectoryGrasp;

                        Eigen::Matrix4f startPose = graspTrajectory->getStartPose();
                        Eigen::Vector3f currentHandForward = ::math::Helpers::TransformDirection(tcpPose, handHelper.getHandForwardVector());
                        Eigen::Vector3f trajHandForward = ::math::Helpers::TransformDirection(startPose, handHelper.getHandForwardVector());
                        Eigen::Vector3f up(0, 0, 1);
                        //                        if (control.side == "Left")
                        //                        {
                        //                            up = Eigen::Vector3f(0, 0, -1);
                        //                        }

                        float angle = ::math::Helpers::Angle(currentHandForward, trajHandForward, up);
                        ARMARX_IMPORTANT << "Angle: " << angle;
                        Eigen::AngleAxisf aa(angle, up);

                        Eigen::Matrix4f transform = ::math::Helpers::CreateTranslationRotationTranslationPose(-math::Helpers::GetPosition(startPose), aa.toRotationMatrix(), math::Helpers::GetPosition(tcpPose));
                        graspTrajectory = graspTrajectory->getTransformed(transform);
                        //                        if (control.side == "Left")
                        //                        {
                        //                            for (int i = 0; i < (int)graspTrajectory->getKeypointCount(); ++i)
                        //                            {
                        //                                Armar6GraspTrajectory::Keypoint& kp = *graspTrajectory->getKeypoint(i);
                        //                                Eigen::Matrix4f target = kp.tcpTarget;
                        //                                target.block<3, 3>(0, 0) = Eigen::AngleAxisf(M_PI, Eigen::Vector3f::UnitY()) * target.block<3, 3>(0, 0);
                        //                            }
                        //                            graspTrajectory->updateKeypointMap();
                        //                        }
                    }
                    else
                    {
                        throw LocalException("Unsupported GraspProcedureType");
                    }

                }
            }
            if (currentPhase == PhaseType::TrajectoryGrasp)
            {
                IceUtil::Time now = TimeUtil::GetTime();
                float t = (now - graspStartTime).toSecondsDouble();
                float duration = graspTrajectory->getDuration();
                if (t > duration)
                {
                    t = graspTrajectory->getDuration();
                    nextPhase = PhaseType::Lift;
                    attachObjectToRobotPose(control.objectPose, tcp);

                    Eigen::Matrix4f target = tcp->getPoseInRootFrame();
                    target.block<3, 1>(0, 3) += liftVector;
                    posHelper.setTarget(target);
                }
                else
                {
                    Eigen::Matrix4f target = graspTrajectory->GetPose(t);
                    posHelper.setTarget(target);
                    posHelper.setFeedForwardVelocity(graspTrajectory->GetPositionDerivative(t), graspTrajectory->GetOrientationDerivative(t));
                    Eigen::Vector3f handShape = graspTrajectory->GetHandValues(t);
                    handShape(0) = std::max(handShape(0), minimumFingerShape);
                    handHelper.shapeHand(handShape);
                }
            }
            if (currentPhase == PhaseType::Lift)
            {
                if (targetReached)
                {
                    posHelper.immediateHardStop();
                    ARMARX_IMPORTANT << "Object grasped and lifted!";
                    break;
                }
            }
            currentPhase = nextPhase;


            // END

            posHelper.updateWrite();

            c.waitForCycleDuration();
        }

        posHelper.immediateHardStop();
        posHelper.updateWrite();
    }

    void PickAndPlaceComponent::executePlace(ControlInfo const& control)
    {
        std::string side = control.side;
        RobotNameHelper::Arm arm = robotNameHelper->getArm(side);
        RobotNameHelper::RobotArm robotArm = arm.addRobot(robot);
        VirtualRobot::RobotNodeSetPtr rns = robotArm.getKinematicChain();
        VirtualRobot::RobotNodePtr tcp = robotArm.getTCP();

        Armar6HandV2Helper handHelper(kinematicUnit, robotUnitObserver, side, config.General.IsSimulation);
        std::string controllerName = getName() + "_" + side;
        VelocityControllerHelperPtr velHelper(new VelocityControllerHelper(robotUnit, arm.getKinematicChain(), controllerName));
        ForceTorqueHelper ftHelper(forceTorqueObserver, arm.getForceTorqueSensor());

        synchronizeLocalClone(robot);
        Eigen::Matrix4f robotPoseGlobal = robot->getGlobalPose();

        // BEGIN

        // END

        Eigen::Matrix4f initialTcpPose = tcp->getPoseInRootFrame();
        PositionControllerHelper posHelper(tcp, velHelper, initialTcpPose);
        {
            CartesianPositionControllerConfigPtr controllerConfig = new CartesianPositionControllerConfig();
            // TODO: Move into
            controllerConfig->KpOri = 1.0f;
            controllerConfig->KpPos = 1.0f;
            controllerConfig->maxOriVel = 1.0f;
            controllerConfig->maxPosVel = 80.0f;
            controllerConfig->thresholdOrientationNear = 0.1f;
            controllerConfig->thresholdOrientationReached = 0.05f;
            controllerConfig->thresholdPositionNear = 40.0f;
            controllerConfig->thresholdPositionReached = 5.0f;
            posHelper.readConfig(controllerConfig);
        }

        PhaseType currentPhase = PhaseType::Replace;

        Eigen::Matrix4f startPose = tcp->getPoseInRootFrame();
        // Make an intermediate pose which only moves in x,y not z
        Eigen::Vector3f placePositionRobot = (robotPoseGlobal.inverse() * control.placePositionGlobal.homogeneous()).head<3>();
        Eigen::Matrix4f intermediatePose = startPose;
        intermediatePose(0, 3) = placePositionRobot(0);
        intermediatePose(1, 3) = placePositionRobot(1);
        Eigen::Matrix4f placePose = intermediatePose;
        placePose(2, 3) = placePositionRobot(2);
        math::Helpers::Position(placePose) = placePositionRobot;

        std::vector<Eigen::Matrix4f> placeWaypoints;
        placeWaypoints.push_back(startPose);
        placeWaypoints.push_back(intermediatePose);
        //placeWaypoints.push_back(placePose);

        posHelper.setNewWaypoints(placeWaypoints);

        CycleUtil c(IceUtil::Time::milliSeconds(10));
        while (!stop && !controlTask->isStopped())
        {
            synchronizeLocalClone(robot);

            posHelper.updateRead();

            // BEGIN

            float positionError = posHelper.getPositionError();
            float orientationError = posHelper.getOrientationError();
            //ARMARX_INFO << deactivateSpam(1) << VAROUT(positionError) << " " << VAROUT(orientationError) << " " << VAROUT(cv.transpose());

            debugObserver->setDebugDatafield("CartesianPositionControl", "positionError", new Variant(positionError));
            debugObserver->setDebugDatafield("CartesianPositionControl", "orientationError", new Variant(orientationError));


            PhaseType nextPhase = currentPhase;
            bool targetReached = posHelper.isFinalTargetReached();
            bool targetNear = posHelper.isFinalTargetNear();

            const Eigen::Vector3f currentForce = ftHelper.getForce();
            const Eigen::Vector3f currentTorque = ftHelper.getTorque();

            debugObserver->setDebugDatafield("ExecuteGrasp", "force_x", new Variant(currentForce(0)));
            debugObserver->setDebugDatafield("ExecuteGrasp", "force_y", new Variant(currentForce(1)));
            debugObserver->setDebugDatafield("ExecuteGrasp", "force_z", new Variant(currentForce(2)));
            debugObserver->setDebugDatafield("ExecuteGrasp", "torque_x", new Variant(currentTorque(0)));
            debugObserver->setDebugDatafield("ExecuteGrasp", "torque_y", new Variant(currentTorque(1)));
            debugObserver->setDebugDatafield("ExecuteGrasp", "torque_z", new Variant(currentTorque(2)));


            bool forceExceeded = currentForce.norm() > config.General.ForceThresholdInPotatoes;
            Eigen::Vector3f tcpPos = tcp->getPositionInRootFrame();
            if (config.General.IsSimulation && tcpPos(2) < config.General.SimulationForceHeightThresholdInMM)
            {
                ARMARX_IMPORTANT << "Simulating force exceeded";
                forceExceeded = true;
            }

            if (currentPhase == PhaseType::Replace)
            {
                //getMessageDisplay()->setMessage("Replacing", "");
                if (targetNear)
                {
                    ftHelper.setZero();
                    // TODO: Do we need to place it further down (offset) to trigger the force reaction?
                    posHelper.setTarget(placePose);
                    nextPhase = PhaseType::Place;
                }
            }
            if (currentPhase == PhaseType::Place)
            {
                {
                    //                    std::stringstream ss;
                    //                    ss << "Force: " << currentForce.norm();
                    //                    getMessageDisplay()->setMessage("Placing", ss.str());
                }
                if (forceExceeded || targetReached)
                {
                    posHelper.immediateHardStop();
                    handHelper.shapeHand(0, 0);
                    // TODO: Set waypoints here!
                    posHelper.setTarget(math::Helpers::TranslatePose(placePose, Eigen::Vector3f(0, 0, 300)));
                    // TODO: Where should we retract to after placing?
                    //posHelper.addWaypoint(in.getRetractAfterPlacePose()->toEigen());
                    //posHelper.addWaypoint(in.getFinalPrePose()->toEigen());
                    //posHelper.addWaypoint(in.getFinalPose()->toEigen());
                    // TODO: Are these adjustments necessary?
                    //                    velHelper->controller->setJointLimitAvoidanceScale(2);
                    //                    velHelper->controller->setKpJointLimitAvoidance(1);
                    nextPhase = PhaseType::RetractAfterPlace;

                    detachAllObjects();

                }
            }
            if (currentPhase == PhaseType::RetractAfterPlace)
            {
                //                getMessageDisplay()->setMessage("Move back", posHelper.getStatusText());
                if (targetReached)
                {
                    posHelper.immediateHardStop();
                    break;
                }
            }

            currentPhase = nextPhase;


            // END

            posHelper.updateWrite();

            c.waitForCycleDuration();
        }

        posHelper.immediateHardStop();
        posHelper.updateWrite();
    }

    void PickAndPlaceComponent::executeSupportPrepose(ControlInfo const& control)
    {
        std::string side = control.side;
        if (side == "Right")
        {
            ARMARX_WARNING << "Right side not supported";
            return;
        }
        RobotNameHelper::Arm arm = robotNameHelper->getArm(side);
        RobotNameHelper::RobotArm robotArm = arm.addRobot(robot);
        VirtualRobot::RobotNodeSetPtr rns = robotArm.getKinematicChain();
        VirtualRobot::RobotNodePtr tcp = robotArm.getTCP();

        Armar6HandV2Helper handHelper(kinematicUnit, robotUnitObserver, side, config.General.IsSimulation);
        std::string controllerName = getName() + "_" + side;
        VelocityControllerHelperPtr velHelper(new VelocityControllerHelper(robotUnit, arm.getKinematicChain(), controllerName));
        //ForceTorqueHelper ftHelper(getForceTorqueObserver(), arm.getForceTorqueSensor());

        // Initial sync to get the right values
        synchronizeLocalClone(robot);

        Eigen::Matrix4f initial_tcp_pose = tcp->getPoseInRootFrame();

        PositionControllerHelper posHelper(tcp, velHelper, initial_tcp_pose);
        {
            CartesianPositionControllerConfigPtr controllerConfig = new CartesianPositionControllerConfig();
            // TODO: Move into
            controllerConfig->KpOri = 1.0f;
            controllerConfig->KpPos = 1.0f;
            controllerConfig->maxOriVel = 1.0f;
            controllerConfig->maxPosVel = 160.0f;
            controllerConfig->thresholdOrientationNear = 0.1f;
            controllerConfig->thresholdOrientationReached = 0.05f;
            controllerConfig->thresholdPositionNear = 40.0f;
            controllerConfig->thresholdPositionReached = 5.0f;
            posHelper.readConfig(controllerConfig);
        }

        auto waypoints = control.side == "Left" ? config.Support.Left.Waypoints : config.Support.Right.Waypoints;
        for (auto& waypoint : waypoints)
        {
            posHelper.addWaypoint(waypoint);
        }

        Eigen::Vector3f initialTcpPosition = simox::math::position(initial_tcp_pose);
        if (posHelper.waypoints.size() > 1)
        {
            Eigen::Vector3f start_position = simox::math::position(posHelper.waypoints[1]);
            Eigen::Vector3f tcp_to_start = start_position - initialTcpPosition;
            float distance_tcp_to_start_in_mm = tcp_to_start.norm();
            ARMARX_INFO << "Distance TCP <-> start: " << distance_tcp_to_start_in_mm << " mm";
            if (distance_tcp_to_start_in_mm > config.General.MaximalDistanceToStartInMM)
            {
                ARMARX_WARNING << "Maximal distance between start and TCP exceeded: " << distance_tcp_to_start_in_mm
                               << " mm > " << config.General.MaximalDistanceToStartInMM << " mm";
                ARMARX_WARNING << "Motion is not started. Are you in the right configuration to start this motion?";
                return;
            }
        }


        CycleUtil c(IceUtil::Time::milliSeconds(10));
        while (!stop && !controlTask->isStopped())
        {
            synchronizeLocalClone(robot);

            posHelper.updateRead();

            bool targetReached = posHelper.isFinalTargetReached();

            if (targetReached)
            {
                posHelper.immediateHardStop();
                // TODO: Indicate success
                ARMARX_IMPORTANT << "Goal reached!";
                break;
            }

            posHelper.updateWrite();

            c.waitForCycleDuration();
        }

        posHelper.immediateHardStop();
        posHelper.updateWrite();
    }

    void PickAndPlaceComponent::executeSupportPreposeReverse(const ControlInfo& control)
    {
        std::string side = control.side;
        RobotNameHelper::Arm arm = robotNameHelper->getArm(side);
        RobotNameHelper::RobotArm robotArm = arm.addRobot(robot);
        VirtualRobot::RobotNodeSetPtr rns = robotArm.getKinematicChain();
        VirtualRobot::RobotNodePtr tcp = robotArm.getTCP();

        Armar6HandV2Helper handHelper(kinematicUnit, robotUnitObserver, side, config.General.IsSimulation);
        std::string controllerName = getName() + "_" + side;
        VelocityControllerHelperPtr velHelper(new VelocityControllerHelper(robotUnit, arm.getKinematicChain(), controllerName));
        //ForceTorqueHelper ftHelper(getForceTorqueObserver(), arm.getForceTorqueSensor());

        Eigen::Matrix4f initialTcpPose = tcp->getPoseInRootFrame();

        PositionControllerHelper posHelper(tcp, velHelper, initialTcpPose);
        {
            CartesianPositionControllerConfigPtr controllerConfig = new CartesianPositionControllerConfig();
            // TODO: Move into
            controllerConfig->KpOri = 1.0f;
            controllerConfig->KpPos = 1.0f;
            controllerConfig->maxOriVel = 1.0f;
            controllerConfig->maxPosVel = 160.0f;
            controllerConfig->thresholdOrientationNear = 0.1f;
            controllerConfig->thresholdOrientationReached = 0.05f;
            controllerConfig->thresholdPositionNear = 40.0f;
            controllerConfig->thresholdPositionReached = 5.0f;
            posHelper.readConfig(controllerConfig);
        }

        auto waypoints = control.side == "Left" ? config.Support.Left.Waypoints : config.Support.Right.Waypoints;
        std::reverse(waypoints.begin(), waypoints.end());
        for (auto& waypoint : waypoints)
        {
            posHelper.addWaypoint(waypoint);
        }

        // Initial sync to get the right values
        synchronizeLocalClone(robot);

        Eigen::Vector3f initialTcpPosition = simox::math::position(initialTcpPose);
        if (posHelper.waypoints.size() > 1)
        {
            Eigen::Vector3f start_position = simox::math::position(posHelper.waypoints[1]);
            Eigen::Vector3f tcp_to_start = start_position - initialTcpPosition;
            float distance_tcp_to_start_in_mm = tcp_to_start.norm();
            ARMARX_INFO << "Distance TCP <-> start: " << distance_tcp_to_start_in_mm << " mm";
            if (distance_tcp_to_start_in_mm > config.General.MaximalDistanceToStartInMM)
            {
                ARMARX_WARNING << "Maximal distance between start and TCP exceeded: " << distance_tcp_to_start_in_mm
                               << " mm > " << config.General.MaximalDistanceToStartInMM << " mm";
                ARMARX_WARNING << "Motion is not started. Are you in the right configuration to start this motion?";
                return;
            }
        }


        CycleUtil c(IceUtil::Time::milliSeconds(10));
        while (!stop && !controlTask->isStopped())
        {
            synchronizeLocalClone(robot);

            posHelper.updateRead();

            bool targetReached = posHelper.isFinalTargetReached();

            if (targetReached)
            {
                posHelper.immediateHardStop();
                // TODO: Indicate success
                ARMARX_IMPORTANT << "Goal reached!";
                break;
            }

            posHelper.updateWrite();

            c.waitForCycleDuration();
        }

        posHelper.immediateHardStop();
        posHelper.updateWrite();
    }

    void PickAndPlaceComponent::executeSupport(const ControlInfo& control)
    {
        std::string side = control.side;
        if (side == "Right")
        {
            ARMARX_WARNING << "Right side not supported";
            return;
        }
        RobotNameHelper::Arm arm = robotNameHelper->getArm(side);
        RobotNameHelper::RobotArm robotArm = arm.addRobot(robot);
        VirtualRobot::RobotNodeSetPtr rns = robotArm.getKinematicChain();
        VirtualRobot::RobotNodePtr tcp = robotArm.getTCP();

        Armar6HandV2Helper handHelper(kinematicUnit, robotUnitObserver, side, config.General.IsSimulation);
        std::string controllerName = getName() + "_" + side;
        VelocityControllerHelperPtr velHelper(new VelocityControllerHelper(robotUnit, arm.getKinematicChain(), controllerName));
        //ForceTorqueHelper ftHelper(getForceTorqueObserver(), arm.getForceTorqueSensor());

        // Initial sync to get the right values
        synchronizeLocalClone(robot);

        Eigen::Matrix4f initial_tcp_pose = tcp->getPoseInRootFrame();

        PositionControllerHelper posHelper(tcp, velHelper, initial_tcp_pose);
        {
            CartesianPositionControllerConfigPtr controllerConfig = new CartesianPositionControllerConfig();
            // TODO: Move into
            controllerConfig->KpOri = 1.0f;
            controllerConfig->KpPos = 1.0f;
            controllerConfig->maxOriVel = 1.0f;
            controllerConfig->maxPosVel = 160.0f;
            controllerConfig->thresholdOrientationNear = 0.1f;
            controllerConfig->thresholdOrientationReached = 0.05f;
            controllerConfig->thresholdPositionNear = 40.0f;
            controllerConfig->thresholdPositionReached = 5.0f;
            posHelper.readConfig(controllerConfig);
        }

        auto waypointsPrepose = control.side == "Left" ? config.Support.Left.Waypoints : config.Support.Right.Waypoints;
        Eigen::Matrix4f referencePose = waypointsPrepose.back();
        Eigen::Vector3f supportPositionRobot = math::Helpers::Position(referencePose) + control.supportPositionOffset;
        Eigen::Matrix4f supportPose = referencePose;
        math::Helpers::Position(supportPose) = supportPositionRobot;

        Eigen::Matrix4f startPose = tcp->getPoseInRootFrame();
        std::vector<Eigen::Matrix4f> waypoints;
        waypoints.push_back(startPose);
        waypoints.push_back(supportPose);

        SimpleDiffIK::Reachability reachability = SimpleDiffIK::CalculateReachability(waypoints,
                Eigen::VectorXf::Zero(rns->getSize()),
                rns, tcp);
        if (!reachability.reachable)
        {
            ARMARX_WARNING << "Support position is not reachable";
            return;
        }

        for (auto& waypoint : waypoints)
        {
            posHelper.addWaypoint(waypoint);
        }

        CycleUtil c(IceUtil::Time::milliSeconds(10));
        while (!stop && !controlTask->isStopped())
        {
            synchronizeLocalClone(robot);

            posHelper.updateRead();

            bool targetReached = posHelper.isFinalTargetReached();

            if (targetReached)
            {
                posHelper.immediateHardStop();
                ARMARX_IMPORTANT << "Goal reached!";
                break;
            }

            posHelper.updateWrite();

            c.waitForCycleDuration();
        }

        posHelper.immediateHardStop();
        posHelper.updateWrite();
    }

    void PickAndPlaceComponent::executeSupportRetreat(const ControlInfo& control)
    {
        std::string side = control.side;
        if (side == "Right")
        {
            ARMARX_WARNING << "Right side not supported";
            return;
        }
        RobotNameHelper::Arm arm = robotNameHelper->getArm(side);
        RobotNameHelper::RobotArm robotArm = arm.addRobot(robot);
        VirtualRobot::RobotNodeSetPtr rns = robotArm.getKinematicChain();
        VirtualRobot::RobotNodePtr tcp = robotArm.getTCP();

        Armar6HandV2Helper handHelper(kinematicUnit, robotUnitObserver, side, config.General.IsSimulation);
        std::string controllerName = getName() + "_" + side;
        VelocityControllerHelperPtr velHelper(new VelocityControllerHelper(robotUnit, arm.getKinematicChain(), controllerName));
        //ForceTorqueHelper ftHelper(getForceTorqueObserver(), arm.getForceTorqueSensor());

        // Initial sync to get the right values
        synchronizeLocalClone(robot);

        Eigen::Matrix4f initial_tcp_pose = tcp->getPoseInRootFrame();

        PositionControllerHelper posHelper(tcp, velHelper, initial_tcp_pose);
        {
            CartesianPositionControllerConfigPtr controllerConfig = new CartesianPositionControllerConfig();
            // TODO: Move into
            controllerConfig->KpOri = 1.0f;
            controllerConfig->KpPos = 1.0f;
            controllerConfig->maxOriVel = 1.0f;
            controllerConfig->maxPosVel = 160.0f;
            controllerConfig->thresholdOrientationNear = 0.1f;
            controllerConfig->thresholdOrientationReached = 0.05f;
            controllerConfig->thresholdPositionNear = 40.0f;
            controllerConfig->thresholdPositionReached = 5.0f;
            posHelper.readConfig(controllerConfig);
        }

        auto waypoints = control.side == "Left" ? config.Support.Left.Waypoints : config.Support.Right.Waypoints;
        posHelper.addWaypoint(waypoints.back());

        CycleUtil c(IceUtil::Time::milliSeconds(10));
        while (!stop && !controlTask->isStopped())
        {
            synchronizeLocalClone(robot);

            posHelper.updateRead();

            bool targetReached = posHelper.isFinalTargetReached();

            if (targetReached)
            {
                posHelper.immediateHardStop();
                // TODO: Indicate success
                ARMARX_IMPORTANT << "Goal reached!";
                break;
            }

            posHelper.updateWrite();

            c.waitForCycleDuration();
        }

        posHelper.immediateHardStop();
        posHelper.updateWrite();
    }
}




