/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    InteractiveSceneExplorationX::ArmarXObjects::RansacUncertainty
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>

#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
#include <VisionX/libraries/VisionXComponentPlugins/SemanticGraphStorageComponentPlugin.h>

#include <InteractiveSceneExploration/PrimitiveFitting/ShapeModels.h>
#include <InteractiveSceneExploration/ObjectHypothesis/SceneDistribution.h>
#include <InteractiveSceneExploration/Datasets/DatasetLoader.h>


namespace armarx
{
    using namespace interactive_scene_exploration;
    using namespace RemoteGui::Client;

    struct RansacUncertaintyTab : Tab
    {
        ComboBox comboBoxPC;
        ComboBox comboBoxLabel;
        IntSlider sliderBox;
        IntSlider sliderPointsSize;
        Button buttonSampleOne;
        Button buttonSampleScene;
        Button buttonEstimateSupport;
        Button buttonAnimateSupport;
        Button buttonSavePCD;
        Button buttonEvaluate;
    };

    /**
     * @class RansacUncertaintyPropertyDefinitions
     * @brief Property definitions of `RansacUncertainty`.
     */
    class RansacUncertaintyPropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        RansacUncertaintyPropertyDefinitions(std::string prefix);
    };



    /**
     * @defgroup Component-RansacUncertainty RansacUncertainty
     * @ingroup InteractiveSceneExplorationX-Components
     * A description of the component RansacUncertainty.
     *
     * @class RansacUncertainty
     * @ingroup Component-RansacUncertainty
     * @brief Brief description of class RansacUncertainty.
     *
     * Detailed description of class RansacUncertainty.
     */
    class RansacUncertainty
        : virtual public armarx::Component
        , virtual public armarx::LightweightRemoteGuiComponentPluginUser
        , virtual public armarx::SemanticGraphStorageComponentPluginUser
        , virtual public armarx::plugins::ArVizComponentPluginUser

    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


    protected:

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:
        void createRemoteGui();
        void RemoteGui_update() override;

        void drawBox();
        void sampleCurrentObject();
        void sampleScene();
        void estimateSupportRelations();
        void animateSupportRelations();
        void savePCD();
        void evaluate();

        void estimateDistribution(pcl::PointCloud<pcl::PointXYZRGBL> const& pointcloud);

    private:
        std::string pointCloudFilename = "/home/paus/repos/semantic-object-relations/evaluation/pointclouds/R2.pcd";

        std::map<std::uint32_t, std::vector<interactive_scene_exploration::EvaluatedBox>> boxesPerSegment;
        std::map<std::uint32_t, std::vector<interactive_scene_exploration::EvaluatedCylinder>> cylindersPerSegment;

        Dataset dataset;
        interactive_scene_exploration::SceneDistribution sceneDistribution;

        RansacUncertaintyTab ui;

        int selectedPC = -1;
        int selectedLabel = -1;
        int selectedBoxIndex = 0;
        int selectedPointsSize = 4;
    };
}
