#include "IO.h"

// The pcd_io.h includes roughly 2600 headers recursively!
// Therefore, we encapsulate it here
#include <pcl/io/pcd_io.h>

namespace interactive_scene_exploration
{

    bool PointCloudIO::loadFromPCD(std::string const& filename,
                                   pcl::PointCloud<pcl::PointXYZRGBL>* outPointCloud)
    {
        return pcl::io::loadPCDFile<pcl::PointXYZRGBL>(filename, *outPointCloud) == 0;
    }

    bool PointCloudIO::loadFromPCD(std::string const& filename,
                                   pcl::PointCloud<pcl::PointXYZRGBA>* outPointCloud)
    {
        return pcl::io::loadPCDFile<pcl::PointXYZRGBA>(filename, *outPointCloud) == 0;
    }

    bool PointCloudIO::saveToPCD(std::string const& filename,
                                 pcl::PointCloud<pcl::PointXYZRGBL> const& pointCloud)
    {
        return pcl::io::savePCDFileBinary<pcl::PointXYZRGBL>(filename, pointCloud);
    }

}
