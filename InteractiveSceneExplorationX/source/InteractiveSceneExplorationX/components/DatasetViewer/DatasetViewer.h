/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    InteractiveSceneExplorationX::ArmarXObjects::DatasetViewer
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>
#include <ArmarXCore/core/Component.h>

#include <InteractiveSceneExploration/Datasets/KIT-SRD.h>

#include <mutex>

namespace armarx
{
    struct DatasetViewerPropertyDefinitions :
        armarx::ComponentPropertyDefinitions
    {
        DatasetViewerPropertyDefinitions(std::string prefix);
    };

    using namespace armarx::RemoteGui::Client;
    struct DatasetViewerTab : Tab
    {
        IntSpinBox entryIndex;

        CheckBox xFilter, yFilter, zFilter;
        FloatSpinBox xMin, xMax;
        FloatSpinBox yMin, yMax;
        FloatSpinBox zMin, zMax;
        IntSpinBox labelFrom, labelTo;

        Button relabel;
        Button save;
    };

    struct DatasetViewerState
    {
        int entryIndex = 0;
        bool xFilter = false;
        float xMin = 4000.0f;
        float xMax = 6000.0f;
        bool yFilter = false;
        float yMin = 4000.0f;
        float yMax = 6000.0f;
        bool zFilter = false;
        float zMin = 500.0f;
        float zMax = 2000.0f;

        int labelFrom = -1;
        int labelTo = -1;
    };


    struct DatasetViewer
        : virtual armarx::Component
        , virtual armarx::LightweightRemoteGuiComponentPluginUser
        , virtual armarx::ArVizComponentPluginUser
    {
        std::string getDefaultName() const override;

        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void createRemoteGuiTab();
        void RemoteGui_update() override;

        void selectEntry(int index);
        void relabel(int index);
        void save(int index);

        DatasetViewerTab tab;
        std::mutex stateMutex;
        DatasetViewerState state;

        interactive_scene_exploration::kit_srd::LoadedDataset dataset;
    };
}
