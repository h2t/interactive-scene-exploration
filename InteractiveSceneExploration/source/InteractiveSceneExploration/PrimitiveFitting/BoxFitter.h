#pragma once

#include "ShapeModels.h"

#include <InteractiveSceneExploration/Math/RandomEngine.h>

#include <Eigen/Core>

namespace interactive_scene_exploration
{

    struct BoxFitterParams
    {
        int maxIterations = 100;
        float distanceThreshold = 5.0f;
        float minInlierRate = 0.75f;
        float outlierRate = 0.01f;
    };

    struct BoxFitter
    {
        static const int MSS_SIZE = 5;
        // [x, y, z, qw, qx, qy, qz, w, h, d]
        static const int MODEL_SIZE = 10;

        // Each row contains a sample
        using SampleMatrix = Eigen::Matrix<float, MSS_SIZE, 3>;

        using ModelVector = Eigen::Matrix<float, MODEL_SIZE, 1>;

        static bool computeModelCoefficients(SampleMatrix const& samples,
                                             ModelVector* modelCoefficients);

        static SampleMatrix selectMSS(std::vector<Eigen::Vector3f> const& points,
                                      RandomEngine& randomEngine);

        static std::vector<EvaluatedBox> sampleModels(BoxFitterParams const& params,
                std::vector<Eigen::Vector3f> const& points,
                RandomEngine& randomEngine);

        //        static Evaluation evaluate(BoxFitterParams const& params,
        //                                   semrel::Sphere const& model,
        //                                   std::vector<Eigen::Vector3f> const& points);
    };

}
