#pragma once

#include "API.h"

#include <memory>
#include <string>

namespace humanoid_robot_skills
{

struct PlatformLocation
{
    float x = 0.0f;
    float y = 0.0f;
    float theta = 0.0f;
};

struct NamedPlatformLocation
{
    std::string mapName;
    std::string locationName;
};

struct Platform_MoveToLocation_Params
{
    float maxTranslationalVelocity = 300.0f;
    float maxRotationalVelocity = 0.3f;

    float positionReachedThreshold = 8.0f;
    float angleReachedThreshold = 0.05f;
};

struct Platform_MoveToLocation_Result
{
    PlatformLocation currentLocation;
    bool success = false;
};

struct Platform_LookUpNamedLocation_Result
{
    PlatformLocation location;
    bool success = false;
};

struct HRS_API PlatformInterface
{
    virtual ~PlatformInterface() = default;

    virtual Platform_MoveToLocation_Result moveToLocation(
            PlatformLocation targetLocation,
            Platform_MoveToLocation_Params const& params) = 0;

    virtual Platform_LookUpNamedLocation_Result lookUpNamedLocation(NamedPlatformLocation const& namedLocation) = 0;

    virtual PlatformLocation getCurrentLocation() = 0;
};

struct HRS_API Platform
{
    Platform(std::shared_ptr<PlatformInterface> const& impl);

    Platform(Platform&&) = default;

    Platform_MoveToLocation_Result moveToLocation(PlatformLocation targetLocation,
                                                  Platform_MoveToLocation_Params const& params);

    Platform_LookUpNamedLocation_Result lookUpNamedLocation(NamedPlatformLocation const& namedLocation);

    Platform_MoveToLocation_Result moveToNamedLocation(NamedPlatformLocation const& targetLocation,
                                                       Platform_MoveToLocation_Params const& params);

    PlatformLocation getCurrentLocation();

    std::shared_ptr<PlatformInterface> impl;
};

}
