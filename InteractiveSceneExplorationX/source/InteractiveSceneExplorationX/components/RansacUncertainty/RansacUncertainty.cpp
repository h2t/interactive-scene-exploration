/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    InteractiveSceneExplorationX::ArmarXObjects::RansacUncertainty
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RansacUncertainty.h"

#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>

#include <InteractiveSceneExploration/Evaluation/ProbabilisticSupportEvaluation.h>
#include <InteractiveSceneExploration/SupportRelation/ProbabilisticSupportGraph.h>
#include <InteractiveSceneExploration/SupportRelation/ProbabilisticSupportAnalysis.h>
#include <InteractiveSceneExploration/ObjectHypothesis/PointCloudToScene.h>
#include <InteractiveSceneExploration/Math/Relation.h>
#include <InteractiveSceneExploration/Math/Statistics.h>
#include <InteractiveSceneExploration/PointCloud/IO.h>

#include <SemanticObjectRelations/RelationGraph/json/AttributedGraph.h>
#include <SemanticObjectRelations/SupportAnalysis/json.h>
#include <SemanticObjectRelations/Hooks/Log.h>

#include <SimoxUtility/color/GlasbeyLUT.h>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/filters/voxel_grid.h>

using namespace interactive_scene_exploration;


namespace armarx
{
    ARMARX_DECOUPLED_REGISTER_COMPONENT(RansacUncertainty);

    static std::default_random_engine randomEngine;

    static viz::Color glasbeyLUT(int id, int alpha)
    {
        simox::Color sc = simox::color::GlasbeyLUT::at(id, alpha);
        viz::Color c;
        c.r = sc.r;
        c.g = sc.g;
        c.b = sc.b;
        c.a = sc.a;
        return c;
    }

    static void addShapeToLayer(semrel::Shape* shape, armarx::viz::Layer& layer)
    {
        int label = (int)shape->getID();
        viz::Color color = glasbeyLUT(label, 255);
        std::string suffix = std::to_string(label);

        if (semrel::Box* sampledBox = dynamic_cast<semrel::Box*>(shape))
        {
            viz::Box b = viz::Box("Box_" + suffix)
                         .position(sampledBox->getPosition())
                         .orientation(sampledBox->getOrientation())
                         .size(sampledBox->getExtents())
                         .color(color);
            layer.add(b);
        }
        else if (semrel::Cylinder* sampledCyl = dynamic_cast<semrel::Cylinder*>(shape))
        {
            viz::Cylinder c = viz::Cylinder("Cylinder_" + suffix)
                              .position(sampledCyl->getPosition())
                              .orientation(sampledCyl->getOrientation())
                              .radius(sampledCyl->getRadius())
                              .height(sampledCyl->getHeight())
                              .color(color);
            layer.add(c);
        }
        else if (semrel::Sphere* sampledSphere = dynamic_cast<semrel::Sphere*>(shape))
        {
            viz::Sphere s = viz::Sphere("Sphere_" + suffix)
                            .position(sampledSphere->getPosition())
                            .orientation(sampledSphere->getOrientation())
                            .radius(sampledSphere->getRadius())
                            .color(color);
            layer.add(s);
        }
        else
        {
            throw std::runtime_error("not yet implemented");
        }
    }

    using PointCloud = pcl::PointCloud<pcl::PointXYZRGBL>;

    static void visualizePointCloud(armarx::viz::Client& arviz,
                                    std::map<std::uint32_t, PointCloud::Ptr> const& segments,
                                    int pointSizeInPixels)
    {
        viz::Layer pointsSegmentedLayer = arviz.layer("PointCloudSegmented");
        viz::Layer pointsRGBLayer = arviz.layer("PointCloudRGB");

        for (auto& pair : segments)
        {
            std::uint32_t label = pair.first;
            pcl::PointCloud<pcl::PointXYZRGBL>::Ptr const& segment = pair.second;

            viz::PointCloud vizSeg = viz::PointCloud("Points_" + std::to_string(label))
                                     .pointSizeInPixels(pointSizeInPixels);
            viz::PointCloud vizRGB = viz::PointCloud("Points_" + std::to_string(label))
                                     .pointSizeInPixels(pointSizeInPixels);
            for (auto& p : *segment)
            {
                viz::ColoredPoint cp;
                cp.x = p.x;
                cp.y = p.y;
                cp.z = p.z;
                cp.color = glasbeyLUT(label, 255);
                vizSeg.addPoint(cp);

                cp.color.r = p.r;
                cp.color.g = p.g;
                cp.color.b = p.b;
                cp.color.a = 255;
                vizRGB.addPoint(cp);
            }
            pointsSegmentedLayer.add(vizSeg);
            pointsRGBLayer.add(vizRGB);
        }

        arviz.commit({pointsSegmentedLayer, pointsRGBLayer});
    }

    static void visualizeMeanOfDistribution(armarx::viz::Client& arviz,
                                            SceneDistribution const& distribution)
    {
        viz::Layer meanLayer = arviz.layer("MeanOfDistribution");
        for (auto& object : distribution.objects)
        {
            semrel::ShapePtr meanShape = object.getMeanShape();
            addShapeToLayer(meanShape.get(), meanLayer);
        }
        arviz.commit({meanLayer});
    }

    RansacUncertaintyPropertyDefinitions::RansacUncertaintyPropertyDefinitions(std::string prefix) :
        armarx::ComponentPropertyDefinitions(prefix)
    {
        //defineOptionalProperty<std::string>("MyProxyName", "MyProxy", "Name of the proxy to be used");
    }


    std::string RansacUncertainty::getDefaultName() const
    {
        return "RansacUncertainty";
    }


    void RansacUncertainty::onInitComponent()
    {
    }

    void RansacUncertainty::onConnectComponent()
    {
        // armarx::ArVizVisualizer::setAsImplementation(arviz);
        // semrel::VisualizerInterface::setMinimumVisuLevel(semrel::VisuLevel::VERBOSE);

#if 0
        std::string datasetFilepath = "/home/paus/projects/2020/interactive-scene-exploration/evaluation/data/OSD";
        dataset = DatasetLoader::load(DatasetName::OSD, datasetFilepath);
#else
        std::string datasetFilepath = "/home/paus/projects/2020/interactive-scene-exploration/InteractiveSceneExplorationX/data/InteractiveSceneExplorationX/dataset";
        dataset = DatasetLoader::load(DatasetName::KIT_SR, datasetFilepath);
#endif

        semrel::LogInterface::setMinimumLogLevel(semrel::LogLevel::ERROR);

        createRemoteGui();
        RemoteGui_startRunningTask();
    }


    void RansacUncertainty::onDisconnectComponent()
    {

    }


    void RansacUncertainty::onExitComponent()
    {

    }




    armarx::PropertyDefinitionsPtr RansacUncertainty::createPropertyDefinitions()
    {
        auto defs = armarx::PropertyDefinitionsPtr(new RansacUncertaintyPropertyDefinitions(
                        getConfigIdentifier()));

        defs->optional(pointCloudFilename, "PointCloudFilename", "Filename for the point cloud.");

        return defs;
    }

    void RansacUncertainty::createRemoteGui()
    {
        GridLayout grid;
        int row = 0;
        {
            Label label("PC:");
            grid.add(label, Pos{row, 0});
            std::vector<std::string> options;
            for (auto& entry : dataset.entries)
            {
                std::string pcName = entry.id;
                options.push_back(pcName);
            }
            if (options.empty())
            {
                options.push_back("<NONE>");
            }
            ui.comboBoxPC.setOptions(options);
            if (selectedPC != -1)
            {
                ui.comboBoxPC.setValue(options[selectedPC]);
            }
            grid.add(ui.comboBoxPC, Pos{row, 1});
            row += 1;
        }
        {
            Label label("Label:");
            grid.add(label, Pos{row, 0});
            std::vector<std::string> options;
            for (auto& pair : boxesPerSegment)
            {
                options.push_back(std::to_string(pair.first));
            }
            if (options.empty())
            {
                options.push_back("-1");
            }
            ui.comboBoxLabel.setOptions(options);
            if (selectedLabel != -1)
            {
                ui.comboBoxLabel.setValue(std::to_string(selectedLabel));
            }
            grid.add(ui.comboBoxLabel, Pos{row, 1});
            row += 1;
        }
        {
            Label label("Box:");
            grid.add(label, Pos{row, 0});
            int maxSize = 1;
            if (cylindersPerSegment.count(selectedLabel))
            {
                auto& boxes = cylindersPerSegment[selectedLabel];
                if (boxes.size() > 0)
                {
                    maxSize = boxes.size();
                }
            }
            if (selectedBoxIndex >= maxSize)
            {
                selectedBoxIndex = 0;
            }
            ui.sliderBox.setRange(0, maxSize);
            ui.sliderBox.setValue(selectedBoxIndex);

            grid.add(ui.sliderBox, Pos{row, 1});
            row += 1;
        }
        {
            grid.add(Label("Points Size:"), Pos{row, 0});
            ui.sliderPointsSize.setRange(1, 20);
            ui.sliderPointsSize.setValue(selectedPointsSize);

            grid.add(ui.sliderPointsSize, Pos{row, 1});
            row += 1;
        }
        {
            ui.buttonSampleOne.setLabel("Sample Selected Box");
            grid.add(ui.buttonSampleOne, Pos{row, 0}, Span{1, 2});
            row += 1;
        }
        {
            ui.buttonSampleScene.setLabel("Sample Scene");
            grid.add(ui.buttonSampleScene, Pos{row, 0}, Span{1, 2});
            row += 1;
        }
        {
            ui.buttonEstimateSupport.setLabel("Estimate Support Relations");
            grid.add(ui.buttonEstimateSupport, Pos{row, 0}, Span{1, 2});
            row += 1;
        }
        {
            ui.buttonAnimateSupport.setLabel("Animate Support Relations");
            grid.add(ui.buttonAnimateSupport, Pos{row, 0}, Span{1, 2});
            row += 1;
        }
        {
            ui.buttonSavePCD.setLabel("Save PCD");
            grid.add(ui.buttonSavePCD, Pos{row, 0}, Span{1, 2});
            row += 1;
        }
        {
            ui.buttonEvaluate.setLabel("Evaluate");
            grid.add(ui.buttonEvaluate, Pos{row, 0}, Span{1, 2});
            row += 1;
        }

        RemoteGui_createTab("RansacUncertainty", grid, &ui);
    }

    void RansacUncertainty::RemoteGui_update()
    {
        int pc = ui.comboBoxPC.getIndex();
        int label = std::stoi(ui.comboBoxLabel.getValue());
        int boxIndex = ui.sliderBox.getValue();
        int pointsSize = ui.sliderPointsSize.getValue();

        if (pc != selectedPC)
        {
            selectedPC = pc;

            // Downsample the cloud
            pcl::VoxelGrid<pcl::PointXYZRGBL> grid;
            PointCloud::Ptr input(new PointCloud(dataset.entries[pc].pointCloud));
            grid.setInputCloud(input);
            grid.setLeafSize(4.0f, 4.0f, 4.0f);
            PointCloud::Ptr filtered(new PointCloud);
            grid.filter(*filtered);

            //estimateDistribution(dataset.entries[pc].pointCloud);
            estimateDistribution(*filtered);
        }

        bool changed = label != selectedLabel ||
                       boxIndex != selectedBoxIndex ||
                       pc != selectedPC ||
                       pointsSize != selectedPointsSize;
        if (changed)
        {
            selectedLabel = label;
            selectedBoxIndex = boxIndex;
            selectedPointsSize = pointsSize;

            // Reload GUI
            createRemoteGui();

            drawBox();
        }

        if (ui.buttonSampleOne.wasClicked())
        {
            ARMARX_INFO << "Sampling...";
            sampleCurrentObject();
        }
        if (ui.buttonSampleScene.wasClicked())
        {
            sampleScene();
        }
        if (ui.buttonEstimateSupport.wasClicked())
        {
            estimateSupportRelations();
        }
        if (ui.buttonAnimateSupport.wasClicked())
        {
            animateSupportRelations();
        }
        if (ui.buttonSavePCD.wasClicked())
        {
            savePCD();
        }
        if (ui.buttonEvaluate.wasClicked())
        {
            evaluate();
        }
    }

    void RansacUncertainty::drawBox()
    {
        // TODO: Mutex needed
        if (selectedLabel < 0)
        {
            ARMARX_INFO << "No label selected";
            return;
        }
        auto& cyls = cylindersPerSegment[selectedLabel];
        if (selectedBoxIndex >= (int)cyls.size())
        {
            ARMARX_WARNING << "Wrong cylinder index '" << selectedBoxIndex << "' (size: " << cyls.size() << ")";
            return;
        }
#if 0
        auto& box = boxes[selectedBoxIndex];

        viz::Box b = viz::Box("Box")
                     .position(box.shape.getPosition())
                     .orientation(box.shape.getOrientation())
                     .size(box.shape.getExtents())
                     .color(glasbeyLUT(selectedLabel, 80));
        viz::Layer boxLayer = arviz.layer("Box");
        boxLayer.add(b);

        // TODO: Also add the point cloud segment

        ARMARX_INFO << "Committing box layer";
        arviz.commit(boxLayer);
#else
        auto& cyl = cyls[selectedBoxIndex];

        viz::Cylinder b = viz::Cylinder("Cylinder")
                          .position(cyl.shape.getPosition())
                          .orientation(cyl.shape.getOrientation())
                          .radius(cyl.shape.getRadius())
                          .height(cyl.shape.getHeight())
                          .color(glasbeyLUT(selectedLabel, 80));
        viz::Layer boxLayer = arviz.layer("Box");
        boxLayer.add(b);

        // Highlight the selected points
        {
            viz::Arrow arrow1 = viz::Arrow("normal1")
                                .fromTo(cyl.p1, cyl.p1 + 20.0f * cyl.n1)
                                .color(viz::Color::blue())
                                .width(2.0f);
            boxLayer.add(arrow1);
            viz::Arrow arrow2 = viz::Arrow("normal2")
                                .fromTo(cyl.p2, cyl.p2 + 20.0f * cyl.n2)
                                .color(viz::Color::blue())
                                .width(2.0f);
            boxLayer.add(arrow2);

            viz::Arrow line1 = viz::Arrow("line1")
                               .fromTo(cyl.p1 - 1000.0f * cyl.n1, cyl.p1 + 1000.0f * cyl.n1)
                               .color(viz::Color::green())
                               .width(0.2f);
            boxLayer.add(line1);
            viz::Arrow line2 = viz::Arrow("line2")
                               .fromTo(cyl.p2 - 1000.0f * cyl.n2, cyl.p2 + 1000.0f * cyl.n2)
                               .color(viz::Color::green())
                               .width(0.2f);
            boxLayer.add(line2);

            viz::Sphere pL1 = viz::Sphere("pL1")
                              .position(cyl.pointL1)
                              .radius(3.0f)
                              .color(viz::Color::cyan());
            boxLayer.add(pL1);

            viz::Sphere pL2 = viz::Sphere("pL2")
                              .position(cyl.pointL2)
                              .radius(3.0f)
                              .color(viz::Color::cyan());
            boxLayer.add(pL2);

            viz::Arrow direction = viz::Arrow("direction")
                                   .fromTo(cyl.pointL1 - 1000.0f * cyl.direction, cyl.pointL1 + 1000.0f * cyl.direction)
                                   .color(viz::Color::yellow())
                                   .width(0.4f);
            boxLayer.add(direction);

        }

        // TODO: Also add the point cloud segment

        ARMARX_INFO << "Committing box layer";
        arviz.commit(boxLayer);
#endif
    }



    void RansacUncertainty::sampleCurrentObject()
    {
        if (selectedLabel < 0)
        {
            ARMARX_INFO << "No label selected";
            return;
        }
        auto* object = sceneDistribution.findObjectWithID(selectedLabel);
        if (!object)
        {
            ARMARX_WARNING << "Could not find object with ID: " << selectedLabel;
            return;
        }

        viz::Layer layer = arviz.layer("Samples");

        semrel::ShapePtr sampledShape = object->sample(randomEngine);
        addShapeToLayer(sampledShape.get(), layer);

        arviz.commit(layer);
    }

    void RansacUncertainty::sampleScene()
    {
        viz::Layer layer = arviz.layer("Samples");

        for (auto& object : sceneDistribution.objects)
        {
            semrel::ShapePtr sampledShape = object.sample(randomEngine);
            addShapeToLayer(sampledShape.get(), layer);
        }

        arviz.commit(layer);
    }

    // TODO: Move somewhere else
    // This function combines two probability paths into one
    double probabilityOf_A_or_B(double pA, double pB)
    {
        return pA + pB - pA * pB;
    }

    // We can calculate the probability of all intersecting paths
    // This can be used to calculate the transitive support probability in a complicated graph
    //    +-> B -+
    // A -|      |-> D
    //    +-> C -+
    // What is the probability of SUPP(A, D)?
    //  p(A -> D) = p(A->B->D or A->C->D)
    //            = p(A->B->D) + p(A->C->D) - p(A->B->D)*p(A->C->D)
    double probabilityOfIntersection(std::vector<double> const& ps)
    {
        double result = 0.0;
        for (double p : ps)
        {
            result = probabilityOf_A_or_B(result, p);
        }
        return result;
    }

    static void colorVerticesByID(semrel::AttributedGraph& ag)
    {
        for (auto vertex : ag.vertices())
        {
            int segmentID = vertex.attrib().objectID.t;
            Eigen::Vector4i segmentColor = simox::color::GlasbeyLUT::at(segmentID).to_vector4i();
            Eigen::Vector4i black(0, 0, 0, 255);
            Eigen::Vector4i white(255, 255, 255, 255);

            int sumSegColor = segmentColor(0) + segmentColor(1) + segmentColor(2);
            float averageSegColor = sumSegColor / 3.0f;
            Eigen::Vector4i fontColor;
            if (averageSegColor > 127.0f)
            {
                fontColor = black;
            }
            else
            {
                fontColor = white;
            }

            auto& style = vertex.attrib().json["style"];
            style["fill-color"] = segmentColor;
            style["border-color"] = black;
            style["font-color"] = fontColor;
        }
    }

    using interactive_scene_exploration::Relation;

    void RansacUncertainty::estimateSupportRelations()
    {
        IceUtil::Time begin = IceUtil::Time::now();
        ARMARX_INFO << "Estimating support relations";

        semrel::LogInterface::setMinimumLogLevel(semrel::LogLevel::ERROR);

        ProbabilisticSupportAnalysisParams params;
        params.support.groundLabels.emplace(0);
        params.numberOfSamples = 25;
        ProbabilisticSupportAnalysisResult result = ProbabilisticSupportAnalysis::analyze(sceneDistribution, params, randomEngine);
        ProbabilisticSupportGraph const& g = result.graph;

        IceUtil::Time diff = IceUtil::Time::now() - begin;
        ARMARX_INFO << "Estimate support distribution: " << diff.toMilliSeconds() << "ms";

        semrel::AttributedGraph ag = semrel::toAttributedGraph(g);

        colorVerticesByID(ag);

        for (auto edge : g.edges())
        {
            double actProb = edge.attrib().geometricExistenceProbability;
            double tdProb = edge.attrib().topDownExistenceProbability;

            // Scale color with prop
            auto aEdge = ag.edge(ag.vertex(edge.sourceObjectID()), ag.vertex(edge.targetObjectID()));
            auto& style = aEdge.attrib().json["style"];

            double blendFactor = tdProb / (actProb + tdProb);
            double maxProb = std::max(actProb, tdProb);
            int red = 255.0 * blendFactor;
            red = std::min(red, 255);
            int alpha = std::min((int)(64.0 + maxProb * 192.0), 255);
            style["color"] = Eigen::Vector4i(red, 0, 0, alpha);
        }

        storeGraph("Probabilistic Support Graph", ag);

        // Determinisitic Support Graph
        {
            semrel::AttributedGraph ag = semrel::toAttributedGraph(result.deterministicSupportGraph);
            colorVerticesByID(ag);

            for (auto edge : result.deterministicSupportGraph.edges())
            {
                bool topDown = edge.attrib().fromUncertaintyDetection;

                // Scale color with prop
                auto aEdge = ag.edge(ag.vertex(edge.sourceObjectID()), ag.vertex(edge.targetObjectID()));
                auto& style = aEdge.attrib().json["style"];

                int red = topDown ? 255 : 0;
                style["color"] = Eigen::Vector4i(red, 0, 0, 255);
            }


            storeGraph("Deterministic Support Graph", ag);
        }

#if 0
        auto gtRelations = dataset.entries[selectedPC].supportRelations;

        Relation gt = groundTruthToRelation(gtRelations);
        Relation prob = probabilisticGraphToRelation(result.graph);
        Relation cut = prob.binarize(0.5);
        Relation supp = supportGraphToRelation(result.deterministicSupportGraph);

        double probBrier = Relation::brierScore(prob, gt);
        double cutBrier = Relation::brierScore(cut, gt);
        double suppBrier = Relation::brierScore(supp, gt);
        ARMARX_IMPORTANT << "Brier score: Supp " << suppBrier << ", Prob: " << probBrier << ", Cut: " << cutBrier;
#endif
    }

    void RansacUncertainty::animateSupportRelations()
    {
        ARMARX_INFO << "Animating support relation extraction";

        semrel::LogInterface::setMinimumLogLevel(semrel::LogLevel::ERROR);

        ProbabilisticSupportAnalysisParams params;
        ProbabilisticSupportAnalysisResult result = ProbabilisticSupportAnalysis::analyze(sceneDistribution, params, randomEngine);
        ProbabilisticSupportGraph const& g = result.graph;

        semrel::AttributedGraph ag = semrel::toAttributedGraph(g);
        colorVerticesByID(ag);

        for (auto edge : g.edges())
        {
            int sourceID = (int)edge.sourceObjectID();
            int targetID = (int)edge.targetObjectID();
            std::pair<int, int> key(sourceID, targetID);

            // Scale color with prop
            auto sVertex = ag.vertex(edge.sourceObjectID());
            auto tVertex = ag.vertex(edge.targetObjectID());
            auto aEdge = ag.edge(sVertex, tVertex);
            auto& style = aEdge.attrib().json["style"];

            style["color"] = Eigen::Vector4i(0, 0, 0, 0);
        }

        storeGraph("Probabilistic Support Graph", ag);
        usleep(1000 * 1000);

        std::map<std::pair<int, int>, int> actCount;
        std::map<std::pair<int, int>, int> topDownCount;

        armarx::viz::Layer animation = arviz.layer("Animation");
        int numberOfSamples = (int)result.sceneSamples.size();
        for (int i = 0; i < numberOfSamples; ++i)
        {
            animation.clear();

            auto& scene = result.sceneSamples[i];
            auto& supportGraph = result.supportGraphs[i];

            for (auto& shape : scene)
            {
                addShapeToLayer(shape.get(), animation);
            }

            for (auto edge : supportGraph.edges())
            {
                int sourceID = (int)edge.sourceObjectID();
                int targetID = (int)edge.targetObjectID();
                std::pair<int, int> key(sourceID, targetID);

                if (edge.attrib().fromUncertaintyDetection)
                {
                    // Top-down support
                    topDownCount[key] += 1;
                }
                else
                {
                    // Act support
                    actCount[key] += 1;
                }
            }

            for (auto edge : g.edges())
            {
                int sourceID = (int)edge.sourceObjectID();
                int targetID = (int)edge.targetObjectID();
                std::pair<int, int> key(sourceID, targetID);

                // Scale color with prop
                auto sVertex = ag.vertex(edge.sourceObjectID());
                auto tVertex = ag.vertex(edge.targetObjectID());
                auto aEdge = ag.edge(sVertex, tVertex);
                auto& style = aEdge.attrib().json["style"];

                int acts = actCount[key];
                int topDowns = topDownCount[key];
                if (acts == 0 && topDowns == 0)
                {
                    style["color"] = Eigen::Vector4i(0, 0, 0, 0);
                    continue;
                }

#if 0
                double actProb = 1.0 * acts / numberOfSamples;
                double tdProb = 1.0 * topDowns / numberOfSamples;
#else
                double actProb = 1.0 * acts / (i + 1);
                double tdProb = 1.0 * topDowns / (i + 1);
#endif

                double blendFactor = tdProb / (actProb + tdProb);
                double maxProb = std::max(actProb, tdProb);
                int red = 255.0 * blendFactor;
                red = std::min(red, 255);
                int alpha = std::min((int)(maxProb * 255.0), 255);
                style["color"] = Eigen::Vector4i(red, 0, 0, alpha);
            }

            arviz.commit(animation);
            storeGraph("Probabilistic Support Graph", ag);

            usleep(1000 * 40);
        }

    }

    void RansacUncertainty::savePCD()
    {
        if (selectedPC < 0)
        {
            return;
        }

        auto& entry = dataset.entries[selectedPC];
        std::string filename = entry.id + ".pcd";

        PointCloudIO::saveToPCD(filename, entry.pointCloud);
    }


    void RansacUncertainty::evaluate()
    {
        using namespace interactive_scene_exploration;

        ProbabilisticSupportEvaluationParams params;
        params.support.support.groundLabels.emplace(0);

        std::vector<ExtractedRelations> relations = ProbabilisticSupportEvaluation::extractRelations(
                    params, dataset, randomEngine);

        ProbabilisticSupportEvaluationResult result = ProbabilisticSupportEvaluation::evaluate(relations);

        ARMARX_IMPORTANT << "Probabilistic support evaluation OSD\n";
        ARMARX_IMPORTANT << "Deterministic: \n";
        ARMARX_IMPORTANT << "  Precision: " << result.deterministic.precision << "\n"
                         << "  Recall: " << result.deterministic.recall << "\n"
                         << "  Accuracy: " << result.deterministic.accuracy << "\n"
                         << "  F1 score: " << result.deterministic.F1 << "\n"
                         << "  Brier score: " << result.deterministic.brierScore << "\n"
                         << "\n";
        ARMARX_IMPORTANT << "Probabilistic: \n";
        ARMARX_IMPORTANT << "  Precision: " << result.deterministic.precision << "\n"
                         << "  Recall: " << result.deterministic.recall << "\n"
                         << "  Accuracy: " << result.deterministic.accuracy << "\n"
                         << "  F1 score: " << result.deterministic.F1 << "\n"
                         << "  Brier score: " << result.deterministic.brierScore << "\n"
                         << "\n";
    }

    void RansacUncertainty::estimateDistribution(const pcl::PointCloud<pcl::PointXYZRGBL>& pointcloud)
    {
        IceUtil::Time begin = IceUtil::Time::now();
        ARMARX_INFO << "Estimating for point cloud with size: " << pointcloud.size();

        PointCloudToSceneParams params;
        int maxIterations = 25;
        params.object.box.maxIterations = maxIterations;
        params.object.cylinder.maxIterations = maxIterations;
        params.object.sphere.maxIterations = maxIterations;
        PointCloudToSceneResult scene = PointCloudToScene::extractScene(params, pointcloud, randomEngine);
        sceneDistribution = scene.distribution;

        for (auto& extraction : scene.extractionResults)
        {
            boxesPerSegment[extraction.object.id] = extraction.boxes;
            cylindersPerSegment[extraction.object.id] = extraction.cylinders;
        }

        visualizePointCloud(arviz, scene.segmentMap, selectedPointsSize);
        visualizeMeanOfDistribution(arviz, sceneDistribution);

        IceUtil::Time diff = IceUtil::Time::now() - begin;
        ARMARX_INFO << "Estimate scene distribution: " << diff.toMilliSeconds() << "ms";

        estimateSupportRelations();
    }
}
